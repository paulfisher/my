# Pavel Rybakov

email: pavel.rybako@gmail.com

skype: pavel.rybako

linkedin: https://www.linkedin.com/in/pavelrybako


Expert in web-technologies. In software development since 2009

Technology stack

* Java (Spring Boot, Microservices, Kafka)
* PHP (Laravel, Yii)
* MySQL, Javascript

Also had commercial experience with:

* C#, MSSQL
* Android development
* all popular PHP frameworks
* SmartFoxServer, Brightspot CMS
* AngularJS, Less, SCSS and other minor FE technologies


## Java Software Developer, Nitka(Politico) (2016 Oct. - currently)
Client - the biggest internet magazine about politics in USA. They provide a bunch of services(B2B) that requires modern tech solutions.

Microservices development on Spring Boot. My everyday tasks - backend API development, development and maintenance of commonly used libraries, microservices

Technology stack

* Spring Boot, 1.x/2.x
* Kafka
* MySQL
* Pure js/jquery
* some legacy PHP
* Brightspot CMS
* Jenkins, Docker, K8s


## Java developer, OrdinaryPeople(2014 Feb. - 2014 Nov.)
IOS game Age Of Fury backend development. Came on board to help with SmartFox server which was hardly able to handle 100 players online. I've proposed a solution to optimize performance of the server. Implementation of this solution allowed us to handle thousands of players online. 

Technology stack

* SmartFoxServer/Java
* PostgreSQL


## Software developer, TravelLine(2009 Aug. - 2014 Jan.)
Became part of the team as a second grade student. Took part in development of the most parts of the system, starting with css/js ending core booking logic development. During 4 years I went from a junior to senior developer. Have developed B2B part for agents - special tariffs/commissions.

Technology stack

* C#, .NET, MSSQL
* php, js, MySQL
* java


## During 2014-2020 took part in different side projects as a team-lead
rfidcontrol.ru(inactive now) - developed rfid-tag based logistic system for storages:

* java, Jersey server, PostgreSQL
* android
* angularjs


BackItForward.com(now kickico.com) - crowdfunding platform:

* Yii, Laravel, js, php, MySQL


AirportWay.com - online transfer booking engine:

* Laravel, js, php, MySQL


Have pentest experience

* I pay special attention to security issues in my development experience


## Education
Volga State University of Technology(http://en.volgatech.net/) 2007-2012

Faculty of information and computer systems

Master's degree in software development
