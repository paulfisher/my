#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR="$( realpath -s ${DIR}/../.. )"
LIQUIBASE="$( realpath -s ${DIR}/../liquibase/liquibase )"

ENV=$1

if [ -z "$ENV"  ]; then
  echo "Usage: update <ENV>"
  exit 1
fi

ENV_CONFIG="$DIR/$ENV/liquibase.properties"
CHANGELOG_DIR="$ROOT_DIR/db-migrations"
CHANGELOG_FILE="changelog.xml"

if [ ! -f "$ENV_CONFIG"  ]; then
  echo "$ENV: liquibase config was not found at $ENV_CONFIG"
  exit 1
fi

echo "$ENV: Updating..."
$( cd ${CHANGELOG_DIR} && ${LIQUIBASE} --defaultsFile="$ENV_CONFIG" --changeLogFile="$CHANGELOG_FILE" rollback "init" )
exit $?
