@echo off

set DIR=%~dp0
set ROOT_DIR=%DIR%..\..
set LIQUIBASE=%DIR%..\liquibase\liquibase
set STARTUP_WORKING_DIR=%cd%

if [%1]==[] goto :usage

set ENV=%1
set CHANGELOG_DIR=%ROOT_DIR%\db-migrations

echo %ENV%: Updating...

cd %CHANGELOG_DIR%
call "%LIQUIBASE%" --defaultsFile="%DIR%\%ENV%\liquibase.properties" --changeLogFile="changelog.xml" rollback "init"
cd %STARTUP_WORKING_DIR%

goto :eof

:usage
  echo Usage: init_revert ^<ENV^>
  goto :eof
