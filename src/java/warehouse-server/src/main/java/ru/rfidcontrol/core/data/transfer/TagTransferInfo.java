package ru.rfidcontrol.core.data.transfer;

import ru.rfidcontrol.core.data.product.PriceInfo;
import ru.rfidcontrol.core.data.product.QuantityInfo;
import ru.rfidcontrol.core.data.tag.TagInfo;
import ru.rfidcontrol.core.entity.TagTransfer;
import ru.rfidcontrol.core.entity.Transfer;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class TagTransferInfo {

    public int id;
    @Nullable public TagInfo tagInfo;

    public int transferId;
    public int transferDirectionType;
    public long transferWhen;

    public int transferCompanyId;
    public String transferCompanyName;

    public int transferUserId;
    @Nullable public String transferUsername;

    public int transferDeviceId;
    public String transferDeviceName;
    public String transferDeviceImei;

    public int transferStorageId;
    public String transferStorageName;
    public String transferBillNumber;

    public int transferSupplierId;
    public String transferSupplierName;

    public QuantityInfo quantityInfo;
    public PriceInfo priceInfo;

    public TagTransferInfo(TagTransfer tagTransfer) {
        this.id = tagTransfer.getId();
        this.tagInfo = new TagInfo(tagTransfer.getTag());

        Transfer transfer = tagTransfer.getTransfer();
        this.transferId = transfer.getId();
        this.transferDirectionType = transfer.getDirection().value;
        this.transferWhen = transfer.getWhen().getTime();

        this.transferCompanyId = transfer.getStorage().getCompany().getId();
        this.transferCompanyName = transfer.getStorage().getCompany().getName();

        if (transfer.getUser() != null) {
            this.transferUserId = transfer.getUser().getId();
            this.transferUsername = transfer.getUser().getName();
        }

        if (transfer.getDevice() != null) {
            this.transferDeviceId = transfer.getDevice().getId();
            this.transferDeviceName = transfer.getDevice().getName();
            this.transferDeviceImei = transfer.getDevice().getImeiCode();
        }

        this.transferStorageId = transfer.getStorage().getId();
        this.transferStorageName = transfer.getStorage().getName();
        this.transferBillNumber = transfer.getBillNumber();

        if (transfer.getSupplier() != null) {
            this.transferSupplierId = transfer.getSupplier().getId();
            this.transferSupplierName = transfer.getSupplier().getName();
        }

        this.priceInfo = new PriceInfo(tagTransfer.getPrice() == null ? 0 : tagTransfer.getPrice(), tagTransfer.getVat() == null ? 0 : tagTransfer.getVat());
        this.quantityInfo = new QuantityInfo(tagTransfer.getSizer() == null ? null : tagTransfer.getSizer().toInfo(), tagTransfer.getQuantity());
    }

    @NotNull
    public TagInfo getTagInfo() {
        if (tagInfo == null) {
            tagInfo = new TagInfo();
        }
        return tagInfo;
    }
}