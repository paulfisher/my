package ru.rfidcontrol.core.data.device;

import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.Storage;

import java.util.Date;

public class DeviceInfo {
    public int id;
    public String imeiCode;
    public String name;
    public Date createTime;
    public Date lastLoginTime;
    public int storageId = 0;
    public int companyId = 0;

    public String storageName = "";
    public String companyName = "";
    public boolean isDeleted;

    public DeviceInfo() {
    }

    public DeviceInfo(Device device) {
        id = device.getId();
        imeiCode = device.getImeiCode();
        name = device.getName();
        createTime = device.getCreateTime();
        lastLoginTime = device.getLastLoginTime();
        isDeleted = device.isDeleted();

        Storage storage = device.getStorage();
        if (storage != null) {
            storageId = storage.getId();
            storageName = storage.getName();
        }

        Company company = device.getCompany();
        companyId = company.getId();
        companyName = company.getName();
    }


}
