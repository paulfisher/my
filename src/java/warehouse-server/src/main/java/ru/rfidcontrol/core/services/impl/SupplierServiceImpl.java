package ru.rfidcontrol.core.services.impl;

import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Supplier;
import ru.rfidcontrol.core.entity.Tag;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.ServiceBase;
import ru.rfidcontrol.core.services.SupplierService;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupplierServiceImpl extends ServiceBase implements SupplierService {

    @NotNull
    @Override
    public Map<Integer, Supplier> getSuppliers(Company company) {
        List<Supplier> supplierList = em().createQuery("from Supplier where company = :company", Supplier.class)
                .setParameter("company", company)
                .getResultList();
        Map<Integer, Supplier> result = new HashMap<>();

        for (Supplier supplier : supplierList) {
            result.put(supplier.getId(), supplier);
        }
        return result;
    }

    @NotNull
    @Override
    public Supplier getSupplier(int id) {
        Supplier supplier = findSupplier(id);
        if (supplier == null) {
            throw WarehouseException.notFound(Supplier.class, id);
        }
        return supplier;
    }


    @Nullable
    @Override
    public Supplier findSupplier(int id) {
        return find(Supplier.class, id);
    }
}