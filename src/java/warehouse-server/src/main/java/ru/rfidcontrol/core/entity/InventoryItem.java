package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.entity.type.InventoryItemStatus;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "inventory_item")
public class InventoryItem {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "status")
    private int status;

    @Column(name = "invent_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inventDate;

    @ManyToOne
    @JoinColumn(name = "tag_id")
    private Tag tag;

    @ManyToOne
    @JoinColumn(name = "expected_product_category_id")
    private ProductCategory expectedProductCategory;

    @ManyToOne
    @JoinColumn(name = "actual_product_category_id")
    private ProductCategory actualProductCategory;

    @ManyToOne
    @JoinColumn(name = "inventory_id")
    private Inventory inventory;

    public InventoryItem() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public InventoryItem setId(int id) {
        this.id = id;
        return this;
    }

    @Nullable
    public Date getInventDate() {
        return inventDate;
    }

    @NotNull
    public InventoryItem setInventDate(@NotNull Date inventDate) {
        this.inventDate = inventDate;
        return this;
    }

    @NotNull
    public InventoryItemStatus getStatus() {
        return InventoryItemStatus.get(status);
    }

    @NotNull
    public InventoryItem setStatus(@NotNull InventoryItemStatus status) {
        this.status = status.value;
        return this;
    }

    @NotNull
    public Inventory getInventory() {
        return inventory;
    }

    @NotNull
    public InventoryItem setInventory(@NotNull Inventory inventory) {
        this.inventory = inventory;
        return this;
    }

    @NotNull
    public Tag getTag() {
        return tag;
    }

    @NotNull
    public InventoryItem setTag(@NotNull Tag tag) {
        this.tag = tag;
        return  this;
    }

    @Nullable
    public ProductCategory getActualProductCategory() {
        return actualProductCategory;
    }

    @NotNull
    public InventoryItem setActualProductCategory(@Nullable ProductCategory actualProductCategory) {
        this.actualProductCategory = actualProductCategory;
        return this;
    }

    /**
     * TODO: возможно использовать getTag().getProductCategory()..
     * @return
     */
    @NotNull
    public ProductCategory getExpectedProductCategory() {
        return expectedProductCategory;
    }

    @NotNull
    public InventoryItem setExpectedProductCategory(@NotNull ProductCategory expectedProductCategory) {
        this.expectedProductCategory = expectedProductCategory;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .toString();
    }
}
