package ru.rfidcontrol.app.guice.logger;

import com.google.inject.MembersInjector;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import org.jboss.logging.Logger;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LoggerTypeListener implements TypeListener {

    @Override public <T> void hear(TypeLiteral<T> typeLiteral, TypeEncounter<T> typeEncounter) {
        for (Field field : getAllFields(typeLiteral.getRawType())) {
            if (!field.isAnnotationPresent(InjectLogger.class)) {
                continue;
            }
            if (field.getType() != Logger.class) {
                throw new RuntimeException(String.format("Failed to inject logger '%s.%s': field is marked @InjectLogger, but not of org.slf4j.Logger type", typeLiteral.getRawType().getName(), field.getName()));
            }
            typeEncounter.register(new LoggerMembersInjector<T>(field, typeLiteral.getRawType()));
        }
    }

    private static List<Field> getAllFields(Class<?> type) {
        return getAllFields(new ArrayList<Field>(), type);
    }

    private static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        Collections.addAll(fields, type.getDeclaredFields());
        if (type.getSuperclass() == null) {
            return fields;
        }
        return getAllFields(fields, type.getSuperclass());
    }

    private static class LoggerMembersInjector<T> implements MembersInjector<T> {
        private final Field field;
        private final Logger logger;

        LoggerMembersInjector(Field field, Class<?> cls) {
            this.field = field;
            this.logger = Logger.getLogger(cls);
            field.setAccessible(true);
        }

        @Override public void injectMembers(T t) {
            try {
                field.set(t, logger);
            }
            catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }
}