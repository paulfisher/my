package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.data.user.UserInfo;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String password;

    @Column(name = "create_time", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "last_login_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginTime;

    @Column(name = "is_admin")
    private boolean isAdmin;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinTable(name = "user_company",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "company_id") })
    @Cascade(CascadeType.SAVE_UPDATE)
    private Company company;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "users")
    private List<Device> devices;

    public User() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public User setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    @NotNull
    public User setName(String name) {
        this.name = name;
        return this;
    }

    @NotNull
    public String getPassword() {
        return password;
    }

    @NotNull
    public User setPassword(@NotNull String password) {
        this.password = password;
        return this;
    }

    @NotNull
    public Date getCreateTime() {
        return createTime;
    }

    @NotNull
    public User setCreateTime(@NotNull Date createTime) {
        this.createTime = createTime;
        return this;
    }

    @Nullable
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    @NotNull
    public User setLastLoginTime(@NotNull Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        return this;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    @NotNull
    public User setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
        return this;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    @NotNull
    public User setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    @Nullable
    public Company getCompany() {
        return company;
    }

    public User setCompany(@Nullable Company company) {
        this.company = company;
        return this;
    }

    @NotNull
    public List<Device> getDevices() {
        if (devices == null) {
            devices = new ArrayList<>();
        }
        return devices;
    }

    public User setDevices(@NotNull Collection<Device> devices) {
        if (this.devices == null) {
            this.devices = new ArrayList<>(devices);
        }
        this.devices.clear();
        this.devices.addAll(devices);
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("isAdmin", isAdmin)
                .toString();
    }

    public static User fromInfo(UserInfo userInfo) {
        return new User()
                .setName(userInfo.name)
                .setAdmin(userInfo.isAdmin);
    }

    public UserInfo toInfo() {
        return new UserInfo(this);
    }
}
