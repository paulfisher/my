package ru.rfidcontrol.app.services;

import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.User;

import javax.validation.constraints.NotNull;

public interface LoginService {

    User loginUser(String name, String password);

    @NotNull Device loginDevice(String imei);

    void loginStorage(StorageInfo storageInfo);

    void logout();
}