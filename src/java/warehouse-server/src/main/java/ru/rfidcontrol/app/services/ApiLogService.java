package ru.rfidcontrol.app.services;

public interface ApiLogService {

    void logRequest(String request);
    void logResponse(String response);

}
