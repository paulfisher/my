package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.data.company.SupplierInfo;
import ru.rfidcontrol.core.data.product.SizerInfo;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sizer")
public class Sizer {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "okei_code")
    private int okeiCode;

    @Column(name = "name")
    private String name;

    @Column(name = "short_name")
    private String shortName;


    public Sizer() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Sizer setId(int id) {
        this.id = id;
        return this;
    }

    public int getOkeiCode() {
        return okeiCode;
    }

    @NotNull
    public Sizer setOkeiCode(int okeiCode) {
        this.okeiCode = okeiCode;
        return this;
    }

    public String getName() {
        return name;
    }

    @NotNull
    public Sizer setName(String name) {
        this.name = name;
        return this;
    }

    @NotNull
    public String getShortName() {
        return shortName;
    }

    @NotNull
    public Sizer setShortName(@NotNull String shortName) {
        this.shortName = shortName;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("okeiCode", okeiCode)
                .add("shortName", shortName)
                .toString();
    }

    public static Sizer fromInfo(SizerInfo sizerInfo) {
        return new Sizer()
                .setName(sizerInfo.name)
                .setOkeiCode(sizerInfo.okeiCode)
                .setShortName(sizerInfo.shortName);
    }

    public SizerInfo toInfo() {
        return new SizerInfo(this);
    }
}