package ru.rfidcontrol.app.data.device;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.device.DeviceInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class DeviceRequest extends BaseRequest {

    @Nullable
    public DeviceInfo info;

    @NotNull
    public DeviceInfo deviceInfo() {
        if (info == null) {
            info = new DeviceInfo();
        }
        return info;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        if (clientContext.getCompanyId() > 0) {
            deviceInfo().companyId = clientContext.getCompanyId();
        }
    }
}
