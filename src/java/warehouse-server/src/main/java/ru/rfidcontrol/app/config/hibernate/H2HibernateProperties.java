package ru.rfidcontrol.app.config.hibernate;

import ru.rfidcontrol.app.config.DbConfig;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;


public class H2HibernateProperties implements HibernateProperties {

    @Override
    @Nonnull
    @SuppressWarnings("SpellCheckingInspection")
    public Map<String, String> constructProperties(@Nonnull DbConfig dbConfig) {
        Map<String, String> properties = new HashMap<>();

        properties.put("hibernate.archive.autodetection", "class");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");

        properties.put("hibernate.connection.driver_class", "org.h2.Driver");

        properties.put("hibernate.connection.url", dbConfig.getUrl());
        properties.put("hibernate.connection.username", dbConfig.getUser());
        properties.put("hibernate.connection.password", dbConfig.getPassword());

        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.hbm2ddl.auto", "create-drop");

        return properties;
    }
}
