package ru.rfidcontrol.core.data.device;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.Device;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceInfoList extends BaseInfoList {

    public final Map<Integer, DeviceInfo> devices;
    public final int size;

    public DeviceInfoList(Map<Integer, Device> devices) {
        this.devices = new HashMap<>();
        for (Map.Entry<Integer, Device> entry : devices.entrySet()) {
            this.devices.put(entry.getKey(), entry.getValue().toInfo());
        }
        this.size = this.devices.size();
    }


    public DeviceInfoList(List<Device> devices) {
        this.devices = new HashMap<>();
        for (Device device : devices) {
            this.devices.put(device.getId(), device.toInfo());
        }
        this.size = this.devices.size();
    }
}