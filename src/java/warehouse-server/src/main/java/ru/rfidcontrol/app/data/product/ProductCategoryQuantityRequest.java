package ru.rfidcontrol.app.data.product;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;
import ru.rfidcontrol.core.data.storage.StorageInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class ProductCategoryQuantityRequest extends BaseRequest {

    @Nullable public ProductCategoryInfo productCategoryInfo;

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        if (productCategoryInfo == null) {
            productCategoryInfo = new ProductCategoryInfo();
        }
        productCategoryInfo.companyId = clientContext.getCompanyId();
    }

}