package ru.rfidcontrol.app.data.tag;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.tag.TagProductInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;

public class SearchTagsRequest extends BaseRequest {
    @Nullable public List<String> rfIds;
    @NotNull public final TagProductInfo tagInfo;

    public SearchTagsRequest() {
        tagInfo = new TagProductInfo();
    }

    @NotNull
    public TagProductInfo getTagInfo() {
        return tagInfo;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        tagInfo.companyId = clientContext.getCompanyId();
    }
}
