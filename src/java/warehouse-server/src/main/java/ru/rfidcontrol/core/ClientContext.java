package ru.rfidcontrol.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import ru.rfidcontrol.core.entity.type.ExternalApp;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class ClientContext {

    private int userId;
    private int companyId;
    private int deviceId;
    private int storageId;
    private boolean isAdmin;
    private String externalApp;

    public int getUserId() {
        return userId;
    }

    public ClientContext setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getCompanyId() {
        return companyId;
    }

    public ClientContext setCompanyId(int companyId) {
        this.companyId = companyId;
        return this;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public ClientContext setDeviceId(int deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public int getStorageId() {
        return storageId;
    }

    public ClientContext setStorageId(int storageId) {
        this.storageId = storageId;
        return this;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public ClientContext setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userId", getUserId())
                .add("deviceId", getDeviceId())
                .add("storageId", getStorageId())
                .add("isAdmin", isAdmin())
                .toString();
    }

    @NotNull
    public ClientContext setExternalApp(ExternalApp externalApp) {
        this.externalApp = externalApp.id;
        return this;
    }

    @Nullable
    public ExternalApp getExternalApp() {
        if (Strings.isNullOrEmpty(this.externalApp)) {
            return null;
        }
        return ExternalApp.get(this.externalApp);
    }
}