package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.product.ProductInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Product;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.entity.Storage;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface ProductService {

    @NotNull Map<Integer, Product> findProducts(ProductInfo productInfo);

    @NotNull Map<Integer, Product> findProducts(Storage storage, ProductCategory productCategory, Pagination pagination);

    @NotNull Map<Integer, Product> findProducts(Storage storage, ProductCategory productCategory);

    int countProducts(Storage storage, ProductCategory productCategory);

    @Nullable Product findProduct(int id);

    @NotNull Product getProduct(int id);

    @NotNull Product createProduct(@NotNull Storage storage,
                                   @NotNull Company company,
                                   @NotNull ProductInfo productInfo);

    @NotNull Product editProduct(@NotNull Product product, @NotNull ProductInfo productInfo);

    @NotNull Product deleteProduct(@NotNull Product product);
}