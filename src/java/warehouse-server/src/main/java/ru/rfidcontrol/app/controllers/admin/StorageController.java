package ru.rfidcontrol.app.controllers.admin;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.storage.StorageRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.storage.StorageInfoList;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.services.StorageService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/admin/storage")
@AccessControl
@AllowFor({AccessTypes.ADMIN})
public class StorageController extends BaseController {

    @Inject @NotNull protected StorageService storageService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findStorages(@NotNull StorageRequest storageRequest) {
        Map<Integer, Storage> findResult = storageService.findStorages(storageRequest.storageInfo());

        return DataResponse.create(new StorageInfoList(findResult));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getStorage(StorageRequest storageRequest) {
        required(storageRequest.storageInfo().id, "info.id");

        Storage storage = storageService.getStorage(storageRequest.storageInfo().id);

        return DataResponse.create(storage.toInfo());
    }

    @POST
    @Path("/create")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse createStorage(StorageRequest storageRequest) {
        Storage created = storageService.createStorage(storageRequest.storageInfo());

        return DataResponse.create(created.toInfo());
    }

    @POST
    @Path("/edit")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse editStorage(StorageRequest storageRequest) {
        required(storageRequest.storageInfo().id, "info.id");

        Storage storage = storageService.getStorage(storageRequest.storageInfo().id);
        storageService.editStorage(storage, storageRequest.storageInfo());

        return DataResponse.create(storage.toInfo());
    }

    @POST
    @Path("/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse deleteStorage(StorageRequest storageRequest) {
        required(storageRequest.storageInfo().id, "info.id");

        Storage storage = storageService.getStorage(storageRequest.storageInfo().id);
        storageService.deleteStorage(storage);

        return DataResponse.create(storage.toInfo());
    }
}