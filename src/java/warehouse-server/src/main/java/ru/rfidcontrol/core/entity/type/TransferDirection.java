package ru.rfidcontrol.core.entity.type;

import static ru.rfidcontrol.core.exceptions.WarehouseException.notFound;

public enum TransferDirection {

    IN(1),
    OUT(2);

    public final int value;

    TransferDirection(int value) {
        this.value = value;
    }

    public static TransferDirection get(int value) {
        for (TransferDirection transferDirection : values()) {
            if (transferDirection.value == value) {
                return transferDirection;
            }
        }
        throw notFound(TransferDirection.class, value);
    }
}