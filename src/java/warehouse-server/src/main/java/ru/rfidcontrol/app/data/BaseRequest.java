package ru.rfidcontrol.app.data;

import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.Sorting;

import javax.validation.constraints.NotNull;

public abstract class BaseRequest {

    public Pagination pagination = new Pagination();
    public Sorting sorting = new Sorting();

    public void populateFromContext(@NotNull ClientContext clientContext) {

    }

}
