package ru.rfidcontrol.core.services;

import com.google.common.eventbus.EventBus;
import com.google.inject.Provider;
import ru.rfidcontrol.core.exceptions.WarehouseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.List;

import static ru.rfidcontrol.core.services.StringHelper.md5;

public class ServiceBase {

    private static final String USER_SALT = "IrIqu1znjIe8MoYeBP";

    @Inject EventBus eventBus;

    @Inject Provider<EntityManager> em;

    @NotNull
    protected EntityManager em() {
        return em.get();
    }

    @NotNull
    protected <T> TypedQuery<T> q(String query, Class<T> resultClass) {
        return em().createQuery(query, resultClass);
    }

    @Nullable
    protected <T> T find(@Nonnull Class<T> entityClass, @Nonnull Object id) {
        return em.get().find(entityClass, id);
    }

    @Nullable
    protected <T> T first(@Nonnull TypedQuery<T> query) {
        return first(query.setMaxResults(1).getResultList());
    }

    @Nullable
    protected <T> T first(List<T> res) {
        return res == null || res.isEmpty() ? null : res.get(0);
    }

    @NotNull
    protected <T> T firstOrFail(@Nonnull TypedQuery<T> query) {
        T result = first(query);
        if (result == null) {
            throw WarehouseException.notFound(query);
        }
        return result;
    }

    protected int count(@Nonnull TypedQuery<Number> query) {
        List<Number> res = query.setMaxResults(1).getResultList();
        return res == null || res.isEmpty() ? 0 : res.get(0).intValue();
    }

    protected <T> T save(@Nonnull T entity) {
        em.get().persist(entity);
        return entity;
    }

    protected void remove(@Nonnull Object entity) {
        em.get().remove(entity);
    }

    protected void trigger(Object event) {
        eventBus.post(event);
    }

    protected String hashUserPassword(String password) {
        return md5(String.format("%s%s", USER_SALT, password));
    }
}
