package ru.rfidcontrol.core.data.user;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserInfoList extends BaseInfoList {

    public final Map<Integer, UserInfo> users;
    public final int size;

    public UserInfoList(Map<Integer, User> users) {
        this.users = new HashMap<>();
        for (Map.Entry<Integer, User> entry : users.entrySet()) {
            this.users.put(entry.getKey(), new UserInfo(entry.getValue()));
        }
        size = this.users.size();
    }

    public UserInfoList(List<User> users) {
        this.users = new HashMap<>();
        for (User user : users) {
            this.users.put(user.getId(), new UserInfo(user));
        }
        size = this.users.size();
    }
}