package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.data.tag.TagProductInfo;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "tag")
public class Tag {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "rfid")
    private String rfId;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "create_time", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "storage_id")
    private Storage storage;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    @OneToOne(mappedBy = "tag")
    private Product product;

    public Tag() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Tag setId(int id) {
        this.id = id;
        return this;
    }

    @NotNull
    public Tag setRfId(String rfId) {
        this.rfId = rfId;
        return this;
    }

    @NotNull
    public String getRfId() {
        return rfId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    @NotNull
    public Tag setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    @NotNull
    public Date getCreateTime() {
        return createTime;
    }

    @NotNull
    public Tag setCreateTime(@NotNull Date createTime) {
        this.createTime = createTime;
        return this;
    }

    @Nullable
    public Storage getStorage() {
        return storage;
    }

    @NotNull
    public Tag setStorage(@Nullable Storage storage) {
        this.storage = storage;
        return this;
    }

    @NotNull
    public Company getCompany() {
        return company;
    }

    @NotNull
    public Tag setCompany(@NotNull Company company) {
        this.company = company;
        return this;
    }

    @Nullable
    public Product getProduct() {
        return product;
    }

    @NotNull
    public Tag setProduct(@NotNull Product product) {
        this.product = product;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("rfid", rfId)
                .toString();
    }

    public TagProductInfo toInfo() {
        return new TagProductInfo(this);
    }

    public static Tag fromInfo(TagProductInfo tagInfo) {
        return new Tag()
                .setRfId(tagInfo.rfId)
                .setDeleted(tagInfo.isDeleted);
    }
}
