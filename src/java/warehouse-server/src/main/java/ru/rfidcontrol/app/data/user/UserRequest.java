package ru.rfidcontrol.app.data.user;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.user.UserInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class UserRequest extends BaseRequest {

    @Nullable
    public UserInfo info;

    @NotNull
    public UserInfo userInfo() {
        if (info == null) {
            info = new UserInfo();
        }
        return info;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        userInfo().companyId = clientContext.getCompanyId();
    }
}
