package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.data.product.ProductInfo;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "create_time", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "quantity")
    private double quantity;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "product_category_id")
    private ProductCategory productCategory;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "responsible_user_id")
    private User responsibleUser;

    @OneToOne
    @JoinColumn(name = "tag_id")
    private Tag tag;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "sizer_id")
    private Sizer sizer;

    public Product() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Product setId(int id) {
        this.id = id;
        return this;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    @NotNull
    public Product setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    @NotNull
    public Date getCreateTime() {
        return createTime;
    }

    @NotNull
    public Product setCreateTime(@NotNull Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public double getQuantity() {
        return quantity;
    }

    @NotNull
    public Product setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    @NotNull
    public ProductCategory getProductCategory() {
        return productCategory;
    }

    @NotNull
    public Product setProductCategory(@NotNull ProductCategory productCategory) {
        this.productCategory = productCategory;
        return this;
    }

    @Nullable
    public User getResponsibleUser() {
        return responsibleUser;
    }

    @NotNull
    public Product setResponsibleUser(@Nullable User responsibleUser) {
        this.responsibleUser = responsibleUser;
        return this;
    }

    @NotNull
    public Tag getTag() {
        return tag;
    }

    @NotNull
    public Product setTag(@NotNull Tag tag) {
        this.tag = tag;
        return this;
    }

    @Nullable
    public Sizer getSizer() {
        return sizer;
    }

    @NotNull
    public Product setSizer(@Nullable Sizer sizer) {
        this.sizer = sizer;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .toString();
    }

    @NotNull
    public static Product fromInfo(@NotNull ProductInfo productInfo) {
        return new Product()
                .setDeleted(productInfo.isDeleted)
                .setQuantity(productInfo.quantityInfo != null && productInfo.quantityInfo.quantity > 0 ? productInfo.quantityInfo.quantity : 1);
    }
}
