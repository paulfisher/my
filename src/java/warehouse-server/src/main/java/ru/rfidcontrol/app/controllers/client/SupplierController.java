package ru.rfidcontrol.app.controllers.client;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.company.SupplierInfoList;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.SupplierService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/supplier")
@AccessControl
@AllowFor({AccessTypes.DEVICE})
public class SupplierController extends BaseController {

    @Inject @NotNull protected SupplierService supplierService;
    @Inject @NotNull protected CompanyService companyService;

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getSuppliers() {
        Company company = companyService.getCompany(getClientContext().getCompanyId());
        return DataResponse.create(new SupplierInfoList(supplierService.getSuppliers(company)));
    }
}