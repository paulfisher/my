package ru.rfidcontrol.app.controllers.manager;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.UserService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

public class BaseOnlineController extends BaseController {

    @Inject @NotNull protected UserService userService;

    @NotNull
    User me() {
        return userService.getUser(getClientContext().getUserId());
    }

    @NotNull
    protected Company myCompany() {
        Company company = me().getCompany();
        if (company == null) {
            throw WarehouseException.companyNotFound(String.format("For user %s", me()));
        }
        return company;
    }

}
