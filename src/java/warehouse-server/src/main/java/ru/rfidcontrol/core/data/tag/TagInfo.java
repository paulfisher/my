package ru.rfidcontrol.core.data.tag;

import ru.rfidcontrol.core.data.BaseInfo;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.entity.Tag;

import javax.validation.constraints.NotNull;

public class TagInfo extends BaseInfo {

    public int id;
    public String rfId;
    public long createTime;
    public boolean isDeleted;

    public int storageId = 0;
    public int companyId = 0;
    public String storageName = "";

    public String productCategoryName;
    public int productCategoryId;

    public TagInfo() {}

    public TagInfo(@NotNull Tag tag) {
        this.id = tag.getId();
        this.rfId = tag.getRfId();
        this.createTime = tag.getCreateTime().getTime();
        this.isDeleted = tag.isDeleted();

        Storage storage = tag.getStorage();
        if (storage != null) {
            this.storageId = storage.getId();
            this.storageName = storage.getName();
            this.companyId = storage.getCompany().getId();
        }

        if (tag.getProduct() != null) {
            this.productCategoryId = tag.getProduct().getProductCategory().getId();
            this.productCategoryName = tag.getProduct().getProductCategory().getName();
        }
    }
}