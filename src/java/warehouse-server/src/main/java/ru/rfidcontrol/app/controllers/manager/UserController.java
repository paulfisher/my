package ru.rfidcontrol.app.controllers.manager;

import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.user.UserRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.user.UserInfoList;
import ru.rfidcontrol.core.entity.User;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/online/user")
@AccessControl
@AllowFor({AccessTypes.MANAGER})
public class UserController extends BaseOnlineController {

    @GET
    @Path("/me")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getMe() {
        return DataResponse.create(me().toInfo());
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getUser(@NotNull UserRequest userRequest) {
        populateFromContext(userRequest);

        required(userRequest.userInfo().id, "info.id");

        User user = userService.getUser(userRequest.userInfo().id);
        if (user.getCompany() != null && user.getCompany().getId() != getClientContext().getCompanyId()) {
            throw notFound("User not found");
        }
        if (user.isAdmin()) {
            throw forbidden();
        }

        return DataResponse.create(user.toInfo());
    }

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findUsers(@NotNull UserRequest userRequest) {
        populateFromContext(userRequest);

        Map<Integer, User> users = userService.findUsers(userRequest.userInfo());

        //����������� �� ���������. �� ������ ��������
        for (Map.Entry<Integer, User> integerUserEntry : users.entrySet()) {
            User user = integerUserEntry.getValue();
            if (user.getCompany() == null
                    || user.isAdmin()
                    || user.getCompany().getId() != getClientContext().getCompanyId()) {
                throw forbidden();
            }
        }
        return DataResponse.create(new UserInfoList(users));
    }
}