package ru.rfidcontrol.app.controllers.manager;

import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.services.CompanyService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/online/company")
@AccessControl
@AllowFor({AccessTypes.MANAGER})
public class CompanyController extends BaseOnlineController {

    @Inject @NotNull protected CompanyService companyService;

    @GET
    @Path("/my")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getMyCompany() {
        return DataResponse.create(myCompany().toInfo());
    }

    @GET
    @Path("/my/stats")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getMyCompanyStats() {
        return DataResponse.create(companyService.getCompanyStats(myCompany()));
    }

}