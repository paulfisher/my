package ru.rfidcontrol.core.entity.type;

import java.util.Objects;

import static ru.rfidcontrol.core.exceptions.WarehouseException.notFound;

public enum ExternalApp {

    C1("1c", "12345");

    public final String id;
    public final String token;

    ExternalApp(String id, String token) {
        this.id = id;
        this.token = token;
    }

    public static ExternalApp get(String id) {
        for (ExternalApp app : values()) {
            if (Objects.equals(app.id, id)) {
                return app;
            }
        }
        throw notFound(ExternalApp.class, id);
    }

}
