package ru.rfidcontrol.app.controllers.client;


import com.google.common.base.Strings;
import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.tag.SearchTagsRequest;
import ru.rfidcontrol.app.data.tag.TagRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.tag.SearchTagsResultInfo;
import ru.rfidcontrol.core.data.tag.TagProductInfo;
import ru.rfidcontrol.core.data.tag.TagProductInfoList;
import ru.rfidcontrol.core.entity.Tag;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.TagService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/tag")
@AccessControl
@AllowFor({AccessTypes.DEVICE})
public class TagController extends BaseController {

    @Inject @NotNull protected TagService tagService;
    @Inject @NotNull protected CompanyService companyService;

    @POST
    @Path("/findByRfIds")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findTagsByRfIds(@NotNull SearchTagsRequest searchTagsRequest) {
        populateFromContext(searchTagsRequest);

        required(searchTagsRequest.rfIds, "rfIds");
        //company id or storage id required
        required(searchTagsRequest.getTagInfo().storageId + searchTagsRequest.getTagInfo().companyId, "tagInfo.storageId||tagInfo.companyId");

        Map<String, Tag> findResult = tagService.findTags(searchTagsRequest.getTagInfo(), searchTagsRequest.rfIds);

        return DataResponse.create(new SearchTagsResultInfo(new TagProductInfoList(findResult), searchTagsRequest.rfIds, searchTagsRequest.getTagInfo()));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getTag(TagRequest tagRequest) {
        //required(tagRequest.getTagInfo().id, "tag id required");

        Tag tag = null;
        if (tagRequest.getTagInfo().id > 0) {
            tag = tagService.getTag(tagRequest.getTagInfo().id);
        } else if (!Strings.isNullOrEmpty(tagRequest.getTagInfo().rfId)) {
            tag = tagService.getTag(companyService.getCompany(getClientContext().getCompanyId()), tagRequest.getTagInfo().rfId);
        }
        if (tag == null) {
            throw notFound("tag not found");
        }
        return DataResponse.create(tag.toInfo());
    }

    @POST
    @Path("/create")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse createTag(TagRequest tagRequest) {
        populateFromContext(tagRequest);

        TagProductInfo tagProductInfo = tagRequest.getTagInfo();

        required(tagProductInfo.rfId, "tagInfo.rfId");
        required(tagProductInfo.storageId, "tagInfo.storageId");

        Tag tag = tagService.getTag(companyService.getCompany(tagProductInfo.companyId), tagProductInfo.rfId);
        if (tag == null) {
            tag = tagService.createTag(tagProductInfo);
        }

        return DataResponse.create(tag.toInfo());
    }

    @POST
    @Path("/edit")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse editTag(TagRequest tagRequest) {
        required(tagRequest.getTagInfo().id, "tagInfo.rfId");

        Tag tag = tagService.getTag(tagRequest.getTagInfo().id);
        tagService.editTag(tag, tagRequest.getTagInfo());

        return DataResponse.create(tag.toInfo());
    }

    @POST
    @Path("/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse deleteTag(TagRequest tagRequest) {
        required(tagRequest.getTagInfo().id, "tagInfo.rfId");

        Tag tag = tagService.getTag(tagRequest.getTagInfo().id);
        tagService.deleteTag(tag);

        return DataResponse.create(tag.toInfo());
    }
}