package ru.rfidcontrol.app.controllers.manager;

import ru.rfidcontrol.app.data.device.DeviceRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.device.DeviceInfoList;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.services.DeviceService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/online/device")
@AccessControl
@AllowFor({AccessTypes.MANAGER})
public class DeviceController extends BaseOnlineController {

    @Inject @NotNull protected DeviceService deviceService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findDevices(@NotNull DeviceRequest deviceRequest) {
        populateFromContext(deviceRequest);

        Map<Integer, Device> devices = deviceService.findDevices(deviceRequest.deviceInfo());

        //����������� �� ���������. �� ������ ��������
        for (Map.Entry<Integer, Device> integerDeviceEntry : devices.entrySet()) {
            Device device = integerDeviceEntry.getValue();
            if (device.getCompany().getId() != getClientContext().getCompanyId()) {
                throw forbidden();
            }
        }

        return DataResponse.create(new DeviceInfoList(devices));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getDevice(@NotNull DeviceRequest deviceRequest) {
        populateFromContext(deviceRequest);

        required(deviceRequest.deviceInfo().id, "info.id");

        Device device = deviceService.getDevice(deviceRequest.deviceInfo().id);
        if (device.getCompany().getId() != getClientContext().getCompanyId()) {
            throw forbidden();
        }

        return DataResponse.create(device.toInfo());
    }
}