package ru.rfidcontrol.core.data.product;

public class QuantityInfo {

    public SizerInfo sizer;
    public double quantity;

    public QuantityInfo() {}

    public QuantityInfo(SizerInfo sizer, double quantity) {
        this.sizer = sizer;
        this.quantity = quantity;
    }
}
