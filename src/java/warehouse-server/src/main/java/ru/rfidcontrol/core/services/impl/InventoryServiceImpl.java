package ru.rfidcontrol.core.services.impl;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.inventory.InventoryInfo;
import ru.rfidcontrol.core.data.inventory.InventoryItemInfo;
import ru.rfidcontrol.core.data.tag.TagProductInfo;
import ru.rfidcontrol.core.entity.*;
import ru.rfidcontrol.core.entity.type.InventoryItemStatus;
import ru.rfidcontrol.core.entity.type.InventoryStatus;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.*;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryServiceImpl extends ServiceBase implements InventoryService {

    @Inject StorageService storageService;
    @Inject TagService tagService;
    @Inject CompanyService companyService;
    @Inject ProductCategoryService productCategoryService;

    @Override
    @Transactional
    public Inventory startInventory(@NotNull InventoryInfo inventoryInfo) {
        Storage storage = storageService.getStorage(inventoryInfo.storageId);

        Inventory inventory = Inventory.fromInfo(inventoryInfo)
                .setStatus(InventoryStatus.ACTIVE)
                .setStorage(storage);
        save(inventory);

        initInventoryItems(inventory);
        calculateStat(inventory);

        return inventory;
    }

    @NotNull
    private Inventory initInventoryItems(@NotNull final Inventory inventory) {
        TagProductInfo searchTagInfo = new TagProductInfo() {{ storageId = inventory.getStorage().getId(); }};
        for (Map.Entry<Integer, Tag> tagEntry : tagService.findTags(searchTagInfo).entrySet()) {
            Tag tag = tagEntry.getValue();
            Product product = tag.getProduct();

            if (product == null) {
                continue;
            }

            InventoryItem inventoryItem = new InventoryItem()
                    .setTag(tag)
                    .setStatus(InventoryItemStatus.NOT_FOUND)
                    .setExpectedProductCategory(product.getProductCategory())
                    .setInventory(inventory);

            save(inventoryItem);
            inventory.getInventoryItems().add(inventoryItem);
        }

        return inventory;
    }

    @Override
    @Transactional
    public Inventory cancelInventory(@NotNull Inventory inventory) {
        assertExpectedStatuses(inventory, InventoryStatus.CREATED, InventoryStatus.ACTIVE);

        inventory
                .setStatus(InventoryStatus.CANCELLED)
                .setFinishDate(new Date());

        calculateStat(inventory);//TODO: throw event and calculate with handler

        return inventory;
    }

    private void assertExpectedStatuses(Inventory inventory, InventoryStatus... statuses) {
        boolean expectedStatusFound = false;
        for (InventoryStatus status : statuses) {
            if (status == inventory.getStatus()) {
                expectedStatusFound = true;
                break;
            }
        }
        if (!expectedStatusFound) {
            throw WarehouseException.inventoryStatusUnexpected(inventory, statuses);
        }
    }

    @Override
    @Transactional
    public Inventory finishInventory(@NotNull Inventory inventory) {
        assertExpectedStatuses(inventory, InventoryStatus.ACTIVE);

        inventory
                .setStatus(InventoryStatus.FINISHED)
                .setFinishDate(new Date());

        calculateStat(inventory);//TODO: throw event and calculate with handler

        return inventory;
    }

    @Override
    public Inventory getInventory(int inventoryId) {
        Inventory inventory = find(Inventory.class, inventoryId);
        if (inventory == null) {
            throw WarehouseException.inventoryNotFound(inventoryId);
        }
        return inventory;
    }

    @Override
    @Transactional
    public InventoryItem inventoryTagFound(Inventory inventory, TagProductInfo tagInfo, InventoryItemInfo inventoryItemInfo) {
        Tag tag = null;
        if (tagInfo.id > 0) {
            tag = tagService.getTag(tagInfo.id);
        } else if (!Strings.isNullOrEmpty(tagInfo.rfId)) {
            tag = tagService.getTag(inventory.getStorage().getCompany(), tagInfo.rfId);
        }
        if (tag == null) {
            throw WarehouseException.notFound(Tag.class, "id or rfId not specified");
        }

        InventoryItem inventoryItem = firstOrFail(
                q("from InventoryItem where inventory = :inventory and tag = :tag", InventoryItem.class)
                        .setParameter("inventory", inventory)
                        .setParameter("tag", tag)
        );

        ProductCategory actualProductCategory = productCategoryService.getProductCategory(inventoryItemInfo.actualProductCategory.id);

        inventoryItem
                .setInventDate(new Date())
                .setActualProductCategory(actualProductCategory);

        InventoryItemStatus previousStatus = inventoryItem.getStatus();
        if (inventoryItem.getExpectedProductCategory().getId() == actualProductCategory.getId()) {
            inventoryItem.setStatus(InventoryItemStatus.FOUND);
            inventory.setCountMatch(inventory.getCountMatch() + 1);
            if (previousStatus == InventoryItemStatus.CATEGORY_CONFLICT) {
                inventory.setCountMatch(inventory.getCountCategoryConflict() - 1);
            } else if (previousStatus == InventoryItemStatus.NOT_FOUND) {
                inventory.setCountNotFound(inventory.getCountNotFound() - 1);
            }
        } else {
            inventoryItem.setStatus(InventoryItemStatus.CATEGORY_CONFLICT);
            inventory.setCountCategoryConflict(inventory.getCountCategoryConflict() + 1);
            if (previousStatus == InventoryItemStatus.FOUND) {
                inventory.setCountMatch(inventory.getCountMatch() - 1);
            } else if (previousStatus == InventoryItemStatus.NOT_FOUND) {
                inventory.setCountNotFound(inventory.getCountNotFound() - 1);
            }
        }

        return inventoryItem;
    }

    @Override
    public Map<Integer, Inventory> findInventories(@NotNull InventoryInfo inventoryInfo) {
        Map<Integer, Inventory> result = new HashMap<>();

        if (inventoryInfo.id > 0) {
            Inventory inventory = findInventory(inventoryInfo.id);
            if (inventory != null) {
                result.put(inventory.getId(), inventory);
            }
        }

        //���� ������ storageId, ���� ������ �� ����. companyId ����� �� ���������
        if (inventoryInfo.storageId > 0) {
            Storage storage = storageService.getStorage(inventoryInfo.storageId);
            for (Inventory inventory: findInventoriesByStorage(storage)) {
                result.put(inventory.getId(), inventory);
            }
        } else if (inventoryInfo.companyId > 0) {
            //������� ��� ��������� ��� ���� ������� ��������
            Company company = companyService.getCompany(inventoryInfo.companyId);
            for (Storage storage : company.getStorages()) {
                for (Inventory inventory: findInventoriesByStorage(storage)) {
                    result.put(inventory.getId(), inventory);
                }
            }
        }

        return result;
    }

    @Nullable
    public Inventory findInventory(int inventoryId) {
        return find(Inventory.class, inventoryId);
    }


    private void calculateStat(Inventory inventory) {
        int countNotFound = 0;
        int countCategoryConflict = 0;
        int countMatch = 0;

        //todo: use sql
        for (InventoryItem inventoryItem : inventory.getInventoryItems()) {
            switch (inventoryItem.getStatus()) {
                case NOT_FOUND:
                    countNotFound++;
                    break;
                case CATEGORY_CONFLICT:
                    countCategoryConflict++;
                    break;
                case FOUND:
                    countMatch++;
                    break;
            }
        }

        if (inventory.getCountNotFound() != countNotFound) {
            //normally these values are equals.
            inventory.setCountNotFound(countNotFound);
        }

        if (inventory.getCountCategoryConflict() != countCategoryConflict) {
            //normally these values are equals.
            inventory.setCountCategoryConflict(countCategoryConflict);
        }

        if (inventory.getCountMatch() != countMatch) {
            //normally these values are equals.
            inventory.setCountMatch(countMatch);
        }
    }

    @NotNull
    private List<Inventory> findInventoriesByStorage(@NotNull Storage storage) {
        return em().createQuery("from Inventory where storage = :storage", Inventory.class)
                .setParameter("storage", storage)
                .getResultList();
    }
}