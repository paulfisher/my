package ru.rfidcontrol.core.data.storage;

import ru.rfidcontrol.core.data.device.DeviceInfoList;
import ru.rfidcontrol.core.data.inventory.InventoryInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Storage;

import java.util.Date;

public class StorageInfo {

    public int id;
    public String name;
    public Date createTime;
    public boolean isDeleted;
    public String reference;

    public int companyId = 0;
    public String companyName = "";

    public InventoryInfo inventory;
    public DeviceInfoList devices;

    public StorageInfo() {}

    public StorageInfo(Storage storage) {
        id = storage.getId();
        name = storage.getName();
        createTime = storage.getCreateTime();
        isDeleted = storage.isDeleted();
        reference = storage.getReference();

        Company company = storage.getCompany();
        companyId = company.getId();
        companyName = company.getName();

        devices = new DeviceInfoList(storage.getDevices());
    }
}
