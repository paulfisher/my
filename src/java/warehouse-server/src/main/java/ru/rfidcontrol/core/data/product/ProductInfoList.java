package ru.rfidcontrol.core.data.product;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.Product;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class ProductInfoList extends BaseInfoList {

    @Nullable public Map<Integer, ProductInfo> products;

    public int size;

    public ProductInfoList() {
    }

    public ProductInfoList(Map<Integer, Product> products) {
        this.products = new HashMap<>();
        for (Map.Entry<Integer, Product> product : products.entrySet()) {
            this.products.put(product.getKey(), new ProductInfo(product.getValue()));
        }
        this.size = this.products.size();
    }

}
