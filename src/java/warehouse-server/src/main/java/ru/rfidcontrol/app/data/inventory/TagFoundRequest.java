package ru.rfidcontrol.app.data.inventory;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.inventory.InventoryItemInfo;
import ru.rfidcontrol.core.data.tag.TagProductInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class TagFoundRequest extends BaseRequest {

    @Nullable public InventoryItemInfo inventoryItem;//required: actualProductCategory.id
    @Nullable public TagProductInfo tag;//required: tag.rfId

    @NotNull
    public InventoryItemInfo getInventoryItem() {
        if (inventoryItem == null) {
            inventoryItem = new InventoryItemInfo();
        }
        return inventoryItem;
    }

    @NotNull
    public TagProductInfo getTag() {
        if (tag == null) {
            tag = new TagProductInfo();
        }
        return tag;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        getInventoryItem().storageId = clientContext.getStorageId();

        getTag().companyId = clientContext.getCompanyId();
        getTag().storageId = clientContext.getStorageId();
    }
}