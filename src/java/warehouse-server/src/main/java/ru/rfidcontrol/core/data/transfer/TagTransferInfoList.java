package ru.rfidcontrol.core.data.transfer;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.TagTransfer;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagTransferInfoList extends BaseInfoList {

    @Nullable public Map<Integer, TagTransferInfo> tagTransfers;

    public int size;

    public TagTransferInfoList() {
    }

    public TagTransferInfoList(List<TagTransfer> tagTransfers) {
        this.tagTransfers = new HashMap<>();
        for (TagTransfer tagTransfer : tagTransfers) {
            this.tagTransfers.put(tagTransfer.getId(), new TagTransferInfo(tagTransfer));
        }
        this.size = this.tagTransfers.size();
    }

    @NotNull
    public Map<Integer, TagTransferInfo> getTagTransfers() {
        if (tagTransfers == null) {
            tagTransfers = new HashMap<>();
        }
        return tagTransfers;
    }
}
