package ru.rfidcontrol.core.annotations;

public final class AccessTypes {
    public final static String ADMIN = "admin";
    public final static String DEVICE = "device";
    public final static String MANAGER = "manager";
    public final static String AUTHORIZED = "authorized";
    public final static String C1 = "1c";
}
