package ru.rfidcontrol.app.data.company;

import ru.rfidcontrol.core.data.company.CompanyInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class CompanyRequest {

    @Nullable
    public CompanyInfo info;

    @NotNull
    public CompanyInfo companyInfo() {
        if (info == null) {
            info = new CompanyInfo();
        }
        return info;
    }
}
