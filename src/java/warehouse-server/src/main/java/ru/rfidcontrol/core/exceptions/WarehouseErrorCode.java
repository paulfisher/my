package ru.rfidcontrol.core.exceptions;

import javax.ws.rs.core.Response;

public enum WarehouseErrorCode {

    NOT_FOUND(100, Response.Status.NOT_FOUND),
    COMPANY_NOT_FOUND(101, Response.Status.NOT_FOUND),
    USER_NOT_FOUND(102, Response.Status.NOT_FOUND),
    DEVICE_NOT_FOUND(103, Response.Status.NOT_FOUND),
    INVENTORY_NOT_FOUND(104, Response.Status.NOT_FOUND),
    TRANSFER_NOT_FOUND(105, Response.Status.NOT_FOUND),


    DEFAULT(0, Response.Status.INTERNAL_SERVER_ERROR),
    MISSING_PARAMETER(1, Response.Status.BAD_REQUEST),
    MISSING_CONTEXT(2, Response.Status.BAD_REQUEST),

    USER_LOGIN_FAIL(200, Response.Status.NOT_FOUND),
    USER_IS_DELETED(201, Response.Status.FORBIDDEN),

    PRODUCT_CATEGORY_CYCLE(300, Response.Status.CONFLICT),
    INVENTORY_STATUS_UNEXPECTED(301, Response.Status.CONFLICT),
    PRODUCT_CATEGORY_NOT_FOUND(302, Response.Status.NOT_FOUND);


    public final int value;
    public final Response.Status status;

    WarehouseErrorCode(int value, Response.Status status) {
        this.value = value;
        this.status = status;
    }
}
