package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.ClientContext;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Method;

public interface AccessControlService {

    boolean hasAccess(@NotNull ClientContext context, @NotNull Class<?> controller, @NotNull Method action);

}
