package ru.rfidcontrol.core.entity.type;

import static ru.rfidcontrol.core.exceptions.WarehouseException.notFound;

public enum InventoryStatus {

    CREATED(0),
    ACTIVE(1),
    FINISHED(2),
    CANCELLED(3),
    DELETED(4);

    public final int value;

    InventoryStatus(int value) {
        this.value = value;
    }

    public static InventoryStatus get(int value) {
        for (InventoryStatus status : values()) {
            if (status.value == value) {
                return status;
            }
        }
        throw notFound(InventoryStatus.class, value);
    }
}
