package ru.rfidcontrol.app.data.product;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.data.product.ProductInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class ProductRequest extends BaseRequest {

    @Nullable public ProductInfo productInfo;

    public ProductRequest() {
        this.productInfo = new ProductInfo();
    }

    @NotNull
    public ProductInfo getProductInfo() {
        return productInfo;
    }
}
