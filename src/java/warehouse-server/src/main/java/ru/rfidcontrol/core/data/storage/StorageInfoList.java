package ru.rfidcontrol.core.data.storage;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.Storage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StorageInfoList extends BaseInfoList {

    public final Map<Integer, StorageInfo> storages;
    public final int size;

    public StorageInfoList(Map<Integer, Storage> storages) {
        this.storages = new HashMap<>();
        for (Map.Entry<Integer, Storage> storageEntry : storages.entrySet()) {
            this.storages.put(storageEntry.getKey(), new StorageInfo(storageEntry.getValue()));
        }
        this.size = this.storages.size();
    }


    public StorageInfoList(List<Storage> storages) {
        this.storages = new HashMap<>();
        for (Storage storage : storages) {
            this.storages.put(storage.getId(), new StorageInfo(storage));
        }
        this.size = this.storages.size();
    }
}