package ru.rfidcontrol.core.data.device;

import ru.rfidcontrol.core.data.company.CompanyInfo;
import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.data.user.UserInfo;

public class InitInfo {
    public CompanyInfo companyInfo;
    public StorageInfo storageInfo;
    public UserInfo userInfo;
    public DeviceInfo deviceInfo;
}
