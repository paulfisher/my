package ru.rfidcontrol.app.filters;

import com.google.common.base.Strings;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CorsResponseFilter implements ContainerResponseFilter {

    private static final List<String> allowedDomains =
            Arrays.asList(
                    //����� �������
                    "http://admin.rfidcontrol.ru",
                    "http://terminal.rfidcontrol.ru",
                    "http://online.rfidcontrol.ru",

                    //���������
                    "http://admin.rfidcontrol.loc",
                    "http://terminal.rfidcontrol.loc",
                    "http://online.rfidcontrol.loc");

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        MultivaluedMap<String, Object> headers = responseContext.getHeaders();

        String origin = requestContext.getHeaderString("Origin");
        if (!Strings.isNullOrEmpty(origin) && allowedDomains.contains(origin)) {
            headers.add("Access-Control-Allow-Origin", origin);
            headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
            headers.add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia");
            headers.add("Access-Control-Allow-Credentials", "true");
        }
    }
}
