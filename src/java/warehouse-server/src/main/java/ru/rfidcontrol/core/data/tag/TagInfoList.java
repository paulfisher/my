package ru.rfidcontrol.core.data.tag;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.Tag;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagInfoList extends BaseInfoList {

    @Nullable public Map<Integer, TagInfo> tags;

    public int size;

    @NotNull
    public Map<Integer, TagInfo> getTags() {
        if (tags == null) {
            tags = new HashMap<>();
        }
        return tags;
    }

    public TagInfoList(List<Tag> tags) {
        this.tags = new HashMap<>();
        for (Tag tag : tags) {
            this.tags.put(tag.getId(), new TagInfo(tag));
        }
        this.size = this.tags.size();
    }

    public TagInfoList() {
        this(new ArrayList<Tag>());
    }
}
