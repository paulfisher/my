package ru.rfidcontrol.app.controllers.admin;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.product.ProductCategoryRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;
import ru.rfidcontrol.core.data.product.ProductCategoryInfoList;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.services.ProductCategoryService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/admin/product/category")
@AccessControl
@AllowFor({AccessTypes.ADMIN})
public class ProductCategoryController extends BaseController {

    @Inject @NotNull protected ProductCategoryService productCategoryService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findProductCategories(@NotNull ProductCategoryRequest findProductCategoryRequest) {

        required(findProductCategoryRequest.getProductCategoryInfo().companyId, "productCategoryInfo.companyId");

        Map<Integer, ProductCategory> products = productCategoryService.findProductCategories(findProductCategoryRequest.getProductCategoryInfo(), findProductCategoryRequest.pagination);

        return DataResponse.create(new ProductCategoryInfoList(products));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getProductCategory(@NotNull ProductCategoryRequest productCategoryRequest) {
        required(productCategoryRequest.getProductCategoryInfo().id, "productCategoryInfo.id");

        ProductCategory category = productCategoryService.getProductCategory(productCategoryRequest.getProductCategoryInfo().id);

        return DataResponse.create(category.toInfo());
    }


    @POST
    @Path("/create")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @AllowFor({AccessTypes.AUTHORIZED})
    public DataResponse createProductCategory(ProductCategoryRequest productCategoryRequest) {
        ProductCategory productCategory = productCategoryService.createProductCategory(productCategoryRequest.getProductCategoryInfo());

        return DataResponse.create(new ProductCategoryInfo(productCategory));
    }

    @POST
    @Path("/edit")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse editProductCategory(ProductCategoryRequest productRequest) {
        required(productRequest.getProductCategoryInfo().id, "productCategoryInfo.id");

        ProductCategory productCategory = productCategoryService.getProductCategory(productRequest.getProductCategoryInfo().id);
        productCategoryService.editProductCategory(productCategory, productRequest.getProductCategoryInfo());

        return DataResponse.create(productCategory.toInfo());
    }

    @POST
    @Path("/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse deleteProductCategory(ProductCategoryRequest productRequest) {
        required(productRequest.getProductCategoryInfo().id, "productCategoryInfo.id");

        ProductCategory productCategory = productCategoryService.getProductCategory(productRequest.getProductCategoryInfo().id);
        productCategoryService.deleteProductCategory(productCategory);

        return DataResponse.create(productCategory.toInfo());
    }
}