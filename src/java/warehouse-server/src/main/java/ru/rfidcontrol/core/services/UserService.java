package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.user.UserInfo;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.User;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface UserService {

    @NotNull User getUser(int id);

    @Nullable User findUser(int id);

    @Nullable User findUser(String name);

    @NotNull Map<Integer, User> findUsers(UserInfo userInfo);

    @NotNull User createUser(@NotNull UserInfo userInfo);

    @NotNull User editUser(@NotNull User user, @NotNull UserInfo userInfo);

    @NotNull User deleteUser(@NotNull User user);

    void setDevice(User user, Device device);
}
