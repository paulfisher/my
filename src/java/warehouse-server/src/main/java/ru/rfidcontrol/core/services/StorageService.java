package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.entity.Storage;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface StorageService {

    @NotNull Storage getStorage(int id);

    @Nullable Storage findStorage(int id);

    @NotNull Map<Integer, Storage> findStorages(@NotNull StorageInfo storageInfo);

    @NotNull Storage createStorage(@NotNull StorageInfo storageInfo);

    @NotNull Storage editStorage(@NotNull Storage storage, @NotNull StorageInfo storageInfo);

    @NotNull Storage deleteStorage(@NotNull Storage storage);

    @NotNull Storage getStorage(@NotNull String reference);

    @Nullable Storage findStorage(@NotNull String reference);
}
