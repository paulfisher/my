package ru.rfidcontrol.app.controllers.manager;

import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.storage.StorageRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.storage.StorageInfoList;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.services.StorageService;
import ru.rfidcontrol.core.services.TagService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/online/storage")
@AccessControl
@AllowFor({AccessTypes.MANAGER})
public class StorageController extends BaseOnlineController {

    @Inject @NotNull protected StorageService storageService;
    @Inject @NotNull protected TagService tagService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findStorages(@NotNull StorageRequest storageRequest) {
        populateFromContext(storageRequest);

        Map<Integer, Storage> storages = storageService.findStorages(storageRequest.storageInfo());

        //����������� �� ���������. �� ������ ��������
        for (Map.Entry<Integer, Storage> integerStorageEntry : storages.entrySet()) {
            Storage storage = integerStorageEntry.getValue();
            if (storage.getCompany().getId() != getClientContext().getCompanyId()) {
                throw forbidden();
            }
        }

        return DataResponse.create(new StorageInfoList(storages));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getStorage(@NotNull StorageRequest storageRequest) {
        populateFromContext(storageRequest);

        required(storageRequest.storageInfo().id, "info.id");

        Storage storage = storageService.getStorage(storageRequest.storageInfo().id);
        if (storage.getCompany().getId() != getClientContext().getCompanyId()) {
            throw forbidden();
        }

        return DataResponse.create(storage.toInfo());
    }
}