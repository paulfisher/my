package ru.rfidcontrol.app.controllers.manager;

import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.tag.TagRequest;
import ru.rfidcontrol.app.data.transfer.SearchTransferRequest;
import ru.rfidcontrol.app.data.transfer.TransferRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.transfer.TagTransferInfo;
import ru.rfidcontrol.core.data.transfer.TagTransferInfoList;
import ru.rfidcontrol.core.data.transfer.TransferInfoList;
import ru.rfidcontrol.core.entity.Tag;
import ru.rfidcontrol.core.entity.TagTransfer;
import ru.rfidcontrol.core.entity.Transfer;
import ru.rfidcontrol.core.services.TagService;
import ru.rfidcontrol.core.services.TransferService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("/online/transfer")
@AccessControl
@AllowFor({AccessTypes.MANAGER})
public class TransferController extends BaseOnlineController {

    @Inject @NotNull protected TransferService transferService;
    @Inject @NotNull protected TagService tagService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findTransfers(@NotNull SearchTransferRequest transferRequest) {
        populateFromContext(transferRequest);

        Map<Integer, Transfer> transfers = transferService.findTransfers(transferRequest.getTransferInfo(), transferRequest.pagination, transferRequest.sorting);
        for (Map.Entry<Integer, Transfer> integerTransferEntry : transfers.entrySet()) {
            Transfer transfer = integerTransferEntry.getValue();
            if (transfer.getStorage().getCompany().getId() != getClientContext().getCompanyId()) {
                throw forbidden();
            }
        }

        return DataResponse.create(new TransferInfoList(transfers).setPagination(transferRequest.pagination));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getTransfer(@NotNull TransferRequest transferRequest) {
        populateFromContext(transferRequest);

        required(transferRequest.getTransferInfo().id, "transferInfo.id");

        Transfer transfer = transferService.getTransfer(transferRequest.getTransferInfo().id);
        if (transfer.getStorage().getCompany().getId() != getClientContext().getCompanyId()) {
            throw forbidden();
        }

        return DataResponse.create(transfer.toInfo());
    }


    @POST
    @Path("/findTag")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findTag(@NotNull TagRequest tagRequest) {
        populateFromContext(tagRequest);

        Tag tag = tagService.getTag(tagRequest.getTagInfo().id);

        List<TagTransfer> tagTransfers = transferService.getTagTransfers(tag, tagRequest.pagination);

        return DataResponse.create(new TagTransferInfoList(tagTransfers).setPagination(tagRequest.pagination));
    }

    @POST
    @Path("/register")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse register(@NotNull TransferRequest transferRequest) {

        populateFromContext(transferRequest);

        required(transferRequest.getTransferInfo().storageId, "transferInfo.storageId");

        Transfer transfer = transferService.createTransfer(transferRequest.getTransferInfo());
        for (Map.Entry<Integer, TagTransferInfo> tagTransferInfoEntry : transferRequest.getTransferInfo().getTagInfoList().getTagTransfers().entrySet()) {
            transferService.createTagTransfer(
                    transfer,
                    tagService.getTag(tagTransferInfoEntry.getValue().getTagInfo().id),
                    tagTransferInfoEntry.getValue().priceInfo,
                    tagTransferInfoEntry.getValue().quantityInfo);
        }

        return DataResponse.create(transfer.toInfo());
    }
}