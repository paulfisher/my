package ru.rfidcontrol.app.data.inventory;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.inventory.InventoryInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class InventoryRequest extends BaseRequest {

    @Nullable public InventoryInfo inventoryInfo;

    @NotNull
    public InventoryInfo getInventoryInfo() {
        if (inventoryInfo == null) {
            inventoryInfo = new InventoryInfo();
        }
        return inventoryInfo;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        if (clientContext.getStorageId() > 0) {
            getInventoryInfo().storageId = clientContext.getStorageId();
        }
        getInventoryInfo().companyId = clientContext.getCompanyId();
    }
}
