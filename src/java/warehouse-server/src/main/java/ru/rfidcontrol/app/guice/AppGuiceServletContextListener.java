package ru.rfidcontrol.app.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistFilter;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import ru.rfidcontrol.app.config.ServerConfig;
import ru.rfidcontrol.app.config.ServerConfigLoader;
import ru.rfidcontrol.app.services.LoginService;
import ru.rfidcontrol.app.services.impl.LoginServiceImpl;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.services.AccessControlService;
import ru.rfidcontrol.core.services.impl.AccessControlServiceImpl;

public class AppGuiceServletContextListener extends GuiceServletContextListener {

    public static Injector injector;

    @Override
    protected Injector getInjector() {
        final ServerConfig config = ServerConfigLoader.load("config");
        injector = Guice.createInjector(
                new ServletModule() {
                    @Override
                    protected void configureServlets() {
                        filter("/*").through(PersistFilter.class);

                        //specifics for non-TEST environment
                        bind(ClientContext.class).toProvider(ClientContextProvider.class);
                        bind(AccessControlService.class).to(AccessControlServiceImpl.class).asEagerSingleton();
                        bind(LoginService.class).to(LoginServiceImpl.class).asEagerSingleton();
                    }
                },
                new ApplicationModule(config));

        return injector;
    }
}
