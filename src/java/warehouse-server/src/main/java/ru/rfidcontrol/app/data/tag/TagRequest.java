package ru.rfidcontrol.app.data.tag;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.tag.TagProductInfo;

import javax.validation.constraints.NotNull;

public class TagRequest extends BaseRequest {

    @NotNull final public TagProductInfo tagInfo;

    public TagRequest() {
        tagInfo = new TagProductInfo();
    }

    @NotNull
    public TagProductInfo getTagInfo() {
        return tagInfo;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        if (tagInfo.storageId == 0) {
            //������������� ���. ���� ���-������ ��������� storageId != clientContext.storageId
            tagInfo.storageId = clientContext.getStorageId();
        }
        tagInfo.companyId = clientContext.getCompanyId();
    }
}
