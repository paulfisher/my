package ru.rfidcontrol.app.config;


import com.google.common.base.MoreObjects;
import com.owlike.genson.annotation.JsonProperty;
import ru.rfidcontrol.app.config.hibernate.HibernateProperties;

import javax.annotation.Nonnull;
import java.util.Map;

public class DbConfig {
    @Nonnull @JsonProperty
    private String url;

    @Nonnull @JsonProperty
    private String user;

    @Nonnull @JsonProperty
    private String password;

    @Nonnull @JsonProperty
    private String jpaUnitName;

    @Nonnull @JsonProperty
    private String hibernatePropertiesClass;

    @Nonnull public String getUrl() {
        return url;
    }

    @Nonnull public String getUser() {
        return user;
    }

    @Nonnull public String getPassword() {
        return password;
    }

    @Nonnull public String getJpaUnitName() {
        return jpaUnitName;
    }

    @Nonnull public String getHibernatePropertiesClass() {
        return hibernatePropertiesClass;
    }

    @Nonnull
    public Map<String, String> getHibernateProperties() {
        try {
            Class<?> propertiesClass = Class.forName(getHibernatePropertiesClass());
            if (!HibernateProperties.class.isAssignableFrom(propertiesClass)) {
                throw new ConfigurationException(String.format("Class '%s' doesn't implement '%s'", propertiesClass, HibernateProperties.class));
            }
            return ((HibernateProperties) propertiesClass.newInstance()).constructProperties(this);
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
            throw new ConfigurationException(String.format("Failed to get HibernateProperties ('%s'): %s", getHibernateProperties(), e.getMessage()), e);
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("url", url)
                .add("user", user)
                .add("jpaUnitName", jpaUnitName)
                .add("hibernatePropertiesClass", hibernatePropertiesClass)
                .toString();
    }

}
