package ru.rfidcontrol.core.services.impl;

import com.google.common.base.Strings;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.device.DeviceInfo;
import ru.rfidcontrol.core.entity.*;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.DeviceService;
import ru.rfidcontrol.core.services.ServiceBase;
import ru.rfidcontrol.core.services.StorageService;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceServiceImpl extends ServiceBase implements DeviceService {

    @Inject @NotNull protected StorageService storageService;
    @Inject @NotNull protected CompanyService companyService;

    @Override
    @Transactional
    public Device createDevice(@NotNull DeviceInfo deviceInfo) {
        Company company = companyService.getCompany(deviceInfo.companyId);
        Device device = Device.fromInfo(deviceInfo)
                .setCompany(company);

        if (deviceInfo.storageId > 0) {
            device.setStorage(storageService.getStorage(deviceInfo.storageId));
        }

        validate(device);
        save(device);

        return device;
    }

    @Override
    @Transactional
    public Device editDevice(Device device, DeviceInfo deviceInfo) {
        Company company = companyService.getCompany(deviceInfo.companyId);
        if (deviceInfo.storageId > 0) {
            Storage storage = storageService.getStorage(deviceInfo.storageId);
            device.setStorage(storage);
        }

        //������������� �������� � ������ ��������, ����� �������� ��������
        if (device.getCompany().getId() != company.getId()) {

            //�������� �������� transfer-�� � ���������
            for (Transfer transfer : device.getTransfers()) {
                transfer.setDevice(null);
                save(transfer);
            }

            //������� ����������� �������������
            device.setUsers(new ArrayList<User>());
            for (User user : device.getUsers()) {
                user.getDevices().remove(device);
                save(user);
            }
            device.setUsers(new ArrayList<User>());

            //��������� �������� �� ������
            device.setStorage(null);
        }

        device.setName(deviceInfo.name)
                .setImeiCode(deviceInfo.imeiCode)
                .setDeleted(deviceInfo.isDeleted)
                .setCompany(company);

        validate(device);
        return device;
    }

    @Nullable
    @Override
    public Device findDevice(int id) {
        return find(Device.class, id);
    }

    @Nullable
    @Override
    public Device findDevice(@NotNull String imeiCode) {
        return first(em().createQuery("from Device where imeiCode = :imeiCode", Device.class)
                .setParameter("imeiCode", imeiCode));
    }


    @NotNull
    @Override
    public Map<Integer, Device> findDevices(@NotNull DeviceInfo deviceInfo) {
        Map<Integer, Device> result = new HashMap<>();

        if (deviceInfo.id > 0) {
            Device device = findDevice(deviceInfo.id);
            if (device != null) {
                result.put(device.getId(), device);
            }
        }
        if (!Strings.isNullOrEmpty(deviceInfo.name)) {
            for (Device device : findDevicesByName(deviceInfo.name)) {
                result.put(device.getId(), device);
            }
        }
        if (!Strings.isNullOrEmpty(deviceInfo.imeiCode)) {
            Device device = findDevice(deviceInfo.imeiCode);
            if (device != null) {
                result.put(device.getId(), device);
            }
        }
        if (deviceInfo.companyId > 0) {
            Company company = companyService.findCompany(deviceInfo.companyId);
            if (company != null) {
                for (Device device : company.getDevices()) {
                    result.put(device.getId(), device);
                }
            }
        }
        if (deviceInfo.storageId > 0) {
            Storage storage = storageService.findStorage(deviceInfo.storageId);
            if (storage != null) {
                for (Device device : storage.getDevices()) {
                    result.put(device.getId(), device);
                }
            }
        }

        return result;
    }

    @Override
    @Transactional
    public Device deleteDevice(Device device) {
        return device.setDeleted(true);
    }

    @Override
    @Transactional
    public Device setStorage(Device device, Storage storage) {
        return device.setStorage(storage);
    }

    @Override
    public Device getDevice(int deviceId) {
        Device device = findDevice(deviceId);
        if (device == null) {
            throw WarehouseException.deviceNotFound(deviceId);
        }
        return device;
    }

    @Override
    public Device getDevice(String imeiCode) {
        Device device = findDevice(imeiCode);
        if (device == null) {
            throw WarehouseException.deviceNotFound(imeiCode);
        }
        return device;
    }

    @NotNull
    private List<Device> findDevicesByName(@NotNull String name) {
        return em().createQuery("from Device where lower(name) like :name", Device.class)
                .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                .getResultList();
    }

    private List<Device> findDevices(boolean isDeleted) {
        return em().createQuery("from Device where isDeleted = :isDeleted", Device.class)
                .setParameter("isDeleted", isDeleted)
                .getResultList();
    }

    private void validate(Device device) {
        if (device.getStorage() != null && device.getStorage().getCompany().getId() != device.getCompany().getId()) {
            throw WarehouseException.create("device storage and company conflict");
        }
    }
}
