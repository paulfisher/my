package ru.rfidcontrol.app.controllers.manager;

import ru.rfidcontrol.app.data.inventory.InventoryRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.inventory.InventoryInfo;
import ru.rfidcontrol.core.data.inventory.InventoryInfoList;
import ru.rfidcontrol.core.entity.Inventory;
import ru.rfidcontrol.core.services.InventoryService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/online/inventory")
@AccessControl
@AllowFor({AccessTypes.MANAGER})
public class InventoryController extends BaseOnlineController {

    @Inject @NotNull protected InventoryService inventoryService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findInventories(@NotNull InventoryRequest inventoryRequest) {
        populateFromContext(inventoryRequest);

        Map<Integer, Inventory> inventories = inventoryService.findInventories(inventoryRequest.getInventoryInfo());

        //����������� �� ���������. �� ������ ��������
        for (Map.Entry<Integer, Inventory> integerInventoryEntry : inventories.entrySet()) {
            Inventory inventory = integerInventoryEntry.getValue();
            if (inventory.getStorage().getCompany().getId() != getClientContext().getCompanyId()) {
                throw forbidden();
            }
        }

        return DataResponse.create(new InventoryInfoList(inventories));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getInventory(@NotNull InventoryRequest inventoryRequest) {
        populateFromContext(inventoryRequest);

        required(inventoryRequest.getInventoryInfo().id, "inventoryInfo.id");

        Inventory inventory = inventoryService.getInventory(inventoryRequest.getInventoryInfo().id);
        if (inventory.getStorage().getCompany().getId() != getClientContext().getCompanyId()) {
            throw forbidden();
        }

        return DataResponse.create(inventory.toInfo());
    }

    @POST
    @Path("/cancel")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse cancel(@NotNull InventoryRequest inventoryRequest) {
        return DataResponse.create(
                new InventoryInfo(
                        inventoryService.cancelInventory(
                                inventoryService.getInventory(inventoryRequest.getInventoryInfo().id)
                        )
                )
        );
    }

    @POST
    @Path("/finish")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse finish(@NotNull InventoryRequest inventoryRequest) {
        return DataResponse.create(
                new InventoryInfo(
                        inventoryService.finishInventory(
                                inventoryService.getInventory(inventoryRequest.getInventoryInfo().id)
                        )
                )
        );
    }
}