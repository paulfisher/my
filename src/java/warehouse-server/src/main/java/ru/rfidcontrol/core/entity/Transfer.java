package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import ru.rfidcontrol.core.data.transfer.TransferInfo;
import ru.rfidcontrol.core.entity.type.TransferDirection;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "transfer")
public class Transfer {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "direction")
    private int direction;

    @Column(name = "bill_number")
    private String billNumber;

    @Generated(GenerationTime.INSERT)
    @Column(name = "\"when\"", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date when;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "storage_id")
    private Storage storage;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "mover_user_id")
    private User user;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "mover_device_id")
    private Device device;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;

    @OneToMany(mappedBy = "transfer")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private List<TagTransfer> tagTransfers;

    public Transfer() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Transfer setId(int id) {
        this.id = id;
        return this;
    }

    @NotNull
    public TransferDirection getDirection() {
        return TransferDirection.get(direction);
    }

    @NotNull
    public Transfer setDirection(@NotNull TransferDirection direction) {
        this.direction = direction.value;
        return this;
    }

    @Nullable
    public String getBillNumber() {
        return billNumber;
    }

    @NotNull
    public Transfer setBillNumber(@Nullable String billNumber) {
        this.billNumber = billNumber;
        return this;
    }

    @NotNull
    public Date getWhen() {
        return when;
    }

    @NotNull
    public Transfer setWhen(@NotNull Date when) {
        this.when = when;
        return this;
    }

    @NotNull
    public Storage getStorage() {
        return storage;
    }

    @NotNull
    public Transfer setStorage(@NotNull Storage storage) {
        this.storage = storage;
        return this;
    }

    @Nullable
    public User getUser() {
        return user;
    }

    @NotNull
    public Transfer setUser(@Nullable User user) {
        this.user = user;
        return this;
    }

    @Nullable
    public Device getDevice() {
        return device;
    }

    @NotNull
    public Transfer setDevice(@Nullable Device device) {
        this.device = device;
        return this;
    }

    @Nullable
    public Supplier getSupplier() {
        return supplier;
    }

    @NotNull
    public Transfer setSupplier(@Nullable Supplier supplier) {
        this.supplier = supplier;
        return this;
    }

    @NotNull
    public List<TagTransfer> getTagTransfers() {
        if (tagTransfers == null) {
            tagTransfers = new ArrayList<>();
        }
        return tagTransfers;
    }

    @NotNull
    public Transfer setTagTransfers(@NotNull List<TagTransfer> tagTransfers) {
        if (this.tagTransfers == null) {
            this.tagTransfers = new ArrayList<>();
        }
        this.tagTransfers.clear();
        this.tagTransfers.addAll(tagTransfers);
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .toString();
    }

    @NotNull
    public static Transfer fromInfo(@NotNull TransferInfo transferInfo) {
        return new Transfer()
                .setBillNumber(transferInfo.billNumber)
                .setDirection(TransferDirection.get(transferInfo.directionType));
    }

    public TransferInfo toInfo() {
        return new TransferInfo(this);
    }
}
