package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.data.company.SupplierInfo;
import ru.rfidcontrol.core.data.user.UserInfo;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "supplier")
public class Supplier {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    public Supplier() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Supplier setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    @NotNull
    public Supplier setName(String name) {
        this.name = name;
        return this;
    }

    @NotNull
    public Company getCompany() {
        return company;
    }

    public Supplier setCompany(@Nullable Company company) {
        this.company = company;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("company", company.getId())
                .toString();
    }

    public static Supplier fromInfo(SupplierInfo supplierInfo) {
        return new Supplier()
                .setName(supplierInfo.name);
    }

    public SupplierInfo toInfo() {
        return new SupplierInfo(this);
    }
}