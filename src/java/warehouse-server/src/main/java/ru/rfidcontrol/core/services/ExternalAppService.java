package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.entity.type.ExternalApp;

import javax.validation.constraints.NotNull;

public interface ExternalAppService {

    @NotNull ExternalApp getByToken(@NotNull String token);

}
