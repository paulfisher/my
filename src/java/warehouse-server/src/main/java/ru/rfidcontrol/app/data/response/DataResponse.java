package ru.rfidcontrol.app.data.response;

import javax.validation.constraints.NotNull;

public class DataResponse {

    public Object data;
    public DataResponseStatus status;

    @NotNull
    public static DataResponse create(Object data) {
        DataResponse response = new DataResponse();
        response.data = data;
        response.status = DataResponseStatus.SUCCESS;
        return response;
    }

    @NotNull
    public static DataResponse error(@NotNull String message) {
        DataResponse response = new DataResponse();
        response.status = DataResponseStatus.ERROR;
        response.data = message;
        return response;
    }
}
