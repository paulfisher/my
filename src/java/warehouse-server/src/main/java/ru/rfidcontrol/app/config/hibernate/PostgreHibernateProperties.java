package ru.rfidcontrol.app.config.hibernate;

import ru.rfidcontrol.app.config.DbConfig;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;


public class PostgreHibernateProperties implements HibernateProperties {

    @Override
    @Nonnull
    @SuppressWarnings("SpellCheckingInspection")
    public Map<String, String> constructProperties(@Nonnull DbConfig dbConfig) {
        Map<String, String> properties = new HashMap<>();

        properties.put("hibernate.archive.autodetection", "class");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL82Dialect");
        properties.put("hibernate.connection.charSet", "UTF-8");
        properties.put("hibernate.connection.characterEncoding", "utf8");

        properties.put("hibernate.bytecode.use_reflection_optimizer", "false");
        properties.put("hibernate.connection.driver_class", "org.postgresql.Driver");
        properties.put("hibernate.connection.url", dbConfig.getUrl());
        properties.put("hibernate.connection.username", dbConfig.getUser());
        properties.put("hibernate.connection.password", dbConfig.getPassword());
        properties.put("hibernate.current_session_context_class", "thread");
        properties.put("hibernate.default_schema", "public");

        return properties;
    }
}
