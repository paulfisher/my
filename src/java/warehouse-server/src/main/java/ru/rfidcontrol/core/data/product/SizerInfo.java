package ru.rfidcontrol.core.data.product;

import ru.rfidcontrol.core.entity.Sizer;

public class SizerInfo {
    public int id;
    public String name;
    public int okeiCode;
    public String shortName;

    public SizerInfo() {}

    public SizerInfo(Sizer sizer) {
        id = sizer.getId();
        name = sizer.getName();
        okeiCode = sizer.getOkeiCode();
        shortName = sizer.getShortName();
    }
}