package ru.rfidcontrol.core.data.tag;

import java.util.List;

public class SearchTagsResultInfo {

    public TagProductInfoList foundTags;
    public List<String> requestedRfIds;
    public TagProductInfo requestedTagInfo;

    public SearchTagsResultInfo(TagProductInfoList tags, List<String> requestedRfIds, TagProductInfo requestedTagInfo) {
        this.foundTags = tags;
        this.requestedRfIds = requestedRfIds;
        this.requestedTagInfo = requestedTagInfo;
    }
}
