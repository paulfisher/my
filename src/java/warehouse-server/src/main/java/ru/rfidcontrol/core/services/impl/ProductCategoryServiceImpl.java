package ru.rfidcontrol.core.services.impl;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.product.PriceInfo;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;
import ru.rfidcontrol.core.data.product.SizerInfo;
import ru.rfidcontrol.core.entity.*;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.ProductCategoryService;
import ru.rfidcontrol.core.services.ServiceBase;
import ru.rfidcontrol.core.services.SizerService;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductCategoryServiceImpl extends ServiceBase implements ProductCategoryService {

    @Inject CompanyService companyService;
    @Inject SizerService sizerService;

    @NotNull
    @Override
    public Map<Integer, ProductCategory> findProductCategories(@NotNull ProductCategoryInfo searchProductCategoryInfo, @NotNull Pagination pagination) {
        Map<Integer, ProductCategory> mapResult = new HashMap<>();

        if (searchProductCategoryInfo.id > 0) {
            //������ �� id, ������ ���� ��������� �� id
            ProductCategory byId = findProductCategory(searchProductCategoryInfo.id);
            if (byId != null) {
                mapResult.put(byId.getId(), byId);
                return mapResult;
            }
        }

        List<ProductCategory> categoriesResult;
        Company company = companyService.getCompany(searchProductCategoryInfo.companyId);
        ProductCategoryInfo searchParent = searchProductCategoryInfo.parent;

        if (searchParent != null) {
            if (searchParent.id == 0) {
                //���� ���, � ���� ���� ��������
                categoriesResult = findCategories(company, searchProductCategoryInfo.name, true, pagination);
            } else {
                //���� �� ����������� ��������
                categoriesResult = findCategories(company, searchProductCategoryInfo.name, getProductCategory(searchParent.id), pagination);
            }
        } else {
            //�������� �� ������
            if (searchProductCategoryInfo.isTopLevel) {
                //���������� ������ ������� ���������
                categoriesResult = findCategories(company, searchProductCategoryInfo.name, null, pagination);
            } else {
                //���������� ���
                categoriesResult = findCategories(company, searchProductCategoryInfo.name, false, pagination);
            }
        }

        for (ProductCategory productCategory : categoriesResult) {
            mapResult.put(productCategory.getId(), productCategory);
        }
        return mapResult;
    }

    private List<ProductCategory> findCategories(@NotNull Company company, @Nullable String name, @Nullable ProductCategory parent, @NotNull Pagination pagination) {
        if (parent != null) {
            if (Strings.isNullOrEmpty(name)) {
                //���� �� parent � ��� ��������
                pagination.total = count(em()
                        .createQuery("select count(id) from ProductCategory where company = :company and parentProductCategory = :parent", Number.class)
                        .setParameter("parent", parent)
                        .setParameter("company", company));

                return em().createQuery("from ProductCategory where company = :company and parentProductCategory = :parent order by name asc", ProductCategory.class)
                        .setParameter("parent", parent)
                        .setParameter("company", company)
                        .setFirstResult(pagination.offset)
                        .setMaxResults(pagination.count)
                        .getResultList();
            } else {
                //���� �� parent � ��������
                pagination.total = count(em()
                        .createQuery("select count(id) from ProductCategory where lower(name) like :name and company = :company and parentProductCategory = :parent", Number.class)
                        .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                        .setParameter("parent", parent)
                        .setParameter("company", company));

                return em().createQuery("from ProductCategory where lower(name) like :name and company = :company and parentProductCategory = :parent order by name asc", ProductCategory.class)
                        .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                        .setParameter("parent", parent)
                        .setParameter("company", company)
                        .setFirstResult(pagination.offset)
                        .setMaxResults(pagination.count)
                        .getResultList();
            }

        } else {
            if (Strings.isNullOrEmpty(name)) {
                //���� parent == null � ��� ��������
                pagination.total = count(em()
                        .createQuery("select count(id) from ProductCategory where company = :company and parentProductCategory is null", Number.class)
                        .setParameter("company", company));

                return em().createQuery("from ProductCategory where company = :company and parentProductCategory is null order by name asc", ProductCategory.class)
                        .setParameter("company", company)
                        .setFirstResult(pagination.offset)
                        .setMaxResults(pagination.count)
                        .getResultList();
            } else {
                //���� parent == null � �� ��������
                pagination.total = count(em()
                        .createQuery("select count(id) from ProductCategory where lower(name) like :name and company = :company and parentProductCategory is null", Number.class)
                        .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                        .setParameter("parent", parent)
                        .setParameter("company", company));

                return em().createQuery("from ProductCategory where lower(name) like :name and company = :company and parentProductCategory is null order by name asc", ProductCategory.class)
                        .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                        .setParameter("parent", parent)
                        .setParameter("company", company)
                        .setFirstResult(pagination.offset)
                        .setMaxResults(pagination.count)
                        .getResultList();
            }
        }
    }

    private List<ProductCategory> findCategories(@NotNull Company company, @Nullable String name, boolean onlyWithParent, @NotNull Pagination pagination) {
        if (onlyWithParent) {
            if (Strings.isNullOrEmpty(name)) {
                //���� �� parent � ��� ��������
                pagination.total = count(em()
                        .createQuery("select count(id) from ProductCategory where company = :company and parentProductCategory is not null", Number.class)
                        .setParameter("company", company));

                return em().createQuery("from ProductCategory where company = :company and parentProductCategory is not null order by name asc", ProductCategory.class)
                        .setParameter("company", company)
                        .setFirstResult(pagination.offset)
                        .setMaxResults(pagination.count)
                        .getResultList();
            } else {
                //���� �� parent � ��������
                pagination.total = count(em()
                        .createQuery("select count(id) from ProductCategory where lower(name) like :name and company = :company and parentProductCategory is not null", Number.class)
                        .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                        .setParameter("company", company));

                return em().createQuery("from ProductCategory where lower(name) like :name and company = :company and parentProductCategory is not null order by name asc", ProductCategory.class)
                        .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                        .setParameter("company", company)
                        .setFirstResult(pagination.offset)
                        .setMaxResults(pagination.count)
                        .getResultList();
            }
        } else {
            if (Strings.isNullOrEmpty(name)) {
                //���� parent == null � ��� ��������
                pagination.total = count(em()
                        .createQuery("select count(id) from ProductCategory where company = :company", Number.class)
                        .setParameter("company", company));

                return em().createQuery("from ProductCategory where company = :company order by name asc", ProductCategory.class)
                        .setParameter("company", company)
                        .setFirstResult(pagination.offset)
                        .setMaxResults(pagination.count)
                        .getResultList();
            } else {
                //���� parent == null � �� ��������
                pagination.total = count(em()
                        .createQuery("select count(id) from ProductCategory where lower(name) like :name and company = :company", Number.class)
                        .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                        .setParameter("company", company));

                return em().createQuery("from ProductCategory where lower(name) like :name and company = :company order by name asc", ProductCategory.class)
                        .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                        .setParameter("company", company)
                        .setFirstResult(pagination.offset)
                        .setMaxResults(pagination.count)
                        .getResultList();
            }
        }
    }

    @Nullable
    @Override
    public ProductCategory findProductCategory(int id) {
        return find(ProductCategory.class, id);
    }

    @Override
    public ProductCategory getProductCategory(int id) {
        ProductCategory category = findProductCategory(id);
        if (category == null) {
            throw WarehouseException.notFound(ProductCategory.class, id);
        }
        return category;
    }

    @Override
    @Transactional
    public ProductCategory createProductCategory(@NotNull ProductCategoryInfo productCategoryInfo) {
        if (productCategoryInfo.companyId == 0) {
            throw WarehouseException.create("company id required");
        }
        Company company = companyService.getCompany(productCategoryInfo.companyId);
        Sizer sizer = null;
        if (productCategoryInfo.sizer != null && productCategoryInfo.sizer.okeiCode > 0) {
            sizer = sizerService.findSizerByOkeiCode(productCategoryInfo.sizer.okeiCode);
        }

        ProductCategory category = ProductCategory
                .fromInfo(productCategoryInfo)
                .setSizer(sizer)
                .setCompany(company);

        if (productCategoryInfo.parent != null && productCategoryInfo.parent.id > 0) {
            category.setParentProductCategory(getProductCategory(productCategoryInfo.parent.id));
        } else {
            category.setParentProductCategory(null);
        }
        checkCycleParent(category);
        save(category);

        return category;
    }

    @Override
    @Transactional
    public ProductCategory editProductCategory(@NotNull ProductCategory productCategory, @NotNull ProductCategoryInfo productCategoryInfo) {
        if (productCategoryInfo.companyId == 0) {
            throw WarehouseException.missingRequiredParameter("companyId");
        }
        Company company = companyService.getCompany(productCategoryInfo.companyId);
        Sizer sizer = null;
        if (productCategoryInfo.sizer != null && productCategoryInfo.sizer.okeiCode > 0) {
            sizer = sizerService.findSizerByOkeiCode(productCategoryInfo.sizer.okeiCode);
        }

        productCategory
                .setReference(productCategoryInfo.reference)
                .setDeleted(productCategoryInfo.isDeleted)
                .setCompany(company)
                .setSizer(sizer)
                .setName(productCategoryInfo.name);

        if (productCategoryInfo.parent != null && productCategoryInfo.parent.id > 0) {
            productCategory.setParentProductCategory(getProductCategory(productCategoryInfo.parent.id));
        } else {
            productCategory.setParentProductCategory(null);
        }
        checkCycleParent(productCategory);

        return productCategory;
    }

    @Override
    @Transactional
    public ProductCategory deleteProductCategory(@NotNull ProductCategory productCategory) {
        return productCategory.setDeleted(true);
    }

    @Nullable
    @Override
    public ProductCategory findByReference(@NotNull Company company, @NotNull String reference) {
        return first(em().createQuery("from ProductCategory where company = :company and reference = :reference", ProductCategory.class)
                .setParameter("company", company)
                .setParameter("reference", reference));
    }

    @Override
    public ProductCategory getByReference(@NotNull Company company, @NotNull String reference) {
        ProductCategory productCategory = findByReference(company, reference);
        if (productCategory == null) {
            throw WarehouseException.productCategoryNotFound(reference);
        }
        return productCategory;
    }

    private List<ProductCategory> findCategories(@NotNull Company company, @Nullable String name) {
        if (name != null) {
            return em().createQuery("from ProductCategory where lower(name) like :name and company = :company order by name", ProductCategory.class)
                    .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                    .setParameter("company", company)
                    .getResultList();
        } else {
            return em().createQuery("from ProductCategory where company = :company order by name", ProductCategory.class)
                    .setParameter("company", company)
                    .getResultList();
        }
    }

    private void checkCycleParent(ProductCategory category) {
        ProductCategory currentCategory = category.getParentProductCategory();
        while (currentCategory != null) {
            if (currentCategory.getId() == category.getId()) {
                throw WarehouseException.productCategoryCycle(category);
            }
            currentCategory = currentCategory.getParentProductCategory();
        }
    }
}
