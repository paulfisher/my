package ru.rfidcontrol.app.data.product;

import ru.rfidcontrol.app.data.BaseRequest;

import javax.annotation.Nullable;

public class ImportProductQuantityRequest extends BaseRequest {
    @Nullable public String companyRef;
    @Nullable public String stockRef;
    @Nullable public String productCategoryRef;
    public double quantity;
}