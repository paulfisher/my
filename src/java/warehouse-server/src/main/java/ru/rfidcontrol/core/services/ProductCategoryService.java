package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.ProductCategory;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface ProductCategoryService {

    @NotNull Map<Integer, ProductCategory> findProductCategories(@NotNull ProductCategoryInfo productCategoryInfo, @NotNull Pagination pagination);

    @Nullable ProductCategory findProductCategory(int id);

    @NotNull ProductCategory getProductCategory(int id);


    @NotNull ProductCategory createProductCategory(@NotNull ProductCategoryInfo productCategoryInfo);

    @NotNull ProductCategory editProductCategory(@NotNull ProductCategory productCategory, @NotNull ProductCategoryInfo productCategoryInfo);

    @NotNull ProductCategory deleteProductCategory(@NotNull ProductCategory productCategory);


    @Nullable ProductCategory findByReference(@NotNull Company company, @NotNull String reference);

    @NotNull ProductCategory getByReference(@NotNull Company company, @NotNull String reference);
}