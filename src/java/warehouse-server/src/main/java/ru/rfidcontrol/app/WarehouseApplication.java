package ru.rfidcontrol.app;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ResourceConfig;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;
import ru.rfidcontrol.app.exceptions.WarehouseExceptionMapper;
import ru.rfidcontrol.app.filters.CorsResponseFilter;
import ru.rfidcontrol.app.filters.LogRequestFilter;
import ru.rfidcontrol.app.guice.AppGuiceServletContextListener;
import ru.rfidcontrol.app.task.C1ProductTreeTimerTask;

import javax.inject.Inject;
import java.util.Timer;
import java.util.TimerTask;

public class WarehouseApplication extends ResourceConfig {

    @Inject
    public WarehouseApplication(ServiceLocator serviceLocator) {
        register(CorsResponseFilter.class);
        register(LogRequestFilter.class);

        //register(AccessControlDynamicBinding.class);

        GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);

        serviceLocator.getService(GuiceIntoHK2Bridge.class).bridgeGuiceInjector(AppGuiceServletContextListener.injector);

        register(WarehouseExceptionMapper.class);

        //runTimerTasks();
    }

    private void runTimerTasks() {
        TimerTask timerTask = new C1ProductTreeTimerTask();
        //running timer task as daemon thread
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(timerTask, 0, 60*1000);
        //timer.cancel();
    }

}