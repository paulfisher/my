package ru.rfidcontrol.app.exceptions;

import ru.rfidcontrol.core.exceptions.WarehouseException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class WarehouseExceptionMapper implements ExceptionMapper<WarehouseException> {

    @Override
    public Response toResponse(WarehouseException ex) {
        return Response
                .status(ex.code.status)
                .entity(new ErrorResponse(ex))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}