package ru.rfidcontrol.app.guice;

import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import com.google.inject.persist.jpa.JpaPersistModule;
import ru.rfidcontrol.app.config.ConfigurationException;
import ru.rfidcontrol.app.config.DbConfig;
import ru.rfidcontrol.app.config.ServerConfig;
import ru.rfidcontrol.app.guice.events.EventBusTypeListener;
import ru.rfidcontrol.app.guice.logger.LoggerTypeListener;
import ru.rfidcontrol.app.services.ApiLogService;
import ru.rfidcontrol.app.services.impl.ApiLogServiceImpl;
import ru.rfidcontrol.core.services.*;
import ru.rfidcontrol.core.services.impl.*;

import javax.annotation.Nonnull;
import java.util.Properties;

public class ApplicationModule extends AbstractModule {

    private final ServerConfig config;
    private final DbConfig dbConfig;

    public ApplicationModule(@Nonnull ServerConfig config) {
        this.config = config;
        if (null == config.getDbConfig()) {
            throw new ConfigurationException("db config is null");
        }
        dbConfig = config.getDbConfig();
    }

    @Override
    protected void configure() {
        bindListener(Matchers.any(), new LoggerTypeListener());

        bind(ServerConfig.class).toInstance(config);
        bind(DbConfig.class).toInstance(dbConfig);

        configureJpa();
        configureEvents();

        bind(CompanyService.class).to(CompanyServiceImpl.class).asEagerSingleton();
        bind(UserService.class).to(UserServiceImpl.class).asEagerSingleton();
        bind(DeviceService.class).to(DeviceServiceImpl.class).asEagerSingleton();
        bind(TagService.class).to(TagServiceImpl.class).asEagerSingleton();
        bind(StorageService.class).to(StorageServiceImpl.class).asEagerSingleton();

        bind(ProductService.class).to(ProductServiceImpl.class).asEagerSingleton();
        bind(ProductCategoryService.class).to(ProductCategoryServiceImpl.class).asEagerSingleton();
        bind(TransferService.class).to(TransferServiceImpl.class).asEagerSingleton();
        bind(InventoryService.class).to(InventoryServiceImpl.class).asEagerSingleton();

        bind(ExternalAppService.class).to(ExternalAppServiceImpl.class).asEagerSingleton();

        bind(ApiLogService.class).to(ApiLogServiceImpl.class).asEagerSingleton();

        bind(SupplierService.class).to(SupplierServiceImpl.class).asEagerSingleton();
        bind(SizerService.class).to(SizerServiceImpl.class).asEagerSingleton();


        bind(PriceService.class).to(PriceServiceImpl.class).asEagerSingleton();
        bind(QuantityService.class).to(QuantityServiceImpl.class).asEagerSingleton();
    }

    private void configureEvents() {
        final EventBus eventBus = new EventBus();

        bind(EventBus.class).toInstance(eventBus);
        bindListener(Matchers.any(), new EventBusTypeListener(eventBus));
    }

    private void configureJpa() {
        final Properties props = new Properties();
        props.putAll(dbConfig.getHibernateProperties());

        install(new JpaPersistModule(dbConfig.getJpaUnitName()).properties(props));
    }
}
