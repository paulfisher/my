package ru.rfidcontrol.core.data.company;

import ru.rfidcontrol.core.entity.Supplier;

import java.util.HashMap;
import java.util.Map;

public class SupplierInfoList {

    public final Map<Integer, SupplierInfo> suppliers;
    public final int size;

    public SupplierInfoList(Map<Integer, Supplier> suppliers) {
        this.suppliers = new HashMap<>();
        for (Map.Entry<Integer, Supplier> supplierEntry : suppliers.entrySet()) {
            this.suppliers.put(supplierEntry.getKey(), new SupplierInfo(supplierEntry.getValue()));
        }
        size = this.suppliers.size();
    }
}
