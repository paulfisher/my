package ru.rfidcontrol.app.data.product;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.data.product.PriceInfo;

import javax.annotation.Nullable;

public class ImportProductPriceRequest extends BaseRequest {

    @Nullable public PriceInfo priceInfo;
    @Nullable public String productCategoryRef;
    @Nullable public String companyRef;
}