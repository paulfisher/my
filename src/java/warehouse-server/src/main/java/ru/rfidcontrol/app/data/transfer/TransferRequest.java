package ru.rfidcontrol.app.data.transfer;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.transfer.TransferInfo;

import javax.validation.constraints.NotNull;

public class TransferRequest extends BaseRequest {

    @NotNull public TransferInfo transferInfo;

    public TransferRequest() {
        transferInfo = new TransferInfo();
    }

    @NotNull
    public TransferInfo getTransferInfo() {
        return transferInfo;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        if (clientContext.getStorageId() > 0) {
            transferInfo.storageId = clientContext.getStorageId();
        }
        transferInfo.userId = clientContext.getUserId();
        transferInfo.companyId = clientContext.getCompanyId();
        transferInfo.deviceId = clientContext.getDeviceId();
    }
}
