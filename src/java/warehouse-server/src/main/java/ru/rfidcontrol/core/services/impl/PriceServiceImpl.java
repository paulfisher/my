package ru.rfidcontrol.core.services.impl;

import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.product.PriceInfo;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.services.PriceService;
import ru.rfidcontrol.core.services.ServiceBase;

import javax.validation.constraints.NotNull;

public class PriceServiceImpl extends ServiceBase implements PriceService {

    @NotNull
    @Override
    @Transactional
    public ProductCategory updatePrice(@NotNull ProductCategory productCategory, @NotNull PriceInfo priceInfo) {
        productCategory.setPrice(priceInfo.price)
                .setVat(priceInfo.vat);

        return productCategory;
    }

}
