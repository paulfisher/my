package ru.rfidcontrol.core.data.tag;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.Tag;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagProductInfoList extends BaseInfoList {

    @Nullable public Map<String, TagProductInfo> tags;

    public int size;

    public TagProductInfoList() {
    }

    public TagProductInfoList(Map<String, Tag> tags) {
        this.tags = new HashMap<>();
        for (Map.Entry<String, Tag> tag : tags.entrySet()) {
            this.tags.put(tag.getKey(), new TagProductInfo(tag.getValue()));
        }
        this.size = this.tags.size();
    }

    public TagProductInfoList(List<Tag> tags) {
        this.tags = new HashMap<>();
        for (Tag tag : tags) {
            this.tags.put(tag.getRfId(), tag.toInfo());
        }
        this.size = this.tags.size();
    }

    @NotNull
    public Map<String, TagProductInfo> getTags() {
        if (tags == null) {
            tags = new HashMap<>();
        }
        return tags;
    }

    @NotNull
    public static TagProductInfoList create(Map<Integer, Tag> tags) {
        TagProductInfoList list = new TagProductInfoList();
        list.tags = new HashMap<>();

        for (Map.Entry<Integer, Tag> integerTagEntry : tags.entrySet()) {
            list.tags.put(integerTagEntry.getValue().getRfId(), integerTagEntry.getValue().toInfo());
        }
        list.size = tags.size();

        return list;
    }
}
