package ru.rfidcontrol.app.data.inventory;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.data.inventory.InventoryInfo;
import ru.rfidcontrol.core.data.inventory.InventoryItemInfo;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class InventoryItemRequest extends BaseRequest {

    @Nullable public InventoryInfo inventoryInfo;
    @Nullable public InventoryItemInfo inventoryItemInfo;

    @NotNull
    public InventoryInfo getInventoryInfo() {
        if (inventoryInfo == null) {
            inventoryInfo = new InventoryInfo();
        }
        return inventoryInfo;
    }

    @NotNull
    public InventoryItemInfo getInventoryItemInfo() {
        if (inventoryItemInfo == null) {
            inventoryItemInfo = new InventoryItemInfo();
        }
        return inventoryItemInfo;
    }
}