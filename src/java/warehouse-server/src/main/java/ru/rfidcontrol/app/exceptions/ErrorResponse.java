package ru.rfidcontrol.app.exceptions;

import ru.rfidcontrol.core.exceptions.WarehouseException;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ErrorResponse {

    @XmlElement(name = "status")
    int status;

    @XmlElement(name = "code")
    int code;

    @XmlElement(name = "message")
    String message;

    @XmlElement(name = "link")
    String link;

    @XmlElement(name = "developerMessage")
    String developerMessage;

    public ErrorResponse(WarehouseException ex) {
        this.status = ex.code.status.getStatusCode();
        this.code = ex.code.value;
        this.message = ex.getMessage();
        this.developerMessage = "ask me about this error pavel.rybako@gmail.com";
        this.link = String.format("http://api.rfidcontrol.ru/help/errorCode/%s", this.code);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}