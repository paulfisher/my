package ru.rfidcontrol.core.data.product;

import ru.rfidcontrol.core.data.BaseInfoList;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class ProductCategoryQuantityInfoList extends BaseInfoList {

    @Nullable public Map<Integer, ProductCategoryQuantityInfo> quantities;
    public int size;

    public int countTotal;
    public int countMarked;
    public int countUnmarked;

    public ProductCategoryQuantityInfoList() {
    }

    public ProductCategoryQuantityInfoList(Map<Integer, ProductCategoryQuantityInfo> quantities) {
        this.countTotal = 0;
        this.countMarked = 0;
        this.countUnmarked = 0;

        this.quantities = new HashMap<>();
        for (Map.Entry<Integer, ProductCategoryQuantityInfo> category : quantities.entrySet()) {
            this.quantities.put(category.getKey(), category.getValue());
            this.countTotal += category.getValue().productQuantityInfo.quantity;
            this.countMarked += category.getValue().tagsCount;
            this.countUnmarked += category.getValue().productQuantityInfo.quantity - category.getValue().tagsCount;
        }
        this.size = this.quantities.size();
    }

}
