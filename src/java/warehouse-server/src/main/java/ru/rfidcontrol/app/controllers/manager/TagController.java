package ru.rfidcontrol.app.controllers.manager;

import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.tag.SearchTagsRequest;
import ru.rfidcontrol.app.data.tag.TagRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.tag.TagProductInfoList;
import ru.rfidcontrol.core.entity.Tag;
import ru.rfidcontrol.core.services.TagService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/online/tag")
@AccessControl
@AllowFor({AccessTypes.MANAGER})
public class TagController extends BaseOnlineController {

    @Inject @NotNull protected TagService tagService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findTags(@NotNull SearchTagsRequest searchTagsRequest) {
        populateFromContext(searchTagsRequest);

        Map<Integer, Tag> tags = tagService.findTags(searchTagsRequest.getTagInfo(), searchTagsRequest.pagination);

        //����������� �� ���������. �� ������ ��������
        for (Map.Entry<Integer, Tag> integerTagEntry : tags.entrySet()) {
            Tag tag = integerTagEntry.getValue();
            if (tag.getCompany().getId() != getClientContext().getCompanyId()) {
                throw forbidden();
            }
        }

        return DataResponse.create(TagProductInfoList.create(tags));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getTag(@NotNull TagRequest tagRequest) {
        populateFromContext(tagRequest);

        required(tagRequest.getTagInfo().id, "tagInfo.id");

        Tag tag = tagService.getTag(tagRequest.getTagInfo().id);
        if (tag.getCompany() == null || tag.getCompany().getId() != getClientContext().getCompanyId()) {
            throw forbidden();
        }

        return DataResponse.create(tag.toInfo());
    }
}