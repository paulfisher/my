package ru.rfidcontrol.core.services.impl;

import ru.rfidcontrol.core.entity.type.ExternalApp;
import ru.rfidcontrol.core.services.ExternalAppService;

import javax.validation.constraints.NotNull;
import java.util.Objects;

import static ru.rfidcontrol.core.exceptions.WarehouseException.notFound;

public class ExternalAppServiceImpl implements ExternalAppService {

    @NotNull
    @Override
    public ExternalApp getByToken(@NotNull String token) {
        if (Objects.equals(token, ExternalApp.C1.token)) {
            return ExternalApp.C1;
        }
        throw notFound(ExternalApp.class, token);
    }

}
