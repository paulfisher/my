package ru.rfidcontrol.core.data.company;

import ru.rfidcontrol.core.data.BaseInfo;
import ru.rfidcontrol.core.data.device.DeviceInfoList;
import ru.rfidcontrol.core.data.storage.StorageInfoList;
import ru.rfidcontrol.core.data.user.UserInfoList;
import ru.rfidcontrol.core.entity.Company;

import java.util.Date;

public class CompanyInfo extends BaseInfo {

    public int id;

    public String name;

    public Date createTime;

    public String email;

    public boolean isDeleted = false;
    public String reference;

    public UserInfoList users;
    public StorageInfoList storages;
    public DeviceInfoList devices;

    public CompanyInfo() {
    }

    public CompanyInfo(Company company) {
        this.id = company.getId();
        this.name = company.getName();
        this.createTime = company.getCreateTime();
        this.email = company.getEmail();
        this.isDeleted = company.isDeleted();
        this.reference = company.getReference();
        this.users = new UserInfoList(company.getUsers());
        this.storages = new StorageInfoList(company.getStorages());
        this.devices = new DeviceInfoList(company.getDevices());
    }
}
