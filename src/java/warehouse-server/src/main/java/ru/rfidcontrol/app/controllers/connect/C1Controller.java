package ru.rfidcontrol.app.controllers.connect;

import com.google.common.base.Strings;
import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.product.ImportProductCategoryRequest;
import ru.rfidcontrol.app.data.product.ImportProductPriceRequest;
import ru.rfidcontrol.app.data.product.ImportProductQuantityRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.services.*;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/connect/c1")
@AccessControl
@AllowFor({AccessTypes.C1})
public class C1Controller extends BaseController {

    @Inject ProductCategoryService categoryService;
    @Inject CompanyService companyService;
    @Inject StorageService storageService;
    @Inject PriceService priceService;
    @Inject QuantityService quantityService;

    @POST
    @Path("/category/save")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse saveCategory(@NotNull ImportProductCategoryRequest productCategoryRequest) {

        required(productCategoryRequest.companyRef, "productCategoryRequest.companyRef");
        required(productCategoryRequest.productCategoryInfo, "productCategoryRequest.productCategoryInfo");

        ProductCategoryInfo categoryInfo = productCategoryRequest.productCategoryInfo;
        required(categoryInfo, "productCategoryInfo");
        required(categoryInfo.reference, "productCategoryInfo.reference");

        Company company = companyService.getCompany(productCategoryRequest.companyRef);
        categoryInfo.companyId = company.getId();

        if (categoryInfo.parent != null && !Strings.isNullOrEmpty(categoryInfo.parent.reference)) {
            ProductCategory parent = categoryService.findByReference(company, categoryInfo.parent.reference);
            if (parent == null) {
                /**
                 * ��� ��������� ������ reference ���������-��������, �� ����� ���������-�������� ��� ��� � ����
                 * ������� ������� ���������-��������, ��� ���������� � ��� ������,
                 * ����� ������ ������ ���������� �� ��������� reference-� ��������
                 */
                categoryInfo.parent.companyId = company.getId();
                parent = categoryService.createProductCategory(categoryInfo.parent);
            }
            categoryInfo.parent = parent.toInfo();
        }

        ProductCategory productCategory = categoryService.findByReference(company, categoryInfo.reference);
        if (productCategory == null) {
            productCategory = categoryService.createProductCategory(categoryInfo);
        } else {
            categoryService.editProductCategory(productCategory, categoryInfo);
        }

        return DataResponse.create(productCategory.toInfo());
    }

    @POST
    @Path("/price/save")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse savePrice(@NotNull ImportProductPriceRequest importProductPriceRequest) {

        required(importProductPriceRequest.companyRef, "importProductPriceRequest.companyRef");
        required(importProductPriceRequest.productCategoryRef, "importProductPriceRequest.productCategoryRef");
        required(importProductPriceRequest.priceInfo, "importProductPriceRequest.priceInfo");

        Company company = companyService.getCompany(importProductPriceRequest.companyRef);

        ProductCategory productCategory = categoryService.getByReference(company, importProductPriceRequest.productCategoryRef);
        priceService.updatePrice(productCategory, importProductPriceRequest.priceInfo);

        return DataResponse.create(productCategory.toInfo());
    }

    @POST
    @Path("/quantity/save")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse saveQuantity(@NotNull ImportProductQuantityRequest quantityRequest) {
        required(quantityRequest.companyRef, "quantityRequest.companyRef");
        required(quantityRequest.stockRef, "quantityRequest.stockRef");
        required(quantityRequest.productCategoryRef, "quantityRequest.productCategoryRef");

        ProductCategory productCategory = categoryService.getByReference(
                companyService.getCompany(quantityRequest.companyRef),
                quantityRequest.productCategoryRef);

        quantityService.updateQuantity(
                storageService.getStorage(quantityRequest.stockRef),
                productCategory,
                quantityRequest.quantity);

        return DataResponse.create(productCategory.toInfo());
    }
}