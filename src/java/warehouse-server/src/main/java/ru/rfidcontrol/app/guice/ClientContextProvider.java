package ru.rfidcontrol.app.guice;


import com.google.common.base.Strings;
import com.google.inject.Provider;
import com.google.inject.servlet.SessionScoped;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.entity.type.ExternalApp;
import ru.rfidcontrol.core.services.DeviceService;
import ru.rfidcontrol.core.services.ExternalAppService;
import ru.rfidcontrol.core.services.UserService;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

@SessionScoped
public class ClientContextProvider implements Provider<ClientContext> {

    private static final String EXTERNAL_APP_TOKEN_HEADER = "rfidcontrol-external-app";

    @Inject HttpSession session;
    @Inject HttpServletRequest httpServletRequest;

    @Inject UserService userService;
    @Inject DeviceService deviceService;
    @Inject ExternalAppService externalAppService;

    @Override
    public ClientContext get() {
        String token = httpServletRequest.getHeader(EXTERNAL_APP_TOKEN_HEADER);
        if (!Strings.isNullOrEmpty(token)) {
            return externalAppContext(externalAppService.getByToken(token));
        }
        return sessionClientContext();

    }

    @NotNull
    private ClientContext sessionClientContext() {
        return new ClientContext()
                .setAdmin((boolean) sessionValue("admin", false))
                .setUserId((int) sessionValue("userId", 0))
                .setDeviceId((int) sessionValue("deviceId", 0))
                .setCompanyId((int) sessionValue("companyId", 0))
                .setStorageId((int) sessionValue("storageId", 0));
    }

    @NotNull
    private ClientContext externalAppContext(@NotNull ExternalApp externalApp) {
        return new ClientContext()
                .setUserId(0)
                .setCompanyId(0)
                .setDeviceId(0)
                .setStorageId(0)
                .setAdmin(false)
                .setExternalApp(externalApp);
    }

    @NotNull
    public User getUser() {
        ClientContext context = get();
        if (context.getUserId() == 0) {
            return null;
        }
        return userService.getUser(context.getUserId());
    }

    protected Object sessionValue(@NotNull String key, @NotNull Object value) {
        if (session.getAttribute(key) == null) {
            return value;
        }
        return session.getAttribute(key);
    }
}