package ru.rfidcontrol.core.exceptions;

import ru.rfidcontrol.core.entity.Inventory;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.entity.type.InventoryStatus;

import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

public class WarehouseException extends RuntimeException {

    public final String warehouseExceptionMessage;
    public final WarehouseErrorCode code;

    public WarehouseException(String message, WarehouseErrorCode code) {
        super(message);
        this.code = code;
        this.warehouseExceptionMessage = message;
    }

    public static WarehouseException companyNotFound(int id) {
        return new WarehouseException(String.format("Company %d not found", id), WarehouseErrorCode.COMPANY_NOT_FOUND);
    }

    public static WarehouseException productCategoryNotFound(@NotNull String reference) {
        return new WarehouseException(String.format("ProductCategory %s not found", reference), WarehouseErrorCode.PRODUCT_CATEGORY_NOT_FOUND);
    }

    public static WarehouseException userNotFound(int id) {
        return new WarehouseException(String.format("User %d not found", id), WarehouseErrorCode.USER_NOT_FOUND);
    }

    public static WarehouseException deviceNotFound(int deviceId) {
        return new WarehouseException(String.format("Device %d not found", deviceId), WarehouseErrorCode.DEVICE_NOT_FOUND);
    }

    public static WarehouseException deviceNotFound(String imeiCode) {
        return new WarehouseException(String.format("Device %s not found", imeiCode), WarehouseErrorCode.DEVICE_NOT_FOUND);
    }

    public static WarehouseException inventoryNotFound(int inventoryId) {
        return new WarehouseException(String.format("Inventory %d not found", inventoryId), WarehouseErrorCode.INVENTORY_NOT_FOUND);
    }

    public static WarehouseException notFound(Class type, int id) {
        String name = type.getSimpleName();
        return new WarehouseException(String.format("%s #%d was not found", name, id), WarehouseErrorCode.NOT_FOUND);
    }

    public static WarehouseException notFound(Class type, String code) {
        String name = type.getSimpleName();
        return new WarehouseException(String.format("%s \"%s\" was not found", name, code), WarehouseErrorCode.NOT_FOUND);
    }

    public static WarehouseException productCategoryCycle(ProductCategory category) {
        return new WarehouseException(String.format("%s has cycle in parents", category.getName()), WarehouseErrorCode.PRODUCT_CATEGORY_CYCLE);
    }

    public static <T> WarehouseException notFound(TypedQuery<T> query) {
        return new WarehouseException(String.format("query returns no result. query: %s", query.toString()), WarehouseErrorCode.NOT_FOUND);
    }

    public static WarehouseException transferNotFound(int transferId) {
        return new WarehouseException(String.format("Transfer %d not found", transferId), WarehouseErrorCode.TRANSFER_NOT_FOUND);
    }

    public static WarehouseException companyNotFound(@NotNull String reference) {
        return new WarehouseException(String.format("Company %s not found", reference), WarehouseErrorCode.COMPANY_NOT_FOUND);
    }

    public static WarehouseException missingRequiredParameter(String parameter) {
        return new WarehouseException(String.format("Required parameter is missing: %s", parameter), WarehouseErrorCode.MISSING_PARAMETER);
    }

    public static WarehouseException missingClientContext() {
        return new WarehouseException("Missing client context", WarehouseErrorCode.MISSING_CONTEXT);
    }

    public static WarehouseException create(String message) {
        return new WarehouseException(message, WarehouseErrorCode.DEFAULT);
    }

    public static WarehouseException userLoginFail(String name, String password) {
        return new WarehouseException(String.format("Pair name: %s, password: %s not found", name, password), WarehouseErrorCode.USER_LOGIN_FAIL);
    }

    public static WarehouseException userLoginFail(String name) {
        return new WarehouseException(String.format("User login %s fail", name), WarehouseErrorCode.USER_LOGIN_FAIL);
    }

    public static WarehouseException userIsDeleted(User user) {
        return new WarehouseException(String.format("User %d is deleted", user.getId()), WarehouseErrorCode.USER_IS_DELETED);
    }

    public static WarehouseException inventoryStatusUnexpected(Inventory inventory, InventoryStatus[] expected) {
        return new WarehouseException(String.format("Inventory %s has not expected status. Expected: %s", inventory, Arrays.toString(expected)), WarehouseErrorCode.INVENTORY_STATUS_UNEXPECTED);
    }
}
