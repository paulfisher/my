package ru.rfidcontrol.app.controllers.client;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.user.UserInfoList;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.UserService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/user")
@AccessControl
@AllowFor({AccessTypes.DEVICE})
public class UserController extends BaseController {

    @Inject @NotNull protected UserService userService;
    @Inject @NotNull protected CompanyService companyService;

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getUsers() {
        return DataResponse.create(
            new UserInfoList(
                companyService.getCompany(getClientContext().getCompanyId())
                    .getUsers()
            )
        );
    }
}