package ru.rfidcontrol.app.data.storage;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.storage.StorageInfo;

import javax.validation.constraints.NotNull;

public class StorageRequest extends BaseRequest {

    @NotNull public final StorageInfo info;

    public StorageRequest() {
        info = new StorageInfo();
    }

    @NotNull
    public StorageInfo storageInfo() {
        return info;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        if (clientContext.getCompanyId() > 0) {
            info.companyId = clientContext.getCompanyId();
        }
    }
}