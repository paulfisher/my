package ru.rfidcontrol.core.data.product;

import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.entity.ProductQuantity;
import ru.rfidcontrol.core.entity.Storage;

import javax.validation.constraints.NotNull;

public class ProductQuantityInfo {

    public int id;

    public int categoryId;
    public String categoryName;

    public int storageId;
    public String storageName;

    public double quantity;

    public ProductQuantityInfo() {}

    public ProductQuantityInfo(@NotNull ProductQuantity productQuantity) {
        this.id = productQuantity.getId();

        this.categoryId = productQuantity.getProductCategory().getId();
        this.categoryName = productQuantity.getProductCategory().getName();

        Storage storage = productQuantity.getStorage();
        if (storage != null) {
            this.storageId = storage.getId();
            this.storageName = storage.getName();
        }

        this.quantity = productQuantity.getQuantity();
    }

    public ProductQuantityInfo(ProductCategory productCategory) {
        this.categoryId = productCategory.getId();
        this.categoryName = productCategory.getName();
    }
}
