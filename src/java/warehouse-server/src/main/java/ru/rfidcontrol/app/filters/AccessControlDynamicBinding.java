package ru.rfidcontrol.app.filters;

import com.google.inject.Injector;
import ru.rfidcontrol.app.filters.annotations.AccessControl;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.USER)
public class AccessControlDynamicBinding implements DynamicFeature {

    @Inject Injector injector;

    @Override
    public void configure(final ResourceInfo resourceInfo, final FeatureContext context) {
        if (resourceInfo.getResourceClass().isAnnotationPresent(AccessControl.class)) {
            ContainerRequestFilter filter = new AccessControlFilter(resourceInfo);
            injector.injectMembers(filter);
            context.register(filter);
        }
    }

}