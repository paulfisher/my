package ru.rfidcontrol.app.filters;

import com.google.inject.Injector;
import org.jboss.logging.Logger;
import ru.rfidcontrol.app.services.ApiLogService;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.*;

@Provider
@Priority(Priorities.USER)
public class LogRequestFilter implements ContainerRequestFilter, ContainerResponseFilter, WriterInterceptor {

    @Inject Injector injector;

    private static Logger logger = Logger.getLogger(LogRequestFilter.class);
    private static final String ENTITY_LOGGER_PROPERTY = LogRequestFilter.class.getName() + ".entityLogger";

    private int maxEntitySize = 10 * 1024;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        StringBuilder b = new StringBuilder();
        b.append(requestContext.getMethod());
        b.append(' ');
        b.append(requestContext.getUriInfo().getRequestUri());
        if (requestContext.hasEntity()) {
            b.append(':');
            requestContext.setEntityStream(logInboundEntity(b, requestContext.getEntityStream()));
        }
        injector.getInstance(ApiLogService.class).logRequest(b.toString());
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        StringBuilder b = new StringBuilder();
        if (responseContext.hasEntity()) {
            OutputStream stream = new LoggingStream(b, responseContext.getEntityStream());
            responseContext.setEntityStream(stream);
            requestContext.setProperty(ENTITY_LOGGER_PROPERTY, stream);
        }
    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext writerInterceptorContext) throws IOException, WebApplicationException {
        LoggingStream stream = (LoggingStream) writerInterceptorContext.getProperty(ENTITY_LOGGER_PROPERTY);
        writerInterceptorContext.proceed();
        if (stream != null) {
            injector.getInstance(ApiLogService.class).logResponse(stream.getStringBuilder().toString());
        }
    }


    private InputStream logInboundEntity(StringBuilder b, InputStream stream) throws IOException {
        if (!stream.markSupported()) {
            stream = new BufferedInputStream(stream);
        }
        stream.mark(maxEntitySize + 1);
        byte[] entity = new byte[maxEntitySize + 1];
        int entitySize = stream.read(entity);
        b.append(new String(entity, 0, Math.min(entitySize, maxEntitySize)));
        if (entitySize > maxEntitySize) {
            b.append("...more...");
        }
        b.append('\n');
        stream.reset();
        return stream;
    }



    private class LoggingStream extends OutputStream {
        private final StringBuilder b;
        private final OutputStream inner;
        private final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        LoggingStream(StringBuilder b, OutputStream inner) {
            this.b = b;
            this.inner = inner;
        }

        StringBuilder getStringBuilder() {
            // write entity to the builder
            byte[] entity = baos.toByteArray();

            b.append(new String(entity, 0, Math.min(entity.length, maxEntitySize)));
            if (entity.length > maxEntitySize) {
                b.append("...more...");
            }
            b.append('\n');

            return b;
        }

        @Override
        public void write(int i) throws IOException {
            if (baos.size() <= maxEntitySize) {
                baos.write(i);
            }
            inner.write(i);
        }
    }
}
