package ru.rfidcontrol.app.services.impl;

import org.jboss.logging.Logger;
import ru.rfidcontrol.app.services.ApiLogService;


public class ApiLogServiceImpl implements ApiLogService {

    private static Logger logger = Logger.getLogger(ApiLogServiceImpl.class);

    @Override
    public void logRequest(String request) {
        logger.info(request);
    }

    @Override
    public void logResponse(String response) {
        logger.info(response);
    }
}
