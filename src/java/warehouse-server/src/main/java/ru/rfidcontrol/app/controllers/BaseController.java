package ru.rfidcontrol.app.controllers;

import com.google.common.base.Strings;
import com.google.inject.Provider;
import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.exceptions.WarehouseException;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

public class BaseController {

    @Inject @NotNull protected Provider<ClientContext> clientContextProvider;

    @Nullable
    protected ClientContext clientContext() {
        return clientContextProvider.get();
    }

    @NotNull
    protected ClientContext getClientContext() {
        ClientContext clientContext = clientContextProvider.get();
        if (clientContext == null) {
            throw WarehouseException.missingClientContext();
        }

        return clientContext;
    }

    protected void required(@Nullable String string, @NotNull String parameterName) {
        if (Strings.isNullOrEmpty(string)) {
            throw WarehouseException.missingRequiredParameter(parameterName);
        }
    }

    protected void required(int value, @NotNull String parameterName) {
        if (value == 0) {
            throw WarehouseException.missingRequiredParameter(parameterName);
        }
    }

    protected void required(@Nullable Object object, @NotNull String parameterName) {
        if (object == null) {
            throw WarehouseException.missingRequiredParameter(parameterName);
        }
    }

    protected WarehouseException notFound(@NotNull String message) {
        return WarehouseException.create(message);
    }

    protected WarehouseException forbidden() {
        return WarehouseException.create("forbidden resource");
    }

    protected void populateFromContext(@NotNull BaseRequest request) {
        request.populateFromContext(getClientContext());
    }
}
