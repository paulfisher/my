package ru.rfidcontrol.app.controllers.client;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.product.ProductCategoryRequest;
import ru.rfidcontrol.app.data.product.ProductRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.product.ProductInfo;
import ru.rfidcontrol.core.data.product.ProductInfoList;
import ru.rfidcontrol.core.entity.Product;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.ProductCategoryService;
import ru.rfidcontrol.core.services.ProductService;
import ru.rfidcontrol.core.services.StorageService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/product")
@AccessControl
@AllowFor({AccessTypes.DEVICE})
public class ProductController extends BaseController {

    @Inject @NotNull protected ProductService productService;
    @Inject @NotNull protected ProductCategoryService productCategoryService;
    @Inject @NotNull protected CompanyService companyService;
    @Inject @NotNull protected StorageService storageService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findProducts(@NotNull ProductRequest findProductRequest) {
        Map<Integer, Product> products = productService.findProducts(findProductRequest.getProductInfo());

        return DataResponse.create(new ProductInfoList(products));
    }

    @POST
    @Path("/create")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @AllowFor({AccessTypes.AUTHORIZED})
    public DataResponse createProduct(ProductRequest productRequest) {

        populateFromContext(productRequest);

        Product product = productService.createProduct(
                storageService.getStorage(getClientContext().getStorageId()),
                companyService.getCompany(getClientContext().getCompanyId()),
                productRequest.getProductInfo());

        return DataResponse.create(new ProductInfo(product));
    }

    @POST
    @Path("/edit")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse editProduct(ProductRequest productRequest) {
        required(productRequest.getProductInfo().id, "productInfo.id");

        Product product = productService.getProduct(productRequest.getProductInfo().id);

        productService.editProduct(product, productRequest.getProductInfo());

        return DataResponse.create(new ProductInfo(product));
    }

    @POST
    @Path("/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse deleteProduct(ProductRequest productRequest) {
        required(productRequest.getProductInfo().id, "productInfo.id");

        Product product = productService.getProduct(productRequest.getProductInfo().id);
        productService.deleteProduct(product);

        return DataResponse.create(new ProductInfo(product));
    }


    @POST
    @Path("/findByCategory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findProductsByCategory(@NotNull ProductCategoryRequest productCategoryRequest) {

        Storage storage = storageService.getStorage(getClientContext().getStorageId());
        ProductCategory productCategory = productCategoryService.getProductCategory(productCategoryRequest.getProductCategoryInfo().id);

        Map<Integer, Product> products = productService
                .findProducts(storage, productCategory, productCategoryRequest.pagination);

        return DataResponse.create(new ProductInfoList(products));
    }
}