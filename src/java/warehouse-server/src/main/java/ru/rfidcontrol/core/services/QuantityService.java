package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.product.ProductCategoryQuantityInfo;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.entity.ProductQuantity;
import ru.rfidcontrol.core.entity.Storage;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface QuantityService {

    @NotNull
    ProductQuantity updateQuantity(@NotNull Storage storage, @NotNull ProductCategory productCategory, double quantityValue);

    @NotNull
    ProductCategoryQuantityInfo calculateProductCategoryQuantityInfo(@NotNull Storage storage, @NotNull ProductCategory productCategory);

    @NotNull
    List<ProductCategoryQuantityInfo> calculateProductCategoryQuantityInfo(@NotNull Storage storage);
}
