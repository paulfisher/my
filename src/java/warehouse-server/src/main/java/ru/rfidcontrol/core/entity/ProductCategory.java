package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product_category")
public class ProductCategory {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "reference")
    private String reference;

    @Column(name = "price")
    private Double price;

    @Column(name = "vat")
    private Double vat;

    @Column(name = "article")
    private String article;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "parent_product_category_id")
    private ProductCategory parentProductCategory;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "sizer_id")
    private Sizer sizer;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentProductCategory")
    private List<ProductCategory> productCategories;

    public ProductCategory() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public ProductCategory setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    @NotNull
    public ProductCategory setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    @NotNull
    public ProductCategory setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    @Nullable
    public String getReference() {
        return reference;
    }

    @NotNull
    public ProductCategory setReference(@NotNull String reference) {
        this.reference = reference;
        return this;
    }

    @Nullable
    public Double getPrice() {
        return price;
    }

    @NotNull
    public ProductCategory setPrice(@Nullable Double price) {
        this.price = price;
        return this;
    }

    @Nullable
    public Double getVat() {
        return vat;
    }

    @NotNull
    public ProductCategory setVat(@Nullable Double vat) {
        this.vat = vat;
        return this;
    }

    @Nullable
    public String getArticle() {
        return article;
    }

    @NotNull
    public ProductCategory setArticle(@Nullable String article) {
        this.article = article;
        return this;
    }

    @NotNull
    public Company getCompany() {
        return company;
    }

    public ProductCategory setCompany(@NotNull Company company) {
        this.company = company;
        return this;
    }

    @Nullable
    public Sizer getSizer() {
        return sizer;
    }

    @NotNull
    public ProductCategory setSizer(@Nullable Sizer sizer) {
        this.sizer = sizer;
        return this;
    }

    @NotNull
    public List<ProductCategory> getProductCategories() {
        if (productCategories == null) {
            productCategories = new ArrayList<>();
        }
        return productCategories;
    }

    @NotNull
    public ProductCategory setProductCategories(@NotNull List<ProductCategory> productCategories) {
        if (this.productCategories == null) {
            this.productCategories = new ArrayList<>(productCategories);
        }
        this.productCategories.clear();
        this.productCategories.addAll(productCategories);
        return this;
    }

    @Nullable
    public ProductCategory getParentProductCategory() {
        return parentProductCategory;
    }

    public void setParentProductCategory(@NotNull ProductCategory parentProductCategory) {
        this.parentProductCategory = parentProductCategory;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("reference", reference)
                .toString();
    }

    @NotNull
    public static ProductCategory fromInfo(@NotNull ProductCategoryInfo productCategoryInfo) {
        return new ProductCategory()
                .setName(productCategoryInfo.name)
                .setReference(productCategoryInfo.reference)
                .setDeleted(productCategoryInfo.isDeleted)
                .setArticle(productCategoryInfo.article);
    }

    @NotNull
    public ProductCategoryInfo toInfo() {
        return new ProductCategoryInfo(this);
    }
}
