package ru.rfidcontrol.core.data.inventory;

import ru.rfidcontrol.core.entity.Inventory;
import ru.rfidcontrol.core.entity.type.InventoryStatus;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class InventoryInfo {

    public int status;
    public int countNotFound;
    public int countMatch;
    public int countCategoryConflict;
    public int id;
    public boolean isDeleted;
    public long finishDate;
    public long startDate;

    public int companyId;
    public String companyName;

    public int storageId;
    public String storageName;

    @NotNull public InventoryItemInfoList inventoryItemInfoList;

    public InventoryInfo() {
        this.id = 0;
        this.isDeleted = false;
        this.countCategoryConflict = 0;
        this.countMatch = 0;
        this.countNotFound = 0;
        this.status = InventoryStatus.CREATED.value;
        this.companyId = 0;
        this.companyName = "";
        this.storageId = 0;
        this.storageName = "";
        this.finishDate = new Date().getTime();
        this.startDate = new Date().getTime();

        this.inventoryItemInfoList = new InventoryItemInfoList();
    }

    public InventoryInfo(@NotNull Inventory inventory) {
        this.id = inventory.getId();
        this.isDeleted = inventory.isDeleted();
        this.countCategoryConflict = inventory.getCountCategoryConflict();
        this.countMatch = inventory.getCountMatch();
        this.countNotFound = inventory.getCountNotFound();
        this.status = inventory.getStatus().value;
        this.companyId = inventory.getStorage().getCompany().getId();
        this.companyName = inventory.getStorage().getCompany().getName();
        this.storageId = inventory.getStorage().getId();
        this.storageName = inventory.getStorage().getName();
        if (inventory.getFinishDate() != null) {
            this.finishDate = inventory.getFinishDate().getTime();
        }
        this.startDate = inventory.getStartDate().getTime();

        this.inventoryItemInfoList = new InventoryItemInfoList(inventory.getInventoryItems());
    }
}