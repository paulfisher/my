package ru.rfidcontrol.app.filters;

import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.services.AccessControlService;

import javax.annotation.Nullable;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@Priority(Priorities.USER)
@SuppressWarnings("unused")
public class AccessControlFilter implements ContainerRequestFilter {

    @Inject @NotNull
    private com.google.inject.Provider<ClientContext> clientContext;

    @Inject @NotNull
    private AccessControlService accessControlService;

    @Nullable
    private final ResourceInfo resourceInfo;

    public AccessControlFilter() {
        this.resourceInfo = null;
    }

    public AccessControlFilter(@Nullable ResourceInfo resourceInfo) {
        this.resourceInfo = resourceInfo;
    }

    @Override
    public void filter(@NotNull ContainerRequestContext requestContext) throws IOException {
        if (resourceInfo != null && !accessControlService.hasAccess(clientContext.get(), resourceInfo.getResourceClass(), resourceInfo.getResourceMethod())) {
            requestContext.abortWith(Response.status(403).entity("not found").build());
        }
    }
}
