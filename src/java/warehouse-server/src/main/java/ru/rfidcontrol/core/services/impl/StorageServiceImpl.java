package ru.rfidcontrol.core.services.impl;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.ServiceBase;
import ru.rfidcontrol.core.services.StorageService;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StorageServiceImpl extends ServiceBase implements StorageService {

    @Inject CompanyService companyService;

    @Override
    @Transactional
    public Storage createStorage(@NotNull StorageInfo storageInfo) throws WarehouseException {
        Company company = companyService.getCompany(storageInfo.companyId);
        Storage storage = Storage
                .fromInfo(storageInfo)
                .setCompany(company);
        save(storage);

        return storage;
    }

    @Override
    @Transactional
    public Storage editStorage(@NotNull Storage storage, @NotNull StorageInfo storageInfo) {
        return storage.setCompany(companyService.getCompany(storageInfo.companyId))
                .setName(storageInfo.name);
    }

    @Nullable
    @Override
    public Storage findStorage(int id) {
        return find(Storage.class, id);
    }

    @Override
    public Map<Integer, Storage> findStorages(@NotNull StorageInfo storageInfo) {
        Map<Integer, Storage> storages = new HashMap<>();
        if (storageInfo.id > 0) {
            Storage storage = findStorage(storageInfo.id);
            if (storage != null) {
                storages.put(storage.getId(), storage);
            }
        }
        if (storageInfo.companyId > 0) {
            Company company = companyService.findCompany(storageInfo.companyId);
            if (company != null) {
                for (Storage storage : company.getStorages()) {
                    storages.put(storage.getId(), storage);
                }
            }
        }
        if (!Strings.isNullOrEmpty(storageInfo.name)) {
            for (Storage storage : findStoragesByName(storageInfo.name)) {
                storages.put(storage.getId(), storage);
            }
        }
        return storages;
    }

    @Override
    @Transactional
    public Storage deleteStorage(Storage storage) {
        return storage.setDeleted(true);
    }

    @NotNull
    @Override
    public Storage getStorage(@NotNull String reference) {
        Storage storage = findStorage(reference);
        if (storage == null) {
            throw WarehouseException.notFound(Storage.class, reference);
        }
        return storage;
    }

    @Nullable
    @Override
    public Storage findStorage(@NotNull String reference) {
        return first(em().createQuery("from Storage where reference = :reference", Storage.class)
                .setParameter("reference", reference));
    }

    @Override
    public Storage getStorage(int storageId) {
        Storage storage = findStorage(storageId);
        if (storage == null) {
            throw WarehouseException.notFound(Storage.class, storageId);
        }
        return storage;
    }

    private List<Storage> findStorages(boolean isDeleted) {
        return em().createQuery("from Storage where isDeleted = :isDeleted", Storage.class)
                .setParameter("isDeleted", isDeleted)
                .getResultList();
    }

    private List<Storage> findStoragesByName(@NotNull String name) {
        return em().createQuery("from Storage where lower(name) like :name", Storage.class)
                .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                .getResultList();
    }
}
