package ru.rfidcontrol.core.data.company;

import ru.rfidcontrol.core.data.BaseInfo;

public class CompanyStatsInfo extends BaseInfo {

    public int tagsCount;
    public int storagesCount;
    public int devicesCount;
    public int usersCount;
    public int inventoriesCount;
    public int transfersCount;

}
