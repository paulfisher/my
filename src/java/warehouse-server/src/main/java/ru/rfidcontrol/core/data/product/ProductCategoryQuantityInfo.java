package ru.rfidcontrol.core.data.product;

import ru.rfidcontrol.core.entity.ProductQuantity;

import javax.validation.constraints.NotNull;

public class ProductCategoryQuantityInfo {

    public ProductQuantityInfo productQuantityInfo;
    public int tagsCount;

    public ProductCategoryQuantityInfo() {}

    public ProductCategoryQuantityInfo(@NotNull ProductQuantity productQuantity, int tagsCount) {
        this.productQuantityInfo = new ProductQuantityInfo(productQuantity);
        this.tagsCount = tagsCount;
    }
}
