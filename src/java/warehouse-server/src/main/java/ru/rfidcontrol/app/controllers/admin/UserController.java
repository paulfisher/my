package ru.rfidcontrol.app.controllers.admin;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.user.UserRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.user.UserInfoList;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.UserService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/admin/user")
@AccessControl
@AllowFor({AccessTypes.ADMIN})
public class UserController extends BaseController {

    @Inject @NotNull protected UserService userService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findUsers(@NotNull UserRequest userRequest) {
        Map<Integer, User> findResult = userService.findUsers(userRequest.userInfo());

        return DataResponse.create(new UserInfoList(findResult));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findUser(@NotNull UserRequest userRequest) {
        required(userRequest.userInfo().id, "info.id");

        User user = userService.getUser(userRequest.userInfo().id);

        return DataResponse.create(user.toInfo());
    }

    @POST
    @Path("/create")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse createUser(@NotNull UserRequest userRequest) {
        User created = userService.createUser(userRequest.userInfo());

        return DataResponse.create(created.toInfo());
    }

    @POST
    @Path("/edit")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse editUser(@NotNull UserRequest userRequest) {
        required(userRequest.userInfo().id, "info.id");

        User user = userService.getUser(userRequest.userInfo().id);
        userService.editUser(user, userRequest.userInfo());

        return DataResponse.create(user.toInfo());
    }

    @POST
    @Path("/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse deleteUser(@NotNull UserRequest userRequest) {
        required(userRequest.userInfo().id, "info.id");

        User user = userService.getUser(userRequest.userInfo().id);

        ClientContext clientContext = clientContext();
        if (clientContext != null && clientContext.getUserId() == user.getId()) {
            throw WarehouseException.create("Cannot delete yourself");
        }

        userService.deleteUser(user);

        return DataResponse.create(user.toInfo());
    }
}