package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.product.PriceInfo;
import ru.rfidcontrol.core.entity.ProductCategory;

import javax.validation.constraints.NotNull;

public interface PriceService {

    @NotNull
    ProductCategory updatePrice(@NotNull ProductCategory productCategory, @NotNull PriceInfo priceInfo);

}
