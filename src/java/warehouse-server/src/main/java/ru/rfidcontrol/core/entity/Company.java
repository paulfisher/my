package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.data.company.CompanyInfo;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "company")
public class Company {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "reference")
    private String reference;

    @Column(name = "create_time", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @OneToMany(mappedBy = "company")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<User> users;

    @OneToMany(mappedBy = "company")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Storage> storages;

    @OneToMany(mappedBy = "company")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Device> devices;

    public Company() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Company setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    @NotNull
    public Company setName(String name) {
        this.name = name;
        return this;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    @NotNull
    public Company setEmail(@NotNull String email) {
        this.email = email;
        return this;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    @NotNull
    public Company setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    @Nullable
    public String getReference() {
        return reference;
    }

    @NotNull
    public Company setReference(@NotNull String reference) {
        this.reference = reference;
        return this;
    }

    @NotNull
    public Date getCreateTime() {
        return createTime;
    }

    @NotNull
    public Company setCreateTime(@NotNull Date createTime) {
        this.createTime = createTime;
        return this;
    }

    @NotNull
    public List<User> getUsers() {
        if (users == null) {
            users = new ArrayList<>();
        }
        return users;
    }

    @NotNull
    public Company setUsers(@NotNull List<User> users) {
        if (this.users == null) {
            this.users = new ArrayList<>();
        }
        this.users.clear();
        this.users.addAll(users);
        return this;
    }

    @NotNull
    public List<Storage> getStorages() {
        if (storages == null) {
            storages = new ArrayList<>();
        }
        return storages;
    }

    @NotNull
    public Company setStorages(@NotNull List<Storage> storages) {
        if (this.storages == null) {
            this.storages = new ArrayList<>();
        }
        this.storages.clear();
        this.storages.addAll(storages);
        return this;
    }

    @NotNull
    public List<Device> getDevices() {
        if (devices == null) {
            devices = new ArrayList<>();
        }
        return devices;
    }

    @NotNull
    public Company setDevices(@NotNull List<Device> devices) {
        if (this.devices == null) {
            this.devices = new ArrayList<>();
        }
        this.devices.clear();
        this.devices.addAll(devices);
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("email", email)
                .add("isDeleted", isDeleted)
                .toString();
    }

    public static Company fromInfo(CompanyInfo companyInfo) {
        return new Company()
                .setName(companyInfo.name)
                .setEmail(companyInfo.email)
                .setDeleted(companyInfo.isDeleted);
    }

    public CompanyInfo toInfo() {
        return new CompanyInfo(this);
    }
}
