package ru.rfidcontrol.core.services.impl;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.Sorting;
import ru.rfidcontrol.core.data.product.PriceInfo;
import ru.rfidcontrol.core.data.product.ProductInfo;
import ru.rfidcontrol.core.data.product.QuantityInfo;
import ru.rfidcontrol.core.data.transfer.TagTransferInfo;
import ru.rfidcontrol.core.data.transfer.TransferInfo;
import ru.rfidcontrol.core.entity.*;
import ru.rfidcontrol.core.entity.type.TransferDirection;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.*;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransferServiceImpl extends ServiceBase implements TransferService {

    @Inject @NotNull protected StorageService storageService;
    @Inject @NotNull protected UserService userService;
    @Inject @NotNull protected TagService tagService;
    @Inject @NotNull protected CompanyService companyService;
    @Inject @NotNull protected DeviceService deviceService;
    @Inject @NotNull protected SupplierService supplierService;
    @Inject @NotNull protected SizerService sizerService;
    @Inject @NotNull protected Provider<ClientContext> clientContextProvider;

    public static final ImmutableSet<String> FIND_TRANSFERS_ALLOWED_SORTS = ImmutableSet.of(
            "when", "id", "tagTransfers.size", "user", "device"
    );

    @NotNull
    @Override
    @Transactional
    public Transfer createTransfer(@NotNull TransferInfo transferInfo) {
        Transfer transfer = null;

        if (!Strings.isNullOrEmpty(transferInfo.billNumber)) {
            //по номеру накладной находим трансфер
            transfer = findTransfer(transferInfo.billNumber);
        }

        if (transfer == null) {
            transfer = Transfer.fromInfo(transferInfo)
                    .setStorage(storageService.getStorage(transferInfo.storageId));
        }

        if (transferInfo.userId > 0) {
            //TODO: проверить user на соответствие контексту
            transfer.setUser(userService.getUser(transferInfo.userId));
        }

        if (transferInfo.deviceId > 0) {
            //TODO: проверить device на соответствие контексту
            transfer.setDevice(deviceService.getDevice(transferInfo.deviceId));
        }

        if (transferInfo.supplierId > 0) {
            transfer.setSupplier(supplierService.getSupplier(transferInfo.supplierId));
        }

        save(transfer);

        return transfer;
    }

    @Nullable
    private Transfer findTransfer(@NotNull String billNumber) {
        return first(
            em().createQuery("from Transfer where bill_number = :bill_number", Transfer.class)
                .setParameter("bill_number", billNumber)
        );
    }

    @Override
    @NotNull
    @Transactional
    public TagTransfer createTagTransfer(@NotNull Transfer transfer, @NotNull Tag tag, @NotNull PriceInfo priceInfo, @NotNull QuantityInfo quantityInfo) {
        if (tag.getCompany() != transfer.getStorage().getCompany()) {
            throw WarehouseException.notFound(Tag.class, tag.getId());
        }

        TagTransfer tagTransfer = new TagTransfer()
                .setTransfer(transfer)
                .setTag(tag)
                .setPrice(priceInfo.price)
                .setVat(priceInfo.vat)
                .setQuantity(quantityInfo.quantity);
        if (quantityInfo.sizer != null && quantityInfo.sizer.okeiCode > 0) {
            tagTransfer.setSizer(sizerService.findSizerByOkeiCode(quantityInfo.sizer.okeiCode));
        }

        save(tagTransfer);
        transfer.getTagTransfers().add(tagTransfer);

        // устанавливаем текущий склад для тэга.
        // впоследствии, если появятся оффлайн-режимы терминалов, нужно будет учитывать время трансфера
        if (transfer.getDirection() == TransferDirection.IN) {
            tag.setStorage(transfer.getStorage());
        } else if (transfer.getDirection() == TransferDirection.OUT) {
            tag.setStorage(null);
        }
        //TODO: проверять валидность операции. делать весь transfer в transaction.

        return tagTransfer;
    }

    @NotNull
    @Override
    public Transfer editTransfer(@NotNull Transfer transfer, @NotNull TransferInfo transferInfo) {

        if (transferInfo.userId > 0) {
            //TODO: проверить user на соответствие контексту
            transfer.setUser(userService.getUser(transferInfo.userId));
        }

        if (transferInfo.deviceId > 0) {
            //TODO: проверить device на соответствие контексту
            transfer.setDevice(deviceService.getDevice(transferInfo.deviceId));
        }

        if (transferInfo.supplierId > 0) {
            transfer.setSupplier(supplierService.getSupplier(transferInfo.supplierId));
        }

        return transfer;
    }

    @NotNull
    @Override
    @Transactional
    public TagTransfer editTagTransfer(@NotNull TagTransfer tagTransfer, @NotNull ProductInfo productInfo) {
        if (productInfo.priceInfo != null) {
            tagTransfer
                    .setPrice(productInfo.priceInfo.price)
                    .setVat(productInfo.priceInfo.vat);
        }
        if (productInfo.quantityInfo != null) {
            tagTransfer.setQuantity(productInfo.quantityInfo.quantity);

            if (productInfo.quantityInfo.sizer != null) {
                tagTransfer.setSizer(sizerService.getSizerByOkeiCode(productInfo.quantityInfo.sizer.okeiCode));
            }
        }
        return tagTransfer;
    }

    @Override
    @Transactional
    public void deleteTagTransfer(@NotNull TagTransfer tagTransfer) {
        Transfer transfer = tagTransfer.getTransfer();
        transfer.getTagTransfers().remove(tagTransfer);
        //todo: помечать флагом
        remove(tagTransfer);
    }

    @Override
    @Transactional
    public void deleteTransfer(@NotNull Transfer transfer) {
        //todo: помечать флагом
        remove(transfer);
    }

    @NotNull
    @Override
    public List<TagTransfer> getTagTransfers(@NotNull Tag tag, @NotNull Pagination pagination) {
        pagination.total = count(em().createQuery("select count(id) from TagTransfer where tag = :tag", Number.class).setParameter("tag", tag));

        return em().createQuery("from TagTransfer where tag = :tag order by id desc", TagTransfer.class)
                .setParameter("tag", tag)
                .setFirstResult(pagination.offset)
                .setMaxResults(pagination.count)
                .getResultList();
    }

    @NotNull
    @Override
    public Map<Integer, Transfer> findTransfers(@NotNull TransferInfo transferInfo, @NotNull Pagination pagination, @NotNull Sorting sorting) {
        List<String> conditions = new ArrayList<>();
        Map<String, Object> parameters = new HashMap<>();

        if (transferInfo.storageId > 0) {
            conditions.add("storage = :storage");
            parameters.put("storage", storageService.getStorage(transferInfo.storageId));
        } else if (transferInfo.companyId > 0) {
            conditions.add("storage in :storages");
            parameters.put("storages", companyService.getCompany(transferInfo.companyId).getStorages());
        }

        if (transferInfo.directionType > 0) {
            conditions.add("direction = :direction");
            parameters.put("direction", TransferDirection.get(transferInfo.directionType).value);
        }

        if (transferInfo.userId > 0) {
            conditions.add("user = :user");
            parameters.put("user", userService.getUser(transferInfo.userId));
        }

        if (transferInfo.deviceId > 0) {
            conditions.add("device = :device");
            parameters.put("device", deviceService.getDevice(transferInfo.deviceId));
        }

        String allConditionsLogicalAnd = StringHelper.implode(" and ", conditions);

        String sortingQueryPart = "";
        if (sorting.enabled && FIND_TRANSFERS_ALLOWED_SORTS.contains(sorting.property)) {
            sortingQueryPart = String.format("order by t.%s %s", sorting.property, sorting.asc ? "asc" : "desc");
        }
        String query = conditions.size() > 0
                ? String.format("from Transfer t where %s %s", allConditionsLogicalAnd, sortingQueryPart)
                : String.format("from Transfer t %s", sortingQueryPart);
        String countQuery = String.format("select count(id) from Transfer where %s", allConditionsLogicalAnd);

        TypedQuery<Transfer> q = q(query, Transfer.class);
        TypedQuery<Number> countQ = q(countQuery, Number.class);

        for (Map.Entry<String, Object> parameterEntry : parameters.entrySet()) {
            q.setParameter(parameterEntry.getKey(), parameterEntry.getValue());
            countQ.setParameter(parameterEntry.getKey(), parameterEntry.getValue());
        }

        pagination.total = count(countQ);

        Map<Integer, Transfer> result = new HashMap<>();
        for (Transfer transfer : decorateQueryWithLimits(q, pagination.offset, pagination.count).getResultList()) {
            result.put(transfer.getId(), transfer);
        }

        return result;
    }

    @NotNull
    @Override
    public Transfer getTransfer(int transferId) {
        Transfer transfer = find(Transfer.class, transferId);
        if (transfer == null) {
            throw WarehouseException.transferNotFound(transferId);
        }
        return transfer;
    }

    @NotNull
    @Override
    public Transfer getTransfer(@NotNull String billNumber, @NotNull Storage storage) {
        return first(em().createQuery("from Transfer where billNumber = :billNumber and storage = :storage", Transfer.class)
            .setParameter("billNumber", billNumber)
            .setParameter("storage", storage));
    }

    private <T> TypedQuery<T> decorateQueryWithLimits(TypedQuery<T> q, int offset, int count) {
        if (offset > 0) {
            q.setFirstResult(offset);
        }
        if (count > 0) {
            q.setMaxResults(count);
        }
        return q;
    }
}
