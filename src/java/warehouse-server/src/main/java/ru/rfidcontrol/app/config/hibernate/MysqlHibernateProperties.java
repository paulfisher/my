package ru.rfidcontrol.app.config.hibernate;

import ru.rfidcontrol.app.config.DbConfig;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;


public class MysqlHibernateProperties implements HibernateProperties {

    @Override @Nonnull
    @SuppressWarnings("SpellCheckingInspection")
    public Map<String, String> constructProperties(@Nonnull DbConfig dbConfig) {
        Map<String, String> properties = new HashMap<>();

        properties.put("hibernate.archive.autodetection", "class");
        properties.put("hibernate.show_sql", "false");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
        properties.put("hibernate.connection.charSet", "UTF-8");
        properties.put("hibernate.connection.characterEncoding", "utf8");
        properties.put("hibernate.connection.useUnicode", "true");

        properties.put("hibernate.connection.url", dbConfig.getUrl());
        properties.put("hibernate.connection.username", dbConfig.getUser());
        properties.put("hibernate.connection.password", dbConfig.getPassword());

        return properties;
    }
}
