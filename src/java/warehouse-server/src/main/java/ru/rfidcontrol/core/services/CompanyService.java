package ru.rfidcontrol.core.services;


import ru.rfidcontrol.core.data.company.CompanyInfo;
import ru.rfidcontrol.core.data.company.CompanyStatsInfo;
import ru.rfidcontrol.core.entity.Company;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface CompanyService {

    @NotNull Company getCompany(int id);

    @Nullable Company findCompany(int id);

    @NotNull Map<Integer, Company> findCompanies(@NotNull CompanyInfo companyInfo);

    @NotNull Company createCompany(@NotNull CompanyInfo companyInfo);

    @NotNull Company editCompany(@NotNull Company company, @NotNull CompanyInfo companyInfo);

    @NotNull Company deleteCompany(@NotNull Company company);

    @Nullable Company findCompany(@NotNull String reference);

    @NotNull Company getCompany(@NotNull String reference);

    @NotNull CompanyStatsInfo getCompanyStats(@NotNull Company company);
}
