package ru.rfidcontrol.core.data.transfer;

import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.Supplier;
import ru.rfidcontrol.core.entity.Transfer;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.entity.type.TransferDirection;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class TransferInfo {

    public int id;
    public int directionType;

    public long when;
    public String whenString;

    @NotNull public TagTransferInfoList tagInfoList;

    public int companyId;
    public String companyName;

    public int userId;
    @Nullable public String username;

    public int deviceId;
    public String deviceName;
    public String deviceImei;

    public int storageId;
    public String storageName;

    public String billNumber;

    public int supplierId;
    public String supplierName;

    public TransferInfo(TransferDirection direction) {
        this.directionType = direction.value;
        this.tagInfoList = new TagTransferInfoList();
    }

    public TransferInfo() {
        this.tagInfoList = new TagTransferInfoList();
    }

    public TransferInfo(Transfer transfer) {
        this.id = transfer.getId();

        this.directionType = transfer.getDirection().value;
        this.storageId = transfer.getStorage().getId();
        this.storageName = transfer.getStorage().getName();

        this.when = transfer.getWhen().getTime();
        this.tagInfoList = new TagTransferInfoList(transfer.getTagTransfers());

        User user = transfer.getUser();
        if (user != null) {
            this.userId = transfer.getUser().getId();
            this.username = transfer.getUser().getName();
        }

        Device device = transfer.getDevice();
        if (device != null) {
            this.deviceId = transfer.getDevice().getId();
            this.deviceName = transfer.getDevice().getName();
            this.deviceImei = transfer.getDevice().getImeiCode();
        }

        this.companyId = transfer.getStorage().getCompany().getId();
        this.companyName = transfer.getStorage().getCompany().getName();

        this.billNumber = transfer.getBillNumber();
        Supplier supplier = transfer.getSupplier();
        if (supplier != null) {
            this.supplierId = supplier.getId();
            this.supplierName = supplier.getName();
        }
        Date when = new Date();
        when.setTime(this.when);
        this.whenString = when.toString();
    }

    @NotNull
    public TagTransferInfoList getTagInfoList() {
        if (tagInfoList == null) {
            tagInfoList = new TagTransferInfoList();
        }
        return tagInfoList;
    }
}