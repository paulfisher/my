package ru.rfidcontrol.core.data.product;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.ProductCategory;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class ProductCategoryInfoList extends BaseInfoList {

    @Nullable public Map<Integer, ProductCategoryInfo> categories;

    public int size;

    public ProductCategoryInfoList() {
    }

    public ProductCategoryInfoList(Map<Integer, ProductCategory> categories) {
        this.categories = new HashMap<>();
        for (Map.Entry<Integer, ProductCategory> category : categories.entrySet()) {
            this.categories.put(category.getKey(), new ProductCategoryInfo(category.getValue()));
        }
        this.size = this.categories.size();
    }

}
