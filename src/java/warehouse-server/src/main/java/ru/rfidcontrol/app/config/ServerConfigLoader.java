package ru.rfidcontrol.app.config;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class ServerConfigLoader {

    @Nonnull
    public static ServerConfig load(String configName) {
        configName += ".json";

        Class<?> configClass = ServerConfig.class;
        InputStream configInputStream = null;

        //todo: find config in outside dir

        if (configInputStream == null) {
            try {
                configInputStream = Resources.getResource(configName).openStream();
            } catch (IllegalArgumentException ex) {
                URL url = Resources.getResource(configName);
                String message =
                        url.getPath() == null
                                ? "Failed to read config file"
                                : String.format("Resource '%s' not found in directory: %s", configName, url.getPath());
                throw new ConfigurationException(message, ex);
            } catch (IOException e) {
                throw new ConfigurationException("config file error", e);
            }
        }

        ServerConfig config;
        try (InputStreamReader reader = new InputStreamReader(configInputStream, Charsets.UTF_8)) {
            config = (ServerConfig) ServerConfig.load(reader, configClass);
        } catch (IOException e) {
            throw new ConfigurationException("config load error", e);
        }

        return config;
    }
}
