package ru.rfidcontrol.core.data.user;

import ru.rfidcontrol.core.data.BaseInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.User;

import java.util.Date;

public class UserInfo extends BaseInfo {

    public int id;
    public String name;
    public String password;
    public Date createTime;
    public Date lastLoginTime;
    public boolean isAdmin;
    public boolean isDeleted;

    public int companyId = 0;
    public String companyName = "";

    public UserInfo() {
    }

    public UserInfo(User user) {
        id = user.getId();
        name = user.getName();
        password = "";//�� ����� ������
        createTime = user.getCreateTime();
        lastLoginTime = user.getLastLoginTime();
        isAdmin = user.isAdmin();
        isDeleted = user.isDeleted();

        Company company = user.getCompany();
        if (company != null) {
            companyId = company.getId();
            companyName = company.getName();
        }
    }
}