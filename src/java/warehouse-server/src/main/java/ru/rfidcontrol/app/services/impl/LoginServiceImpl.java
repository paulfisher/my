package ru.rfidcontrol.app.services.impl;

import com.google.inject.Provider;
import ru.rfidcontrol.app.services.LoginService;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.*;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class LoginServiceImpl extends ServiceBase implements LoginService {

    @Inject @NotNull protected CompanyService companyService;
    @Inject @NotNull protected Provider<HttpSession> sessionProvider;
    @Inject @NotNull protected DeviceService deviceService;
    @Inject @NotNull protected UserService userService;
    @Inject @NotNull protected StorageService storageService;
    @Inject @NotNull protected Provider<ClientContext> requestContextProvider;

    @Override
    @NotNull
    public User loginUser(String name, String password) {
        User user = userService.findUser(name);
        if (user == null) {
            throw WarehouseException.userLoginFail(name);
        }

        if (!Objects.equals(user.getPassword(), hashUserPassword(password))) {
            throw WarehouseException.userLoginFail(name, password);
        }

        if (user.isDeleted()) {
            throw WarehouseException.userIsDeleted(user);
        }

        loginUser(user);

        return user;
    }

    @Override
    @NotNull
    public Device loginDevice(String imeiCode) {
        Device device = deviceService.findDevice(imeiCode);
        if (device == null) {
            throw WarehouseException.deviceNotFound(imeiCode);
        }

        sessionSet("deviceId", device.getId());
        if (device.getCompany() != null) {
            sessionSet("companyId", device.getCompany().getId());
        }
        if (device.getStorage() != null) {
            sessionSet("storageId", device.getStorage().getId());
        }
        if (device.getUsers().size() == 1) {
            //если у девайса всего один пользователь, сразу логинем его
            loginUser(device.getUsers().get(0));
        }

        return device;
    }

    @Override
    public void loginStorage(StorageInfo storageInfo) {
        ClientContext context = requestContextProvider.get();

        int currentCompanyId = 0;

        if (context.getDeviceId() > 0) {
            Device device = deviceService.getDevice(context.getDeviceId());
            currentCompanyId = device.getCompany().getId();
        }

        Storage storage = storageService.getStorage(storageInfo.id);
        if (storage.getCompany().getId() != currentCompanyId) {
            throw WarehouseException.create(String.format("Couldn't log in to the storage %s", storage));
        }

        sessionSet("storageId", storage.getId());
    }

    @Override
    public void logout() {
        sessionUnset("deviceId");
        sessionUnset("companyId");
        sessionUnset("userId");
    }

    @NotNull
    private void loginUser(@NotNull User user) {
        sessionSet("userId", user.getId());
        sessionSet("admin", user.isAdmin());
        if (user.getCompany() != null) {
            sessionSet("companyId", user.getCompany().getId());
        }
    }

    private void sessionSet(String name, Object value) {
        sessionProvider.get().setAttribute(name, value);
    }

    private void sessionUnset(String name) {
        sessionProvider.get().removeAttribute(name);
    }
}
