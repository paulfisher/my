package ru.rfidcontrol.core.services.impl;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.product.ProductCategoryQuantityInfo;
import ru.rfidcontrol.core.data.product.ProductQuantityInfo;
import ru.rfidcontrol.core.entity.Product;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.entity.ProductQuantity;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.services.*;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QuantityServiceImpl extends ServiceBase implements QuantityService {

    @Inject ProductService productService;

    @NotNull
    @Override
    @Transactional
    public ProductQuantity updateQuantity(@NotNull Storage storage, @NotNull ProductCategory productCategory, double quantityValue) {
        ProductQuantity quantity = findProductQuantity(storage, productCategory);

        if (quantity == null) {
            quantity = new ProductQuantity();
            quantity.setStorage(storage);
            quantity.setProductCategory(productCategory);
            quantity.setCompany(storage.getCompany());
        }
        quantity.setQuantity(quantity.getQuantity());

        if (quantity.getId() == 0) {
            save(quantity);
        }

        return quantity;
    }

    @NotNull
    @Override
    public ProductCategoryQuantityInfo calculateProductCategoryQuantityInfo(@NotNull Storage storage, @NotNull ProductCategory productCategory) {
        ProductCategoryQuantityInfo productCategoryQuantityInfo = new ProductCategoryQuantityInfo();

        productCategoryQuantityInfo.tagsCount = productService.countProducts(storage, productCategory);

        ProductQuantity quantity = findProductQuantity(storage, productCategory);
        if (quantity == null) {
            //������ � ���� ���. ������ ����� ��, ��� ���� � ���.
            productCategoryQuantityInfo.productQuantityInfo = new ProductQuantityInfo(productCategory);
            productCategoryQuantityInfo.productQuantityInfo.storageName = storage.getName();
            productCategoryQuantityInfo.productQuantityInfo.storageId = storage.getId();
            productCategoryQuantityInfo.productQuantityInfo.quantity = productCategoryQuantityInfo.tagsCount;
            return productCategoryQuantityInfo;
        }

        productCategoryQuantityInfo.productQuantityInfo = new ProductQuantityInfo(quantity);

        return productCategoryQuantityInfo;
    }

    @NotNull
    @Override
    public List<ProductCategoryQuantityInfo> calculateProductCategoryQuantityInfo(@NotNull Storage storage) {
        List<ProductCategoryQuantityInfo> result = new ArrayList<>();

        List<ProductCategory> productCategories = em()
                .createQuery("select distinct product.productCategory from Product as product inner join product.tag as tag where tag.storage = :storage", ProductCategory.class)
                .setParameter("storage", storage)
                .getResultList();

        for (ProductCategory productCategory : productCategories) {
            result.add(calculateProductCategoryQuantityInfo(storage, productCategory));
        }

        return result;
    }

    @Nullable
    private ProductQuantity findProductQuantity(@NotNull Storage storage, @NotNull ProductCategory productCategory) {
        return first(em()
                .createQuery("from ProductQuantity where storage = :storage and productCategory = :productCategory", ProductQuantity.class)
                .setParameter("storage", storage)
                .setParameter("productCategory", productCategory));
    }
}
