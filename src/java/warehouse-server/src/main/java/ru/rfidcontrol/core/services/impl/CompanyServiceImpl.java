package ru.rfidcontrol.core.services.impl;

import com.google.common.base.Strings;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.company.CompanyInfo;
import ru.rfidcontrol.core.data.company.CompanyStatsInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.ServiceBase;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompanyServiceImpl extends ServiceBase implements CompanyService {

    @Override
    @Transactional
    @NotNull
    public Company createCompany(@NotNull CompanyInfo companyInfo) {
        Company company = Company.fromInfo(companyInfo);
        save(company);

        return company;
    }

    @Override
    @Nullable
    public Company findCompany(int id) {
        return find(Company.class, id);
    }

    @Override
    @Transactional
    @NotNull
    public Company deleteCompany(Company company) {
        return company.setDeleted(true);
    }

    @Nullable
    @Override
    public Company findCompany(@NotNull String reference) {
        return first(em().createQuery("from Company where reference=:reference", Company.class)
                .setParameter("reference", reference));
    }

    @Override
    @NotNull
    public Company getCompany(@NotNull String reference) {
        Company company = findCompany(reference);
        if (company == null) {
            throw WarehouseException.companyNotFound(reference);
        }
        return company;
    }

    @Override
    @NotNull
    public Map<Integer, Company> findCompanies(@NotNull CompanyInfo companyInfo) {
        Map<Integer, Company> result = new HashMap<>();

        if (companyInfo.id > 0) {
            Company company = findCompany(companyInfo.id);
            if (company != null) {
                result.put(company.getId(), company);
            }
        }
        if (!Strings.isNullOrEmpty(companyInfo.name)) {
            for (Company company : findCompaniesByName(companyInfo.name)) {
                result.put(company.getId(), company);
            }
        }
        if (!Strings.isNullOrEmpty(companyInfo.email)) {
            for (Company company : findCompaniesByEmail(companyInfo.email)) {
                result.put(company.getId(), company);
            }
        }
        if (result.isEmpty()) {
            for (Company company : findCompanies(false)) {
                result.put(company.getId(), company);
            }
            for (Company company : findCompanies(true)) {//� ����� ������ ���������� ��������� ��������
                result.put(company.getId(), company);
            }
        }

        return result;
    }

    @Override
    @NotNull
    public Company getCompany(int id) {
        Company company = findCompany(id);
        if (company == null) {
            throw WarehouseException.companyNotFound(id);
        }
        return company;
    }

    @Override
    @NotNull
    @Transactional
    public Company editCompany(@NotNull Company company, @NotNull CompanyInfo companyInfo) {
        return company
                .setName(companyInfo.name)
                .setEmail(companyInfo.email);
    }

    @Override
    @NotNull
    public CompanyStatsInfo getCompanyStats(@NotNull Company company) {
        CompanyStatsInfo stats = new CompanyStatsInfo();

        stats.devicesCount = count(q("select count(id) from Device where company = :company", Number.class).setParameter("company", company));
        stats.inventoriesCount = count(q("select count(id) from Inventory where storage in :companyStorages", Number.class).setParameter("companyStorages", company.getStorages()));
        stats.storagesCount = count(q("select count(id) from Storage where company = :company", Number.class).setParameter("company", company));
        stats.tagsCount = count(q("select count(id) from Tag where company = :company", Number.class).setParameter("company", company));
        stats.usersCount = count(q("select count(id) from User where company = :company", Number.class).setParameter("company", company));
        stats.transfersCount = count(q("select count(id) from Transfer where storage in :companyStorages", Number.class).setParameter("companyStorages", company.getStorages()));


        return stats;
    }

    private List<Company> findCompaniesByName(@NotNull String name) {
        return em().createQuery("from Company where lower(name) like :name", Company.class)
                .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                .getResultList();
    }

    private List<Company> findCompaniesByEmail(@NotNull String email) {
        return em().createQuery("from Company where lower(email) like :email", Company.class)
                .setParameter("email", String.format("%%%s%%", email.toLowerCase()))
                .getResultList();
    }

    private List<Company> findCompanies(boolean isDeleted) {
        return em().createQuery("from Company where isDeleted = :isDeleted", Company.class)
                .setParameter("isDeleted", isDeleted)
                .getResultList();
    }
}
