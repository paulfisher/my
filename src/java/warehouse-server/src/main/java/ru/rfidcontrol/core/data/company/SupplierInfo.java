package ru.rfidcontrol.core.data.company;

import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Supplier;

public class SupplierInfo {

    public int id;
    public String name;

    public int companyId = 0;
    public String companyName = "";

    public SupplierInfo() {}

    public SupplierInfo(Supplier supplier) {
        id = supplier.getId();
        name = supplier.getName();

        Company company = supplier.getCompany();
        companyId = company.getId();
        companyName = company.getName();
    }
}
