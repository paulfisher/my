package ru.rfidcontrol.app.guice.events;

import com.google.common.eventbus.EventBus;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

import javax.annotation.Nonnull;

public class EventBusTypeListener implements TypeListener {

    private final EventBus eventBus;

    public EventBusTypeListener(@Nonnull EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public <T> void hear(final TypeLiteral<T> typeLiteral, final TypeEncounter<T> typeEncounter) {
        typeEncounter.register(new InjectionListener<T>() {
            @Override
            public void afterInjection(final T instance) {
                eventBus.register(instance);
            }
        });
    }

}