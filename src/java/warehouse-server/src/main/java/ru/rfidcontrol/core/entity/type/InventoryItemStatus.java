package ru.rfidcontrol.core.entity.type;


import static ru.rfidcontrol.core.exceptions.WarehouseException.notFound;

public enum InventoryItemStatus {

    NOT_FOUND(1),
    FOUND(2),
    CATEGORY_CONFLICT(3);

    public final int value;

    InventoryItemStatus(int value) {
        this.value = value;
    }

    public static InventoryItemStatus get(int value) {
        for (InventoryItemStatus status : values()) {
            if (status.value == value) {
                return status;
            }
        }
        throw notFound(InventoryItemStatus.class, value);
    }


}
