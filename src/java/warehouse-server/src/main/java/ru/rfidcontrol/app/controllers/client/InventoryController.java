package ru.rfidcontrol.app.controllers.client;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.inventory.InventoryRequest;
import ru.rfidcontrol.app.data.inventory.TagFoundRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.inventory.InventoryInfo;
import ru.rfidcontrol.core.data.inventory.InventoryInfoList;
import ru.rfidcontrol.core.data.inventory.InventoryItemInfo;
import ru.rfidcontrol.core.entity.Inventory;
import ru.rfidcontrol.core.entity.InventoryItem;
import ru.rfidcontrol.core.services.InventoryService;
import ru.rfidcontrol.core.services.StorageService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/inventory")
@AccessControl
@AllowFor({AccessTypes.DEVICE})
public class InventoryController extends BaseController {

    @Inject @NotNull protected InventoryService inventoryService;
    @Inject @NotNull protected StorageService storageService;


    @POST
    @Path("/list")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse list(@NotNull InventoryRequest inventoryRequest) {
        populateFromContext(inventoryRequest);
        return DataResponse.create(
                new InventoryInfoList(
                        inventoryService.findInventories(inventoryRequest.getInventoryInfo())
                )
        );
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getInventory(@NotNull InventoryRequest inventoryRequest) {
        populateFromContext(inventoryRequest);

        required(inventoryRequest.getInventoryInfo().id, "inventoryInfo.id");

        Inventory inventory = inventoryService.getInventory(inventoryRequest.getInventoryInfo().id);
        if (inventory.getStorage().getCompany().getId() != getClientContext().getCompanyId()) {
            throw forbidden();
        }

        return DataResponse.create(inventory.toInfo());
    }

    @POST
    @Path("/start")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse start(@NotNull InventoryRequest inventoryRequest) {
        populateFromContext(inventoryRequest);

        Inventory inventory = inventoryService.startInventory(inventoryRequest.getInventoryInfo());

        return DataResponse.create(new InventoryInfo(inventory));
    }

    @POST
    @Path("/tag/found")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse tagFound(@NotNull TagFoundRequest tagFoundRequest) {
        /**
         * Request example:
         * TagFoundRequest tagFoundRequest {
         *     InventoryItemInfo inventoryItem {
         *         ProductCategoryInfo actualProductCategory {
         *             id: 123
         *         }
         *     }
         *     TagInfo tag {
         *         rfId: "someRfIdCode"
         *     }
         * }
         */
        populateFromContext(tagFoundRequest);

        required(tagFoundRequest.getInventoryItem().actualProductCategory, "inventoryItem.actualProductCategory");
        required(tagFoundRequest.getInventoryItem().actualProductCategory.id, "inventoryItem.actualProductCategory.id");

        InventoryItem inventoryItem = inventoryService.inventoryTagFound(
                inventoryService.getInventory(tagFoundRequest.getInventoryItem().inventoryId),
                tagFoundRequest.getTag(),
                tagFoundRequest.getInventoryItem()
        );

        return DataResponse.create(new InventoryItemInfo(inventoryItem));
    }

    @POST
    @Path("/cancel")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse cancel(@NotNull InventoryRequest inventoryRequest) {

        Inventory inventory = inventoryService.getInventory(inventoryRequest.getInventoryInfo().id);

        inventoryService.cancelInventory(inventory);

        return DataResponse.create(new InventoryInfo(inventory));
    }

    @POST
    @Path("/finish")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse finish(@NotNull InventoryRequest inventoryRequest) {

        Inventory inventory = inventoryService.getInventory(inventoryRequest.getInventoryInfo().id);

        inventoryService.finishInventory(inventory);

        return DataResponse.create(new InventoryInfo(inventory));
    }
}