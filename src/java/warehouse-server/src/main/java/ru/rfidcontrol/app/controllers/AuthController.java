package ru.rfidcontrol.app.controllers;

import com.google.common.base.Strings;
import ru.rfidcontrol.app.data.auth.LoginRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.storage.StorageRequest;
import ru.rfidcontrol.app.services.LoginService;
import ru.rfidcontrol.core.ClientContext;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/auth")
public class AuthController extends BaseController {

    @Inject @NotNull protected LoginService loginService;

    @GET
    @Path("/test")
    @Produces({MediaType.TEXT_PLAIN})
    public Response test() {
        if (clientContextProvider.get() == null) {
            return Response.ok("empty context").build();
        } else {
            return Response.ok(clientContextProvider.get().isAdmin()).build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public ClientContext check() {
        return clientContextProvider.get();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse auth(LoginRequest loginRequest) {
        if (!Strings.isNullOrEmpty(loginRequest.imei)) {
            //логиним девайс
            loginService.loginDevice(loginRequest.imei);
        }
        if (!Strings.isNullOrEmpty(loginRequest.username)) {
            //логиним пользователя
            loginService.loginUser(loginRequest.username, loginRequest.password);
        }

        return DataResponse.create(clientContextProvider.get());
    }


    @POST
    @Path("/v2")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse authV2(LoginRequest loginRequest) {
        if (!Strings.isNullOrEmpty(loginRequest.imei)) {
            //логиним девайс
            loginService.loginDevice(loginRequest.imei);
        }
        if (!Strings.isNullOrEmpty(loginRequest.username)) {
            //логиним пользователя
            loginService.loginUser(loginRequest.username, loginRequest.password);
        }

        return DataResponse.create(clientContextProvider.get());
    }

    @POST
    @Path("/user")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ClientContext authUser(LoginRequest loginRequest) {
        required(loginRequest.username, "username");

        loginService.loginUser(loginRequest.username, loginRequest.password);

        return clientContextProvider.get();
    }

    @POST
    @Path("/device")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ClientContext authDevice(LoginRequest loginRequest) {
        required(loginRequest.imei, "imei");

        loginService.loginDevice(loginRequest.imei);

        return clientContextProvider.get();
    }

    @POST
    @Path("/logout")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse logout(StorageRequest storageRequest) {
        loginService.logout();

        return DataResponse.create(clientContextProvider.get());
    }
}