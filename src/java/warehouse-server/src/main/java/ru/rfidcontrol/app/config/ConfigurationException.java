package ru.rfidcontrol.app.config;

import javax.annotation.Nonnull;

public class ConfigurationException extends RuntimeException {

    public ConfigurationException(@Nonnull String message) {
        super(message);
    }

    public ConfigurationException(@Nonnull String message, Throwable ex) {
        super(message, ex);
    }

}
