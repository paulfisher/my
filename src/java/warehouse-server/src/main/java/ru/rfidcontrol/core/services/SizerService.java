package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.entity.Sizer;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface SizerService {

    @NotNull Map<Integer, Sizer> getSizers();

    @NotNull Sizer getSizerByOkeiCode(int okeiCode);
    @Nullable Sizer findSizerByOkeiCode(int okeiCode);
}