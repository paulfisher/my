package ru.rfidcontrol.core.data.inventory;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.InventoryItem;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryItemInfoList extends BaseInfoList {

    @Nullable public Map<Integer, InventoryItemInfo> inventoryItems;

    public int size;

    public InventoryItemInfoList() {
        this(new ArrayList<InventoryItem>());
    }

    public InventoryItemInfoList(List<InventoryItem> inventoryItems) {
        this.inventoryItems = new HashMap<>();
        for (InventoryItem inventoryItem : inventoryItems) {
            this.inventoryItems.put(inventoryItem.getId(), new InventoryItemInfo(inventoryItem));
        }
        this.size = this.inventoryItems.size();
    }

}
