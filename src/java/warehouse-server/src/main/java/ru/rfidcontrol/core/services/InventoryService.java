package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.inventory.InventoryInfo;
import ru.rfidcontrol.core.data.inventory.InventoryItemInfo;
import ru.rfidcontrol.core.data.tag.TagProductInfo;
import ru.rfidcontrol.core.entity.Inventory;
import ru.rfidcontrol.core.entity.InventoryItem;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface InventoryService {

    @NotNull Inventory startInventory(@NotNull InventoryInfo inventoryInfo);

    @NotNull Inventory cancelInventory(@NotNull Inventory inventory);

    @NotNull Inventory finishInventory(@NotNull Inventory inventory);

    @NotNull Inventory getInventory(int inventoryId);

    @NotNull InventoryItem inventoryTagFound(Inventory inventory, TagProductInfo tagInfo, InventoryItemInfo inventoryItemInfo);

    @NotNull Map<Integer, Inventory> findInventories(@NotNull InventoryInfo inventoryInfo);

    @Nullable Inventory findInventory(int inventoryId);
}
