package ru.rfidcontrol.core.services.impl;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.tag.TagProductInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.entity.Tag;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.ServiceBase;
import ru.rfidcontrol.core.services.StorageService;
import ru.rfidcontrol.core.services.TagService;

import javax.annotation.Nullable;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagServiceImpl extends ServiceBase implements TagService {

    @Inject @NotNull StorageService storageService;

    @Inject @NotNull CompanyService companyService;

    @NotNull
    @Override
    public Map<String, Tag> findTags(@NotNull TagProductInfo tagInfo, @NotNull List<String> rfIds) {
        Map<String, Tag> result = new HashMap<>();
        if (rfIds.size() == 0) {
            return result;
        }

        TypedQuery<Tag> query;
        if (tagInfo.storageId > 0) {
            query = em().createQuery("from Tag where rfId in :rfIds and storage = :storage", Tag.class)
                    .setParameter("rfIds", rfIds)
                    .setParameter("storage", storageService.getStorage(tagInfo.storageId));
        } else if (tagInfo.companyId > 0) {
            query = em().createQuery("from Tag where rfId in :rfIds and company = :company", Tag.class)
                    .setParameter("rfIds", rfIds)
                    .setParameter("company", companyService.getCompany(tagInfo.companyId));
        } else {
            throw WarehouseException.create("specify storage id or company id");
        }
        List<Tag> tags = query.getResultList();
        for (Tag tag : tags) {
            result.put(tag.getRfId(), tag);
        }
        return result;
    }

    @NotNull
    @Override
    public Map<Integer, Tag> findTags(TagProductInfo tagInfo) {
        return findTags(tagInfo, Pagination.all());
    }

    @NotNull
    @Override
    public Map<Integer, Tag> findTags(TagProductInfo tagInfo, Pagination pagination) {
        TypedQuery<Tag> query;
        if (tagInfo.storageId > 0) {

            pagination.total = count(
                    em().createQuery("select count(id) from Tag where storage = :storage", Number.class)
                            .setParameter("storage", storageService.getStorage(tagInfo.storageId))
            );
            query = em().createQuery("from Tag where storage = :storage", Tag.class)
                    .setParameter("storage", storageService.getStorage(tagInfo.storageId));

        } else if (tagInfo.companyId > 0) {

            pagination.total = count(
                    em().createQuery("select count(id) from Tag where company = :company", Number.class)
                            .setParameter("company", companyService.getCompany(tagInfo.companyId))
            );
            query = em().createQuery("from Tag where company = :company", Tag.class)
                    .setParameter("company", companyService.getCompany(tagInfo.companyId));

        } else {
            pagination.total = count(em().createQuery("select count(id) from Tag", Number.class));
            query = em().createQuery("from Tag", Tag.class);
        }

        Map<Integer, Tag> result = new HashMap<>();
        if (!pagination.all) {
            query.setFirstResult(pagination.offset).setMaxResults(pagination.count);
        }
        for (Tag tag : query.getResultList()) {
            result.put(tag.getId(), tag);
        }

        return result;
    }

    @Nullable
    @Override
    public Tag findTag(int id) {
        return find(Tag.class, id);
    }

    @Override
    public Tag getTag(int id) {
        Tag tag = findTag(id);
        if (tag == null) {
            throw WarehouseException.notFound(Tag.class, id);
        }
        return tag;
    }

    @Nullable
    @Override
    public Tag findTag(Company company, @NotNull String rfId) {
        return first(em().createQuery("from Tag where rfId = :rfid and company = :company", Tag.class)
                .setParameter("rfid", rfId)
                .setParameter("company", company));
    }

    @NotNull
    @Override
    public Tag getTag(Company company, String rfId) {
        Tag tag = findTag(company, rfId);
        if (tag == null) {
            throw WarehouseException.notFound(Tag.class, rfId);
        }
        return tag;
    }

    @NotNull
    @Override
    @Transactional
    public Tag createTag(@NotNull TagProductInfo tagInfo) {
        Storage storage = storageService.getStorage(tagInfo.storageId);

        Tag tag = Tag.fromInfo(tagInfo)
                .setStorage(storage)
                .setCompany(storage.getCompany());
        save(tag);

        return tag;
    }

    @NotNull
    @Override
    @Transactional
    public Tag editTag(@NotNull Tag tag, @NotNull TagProductInfo tagInfo) {
        Storage storage = storageService.getStorage(tagInfo.storageId);
        return tag.setRfId(tagInfo.rfId)
                .setStorage(storage)
                .setCompany(storage.getCompany());
    }

    @NotNull
    @Override
    @Transactional
    public Tag deleteTag(@NotNull Tag tag) {
        return tag.setDeleted(true);
    }
}
