package ru.rfidcontrol.app.config.hibernate;

import ru.rfidcontrol.app.config.DbConfig;

import javax.annotation.Nonnull;
import java.util.Map;

public interface HibernateProperties {
    @Nonnull Map<String, String> constructProperties(@Nonnull DbConfig dbConfig);
}