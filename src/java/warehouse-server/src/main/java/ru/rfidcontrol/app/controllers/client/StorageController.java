package ru.rfidcontrol.app.controllers.client;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.storage.StorageRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.app.services.LoginService;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.device.DeviceInfo;
import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.data.storage.StorageInfoList;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.services.DeviceService;
import ru.rfidcontrol.core.services.StorageService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/storage")
@AccessControl
@AllowFor({AccessTypes.DEVICE})
public class StorageController extends BaseController {

    @Inject @NotNull protected StorageService storageService;
    @Inject @NotNull protected DeviceService deviceService;
    @Inject @NotNull protected LoginService loginService;

    @POST
    @Path("/find")
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse find() {
        StorageInfo storageInfo = new StorageInfo();
        storageInfo.companyId = getClientContext().getCompanyId();

        Map<Integer, Storage> findResult = storageService.findStorages(storageInfo);

        return DataResponse.create(new StorageInfoList(findResult));
    }

    @POST
    @Path("/set")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse setStorage(StorageRequest storageRequest) {
        ClientContext clientContext = getClientContext();

        Device device = deviceService.getDevice(clientContext.getDeviceId());
        Storage storage = storageService.getStorage(storageRequest.storageInfo().id);

        loginService.loginStorage(storageRequest.storageInfo());
        deviceService.setStorage(device, storage);

        return DataResponse.create(new DeviceInfo(device));
    }

}