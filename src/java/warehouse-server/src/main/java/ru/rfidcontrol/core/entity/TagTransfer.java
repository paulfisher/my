package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tag_transfer")
public class TagTransfer {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "price")
    private Double price;

    @Column(name = "vat")
    private Double vat;

    @Column(name = "quantity")
    private double quantity;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "tag_id")
    private Tag tag;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "transfer_id")
    private Transfer transfer;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "sizer_id")
    private Sizer sizer;

    public TagTransfer() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public TagTransfer setId(int id) {
        this.id = id;
        return this;
    }

    @Nullable
    public Double getPrice() {
        return price;
    }

    @NotNull
    public TagTransfer setPrice(@Nullable Double price) {
        this.price = price;
        return this;
    }

    @Nullable
    public Double getVat() {
        return vat;
    }

    @NotNull
    public TagTransfer setVat(@Nullable Double vat) {
        this.vat = vat;
        return this;
    }

    @NotNull
    public Tag getTag() {
        return tag;
    }

    @NotNull
    public TagTransfer setTag(@NotNull Tag tag) {
        this.tag = tag;
        return this;
    }

    public double getQuantity() {
        return quantity;
    }

    @NotNull
    public TagTransfer setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    @NotNull
    public Transfer getTransfer() {
        return transfer;
    }

    public TagTransfer setTransfer(@NotNull Transfer transfer) {
        this.transfer = transfer;
        return this;
    }

    @Nullable
    public Sizer getSizer() {
        return sizer;
    }

    @NotNull
    public TagTransfer setSizer(@Nullable Sizer sizer) {
        this.sizer = sizer;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .toString();
    }
}
