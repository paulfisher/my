package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.Sorting;
import ru.rfidcontrol.core.data.product.PriceInfo;
import ru.rfidcontrol.core.data.product.ProductInfo;
import ru.rfidcontrol.core.data.product.QuantityInfo;
import ru.rfidcontrol.core.data.transfer.TransferInfo;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.entity.Tag;
import ru.rfidcontrol.core.entity.TagTransfer;
import ru.rfidcontrol.core.entity.Transfer;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public interface TransferService {

    @NotNull Transfer createTransfer(@NotNull TransferInfo transferInfo);

    @NotNull List<TagTransfer> getTagTransfers(@NotNull Tag tag, @NotNull Pagination pagination);

    @NotNull Map<Integer,Transfer> findTransfers(@NotNull TransferInfo transferInfo, @NotNull Pagination pagination, @NotNull Sorting sorting);

    @NotNull Transfer getTransfer(int transferId);

    @NotNull Transfer getTransfer(@NotNull String billNumber, @NotNull Storage storage);

    @NotNull TagTransfer createTagTransfer(
            @NotNull Transfer transfer,
            @NotNull Tag tag,
            @NotNull PriceInfo priceInfo,
            @NotNull QuantityInfo quantityInfo);

    @NotNull Transfer editTransfer(@NotNull Transfer transfer, @NotNull TransferInfo transferInfo);

    @NotNull TagTransfer editTagTransfer(@NotNull TagTransfer tagTransfer, @NotNull ProductInfo productInfo);

    void deleteTagTransfer(@NotNull TagTransfer tagTransfer);

    void deleteTransfer(@NotNull Transfer transfer);
}
