package ru.rfidcontrol.core.services.impl;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.user.UserInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.ServiceBase;
import ru.rfidcontrol.core.services.UserService;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class UserServiceImpl extends ServiceBase implements UserService {

    @Inject CompanyService companyService;

    @Override
    @Transactional
    public User createUser(@NotNull UserInfo userInfo) {
        User user = User.fromInfo(userInfo)
                .setPassword(hashUserPassword(userInfo.password));
        Company company = null;
        if (userInfo.companyId > 0) {
            company = companyService.getCompany(userInfo.companyId);
            user.setCompany(company).setAdmin(false);
        }
        save(user);

        if (company != null) {
            company.getUsers().add(user);
        }

        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User editUser(@NotNull User user, @NotNull UserInfo userInfo) {
        if (userInfo.companyId > 0) {
            user.setCompany(companyService.getCompany(userInfo.companyId));
        } else {
            user.setCompany(null);
        }
        if (!Strings.isNullOrEmpty(userInfo.password)) {
            String hashPassword = hashUserPassword(userInfo.password);

            if (Strings.isNullOrEmpty(user.getPassword())
                    || !Objects.equals(hashPassword, user.getPassword())) {
                user.setPassword(hashPassword);
            }

        }
        return user.setName(userInfo.name)
                .setAdmin(userInfo.isAdmin)
                .setDeleted(userInfo.isDeleted);
    }

    @Nullable
    @Override
    public User findUser(int id) {
        return find(User.class, id);
    }

    @Nullable
    @Override
    public User findUser(String name) {
        return first(em().createQuery("from User where name = :name", User.class)
                .setParameter("name", name));
    }

    @Nullable
    @Override
    public Map<Integer, User> findUsers(UserInfo userInfo) {
        Map<Integer, User> result = new HashMap<>();
        if (!Strings.isNullOrEmpty(userInfo.name)) {
            for (User user : findUsersByName(userInfo.name)) {
                result.put(user.getId(), user);
            }
        }
        if (userInfo.companyId > 0) {
            Company company = companyService.findCompany(userInfo.companyId);
            if (company != null) {
                for (User user : company.getUsers()) {
                    result.put(user.getId(), user);
                }
            }
        } else if (result.isEmpty()) {
            for (User user : findUsers(false)) {
                result.put(user.getId(), user);
            }
        }
        return result;
    }

    @Override
    public User getUser(int id) {
        User user = findUser(id);
        if (user == null) {
            throw WarehouseException.userNotFound(id);
        }
        return user;
    }

    @Override
    @Transactional
    public User deleteUser(User user) {
        return user.setDeleted(true);
    }

    @Override
    @Transactional
    public void setDevice(User user, Device device) {
        user.getDevices().add(device);
        device.getUsers().add(user);
    }

    private List<User> findUsersByName(@NotNull String name) {
        return em().createQuery("from User where isDeleted = :isDeleted and lower(name) like :name", User.class)
                .setParameter("isDeleted", false)
                .setParameter("name", String.format("%%%s%%", name.toLowerCase()))
                .getResultList();
    }

    private List<User> findUsers(boolean isDeleted) {
        return em().createQuery("from User where isDeleted = :isDeleted", User.class)
                .setParameter("isDeleted", isDeleted)
                .getResultList();
    }
}