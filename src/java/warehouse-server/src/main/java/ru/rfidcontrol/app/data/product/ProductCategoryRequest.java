package ru.rfidcontrol.app.data.product;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;

import javax.validation.constraints.NotNull;

public class ProductCategoryRequest extends BaseRequest {

    @NotNull final public ProductCategoryInfo productCategoryInfo;

    public ProductCategoryRequest() {
        productCategoryInfo = new ProductCategoryInfo();
    }

    @NotNull
    public ProductCategoryInfo getProductCategoryInfo() {
        return productCategoryInfo;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        productCategoryInfo.companyId = clientContext.getCompanyId();
    }
}