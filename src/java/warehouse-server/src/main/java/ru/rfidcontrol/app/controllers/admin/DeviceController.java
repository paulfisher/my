package ru.rfidcontrol.app.controllers.admin;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.device.DeviceRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.device.DeviceInfoList;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.services.DeviceService;
import ru.rfidcontrol.core.services.StorageService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/admin/device")
@AccessControl
@AllowFor({AccessTypes.ADMIN})
public class DeviceController extends BaseController {

    @Inject @NotNull protected DeviceService deviceService;
    @Inject @NotNull protected StorageService storageService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findDevices(@NotNull DeviceRequest deviceRequest) {
        Map<Integer, Device> findResult = deviceService.findDevices(deviceRequest.deviceInfo());

        return DataResponse.create(new DeviceInfoList(findResult));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse getDevice(DeviceRequest deviceRequest) {
        required(deviceRequest.deviceInfo().id, "info.id");

        Device device = deviceService.getDevice(deviceRequest.deviceInfo().id);

        return DataResponse.create(device.toInfo());
    }

    @POST
    @Path("/create")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse createDevice(DeviceRequest deviceRequest) {
        Device created = deviceService.createDevice(deviceRequest.deviceInfo());

        return DataResponse.create(created.toInfo());
    }

    @POST
    @Path("/edit")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse editDevice(DeviceRequest deviceRequest) {
        required(deviceRequest.deviceInfo().id, "info.id");

        Device device = deviceService.getDevice(deviceRequest.deviceInfo().id);
        deviceService.editDevice(device, deviceRequest.deviceInfo());

        return DataResponse.create(device.toInfo());
    }

    @POST
    @Path("/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse deleteDevice(DeviceRequest deviceRequest) {
        required(deviceRequest.deviceInfo().id, "info.id");

        Device device = deviceService.getDevice(deviceRequest.deviceInfo().id);
        deviceService.deleteDevice(device);

        return DataResponse.create(device.toInfo());
    }
}