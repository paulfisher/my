package ru.rfidcontrol.core.services.impl;

import ru.rfidcontrol.core.entity.Product;
import ru.rfidcontrol.core.entity.Sizer;
import ru.rfidcontrol.core.entity.Transfer;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.ServiceBase;
import ru.rfidcontrol.core.services.SizerService;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.rfidcontrol.core.exceptions.WarehouseException.notFound;

public class SizerServiceImpl extends ServiceBase implements SizerService {

    @NotNull
    @Override
    public Map<Integer, Sizer> getSizers() {
        Map<Integer, Sizer> result = new HashMap<>();
        List<Sizer> sizers = em().createQuery("from Sizer", Sizer.class).getResultList();
        for (Sizer sizer : sizers) {
            result.put(sizer.getOkeiCode(), sizer);
        }

        return result;
    }

    @NotNull
    @Override
    public Sizer getSizerByOkeiCode(int okeiCode) {
        Sizer sizer = findSizerByOkeiCode(okeiCode);
        if (sizer == null) {
            throw notFound(Sizer.class, okeiCode);
        }
        return sizer;
    }

    @Nullable
    @Override
    public Sizer findSizerByOkeiCode(int okeiCode) {
        return first(
                em().createQuery("from Sizer where okei_code = :okei_code", Sizer.class)
                        .setParameter("okei_code", okeiCode)
        );
    }
}
