package ru.rfidcontrol.core.data.inventory;

import ru.rfidcontrol.core.data.product.ProductCategoryInfo;
import ru.rfidcontrol.core.entity.InventoryItem;

public class InventoryItemInfo {

    public int id;
    public boolean isDeleted;
    public ProductCategoryInfo expectedProductCategory;
    public ProductCategoryInfo actualProductCategory;
    public int inventoryId;
    public int storageId;
    public int status;
    public long inventDate;

    public int tagId;
    public String tagRfId;

    public InventoryItemInfo() {}

    public InventoryItemInfo(InventoryItem inventoryItem) {
        this.id = inventoryItem.getId();
        this.isDeleted = false;
        this.expectedProductCategory = ProductCategoryInfo.flat(inventoryItem.getExpectedProductCategory());
        this.actualProductCategory = null;
        if (inventoryItem.getActualProductCategory() != null) {
            this.actualProductCategory = ProductCategoryInfo.flat(inventoryItem.getActualProductCategory());
        }
        this.inventoryId = inventoryItem.getInventory().getId();
        this.storageId = inventoryItem.getInventory().getStorage().getId();
        this.status = inventoryItem.getStatus().value;
        this.inventDate = inventoryItem.getInventDate() != null ? inventoryItem.getInventDate().getTime() : 0;
        this.tagId = inventoryItem.getTag().getId();
        this.tagRfId = inventoryItem.getTag().getRfId();
    }
}
