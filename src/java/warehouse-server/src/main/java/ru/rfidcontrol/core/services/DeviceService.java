package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.device.DeviceInfo;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.Storage;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface DeviceService {

    @Nullable
    Device findDevice(int id);

    @Nullable Device findDevice(@NotNull String imeiCode);

    @NotNull Map<Integer, Device> findDevices(@NotNull DeviceInfo deviceInfo);

    @NotNull Device getDevice(int deviceId);

    @NotNull Device getDevice(@NotNull String imeiCode);

    @NotNull Device createDevice(@NotNull DeviceInfo deviceInfo);

    @NotNull Device editDevice(Device device, DeviceInfo deviceInfo);

    @NotNull Device deleteDevice(Device device);

    @NotNull Device setStorage(Device device, Storage storage);
}