package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import ru.rfidcontrol.core.data.inventory.InventoryInfo;
import ru.rfidcontrol.core.entity.type.InventoryStatus;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "inventory")
public class Inventory {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "start_date", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "finish_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;

    @Column(name = "status")
    private int status;

    @Column(name = "count_not_found")
    private int countNotFound;

    @Column(name = "count_category_conflict")
    private int countCategoryConflict;

    @Column(name = "count_match")
    private int countMatch;

    @ManyToOne
    @JoinColumn(name = "storage_id")
    private Storage storage;

    @OneToMany(mappedBy = "inventory")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<InventoryItem> inventoryItems;

    public Inventory() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Inventory setId(int id) {
        this.id = id;
        return this;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    @NotNull
    public Inventory setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    @Nullable
    public Date getFinishDate() {
        return finishDate;
    }

    @NotNull
    public Inventory setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
        return this;
    }

    @NotNull
    public Date getStartDate() {
        return startDate;
    }

    @NotNull
    public Inventory setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
        return this;
    }

    @NotNull
    public InventoryStatus getStatus() {
        return InventoryStatus.get(status);
    }

    @NotNull
    public Inventory setStatus(InventoryStatus status) {
        this.status = status.value;
        return this;
    }

    @NotNull
    public Storage getStorage() {
        return storage;
    }

    @NotNull
    public Inventory setStorage(Storage storage) {
        this.storage = storage;
        return this;
    }

    public int getCountCategoryConflict() {
        return countCategoryConflict;
    }

    @NotNull
    public Inventory setCountCategoryConflict(int countCategoryConflict) {
        this.countCategoryConflict = countCategoryConflict;
        return this;
    }

    public int getCountMatch() {
        return countMatch;
    }

    @NotNull
    public Inventory setCountMatch(int countMatch) {
        this.countMatch = countMatch;
        return this;
    }

    public int getCountNotFound() {
        return countNotFound;
    }

    @NotNull
    public Inventory setCountNotFound(int countNotFound) {
        this.countNotFound = countNotFound;
        return this;
    }

    @NotNull
    public List<InventoryItem> getInventoryItems() {
        if (inventoryItems == null) {
            inventoryItems = new ArrayList<>();
        }
        return inventoryItems;
    }

    @NotNull
    public Inventory setUsers(@NotNull List<InventoryItem> inventoryItems) {
        if (this.inventoryItems == null) {
            this.inventoryItems = new ArrayList<>();
        }
        this.inventoryItems.clear();
        this.inventoryItems.addAll(inventoryItems);
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("status", status)
                .toString();
    }

    @NotNull
    public static Inventory fromInfo(@NotNull InventoryInfo inventoryInfo) {
        return new Inventory()
                .setDeleted(inventoryInfo.isDeleted)
                .setCountCategoryConflict(inventoryInfo.countCategoryConflict)
                .setCountMatch(inventoryInfo.countMatch)
                .setCountNotFound(inventoryInfo.countNotFound)
                .setFinishDate(inventoryInfo.finishDate > 0 ? new Date(inventoryInfo.finishDate) : null)
                .setStartDate(new Date(inventoryInfo.startDate));
    }

    public InventoryInfo toInfo() {
        return new InventoryInfo(this);
    }
}
