package ru.rfidcontrol.app.controllers.client;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.product.ProductCategoryQuantityRequest;
import ru.rfidcontrol.app.data.product.ProductCategoryRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;
import ru.rfidcontrol.core.data.product.ProductCategoryInfoList;
import ru.rfidcontrol.core.data.product.ProductCategoryQuantityInfo;
import ru.rfidcontrol.core.data.product.ProductCategoryQuantityInfoList;
import ru.rfidcontrol.core.entity.ProductCategory;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.services.ProductCategoryService;
import ru.rfidcontrol.core.services.QuantityService;
import ru.rfidcontrol.core.services.StorageService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

@Path("/product/category")
@AccessControl
@AllowFor({AccessTypes.DEVICE})
public class ProductCategoryController extends BaseController {

    @Inject @NotNull protected ProductCategoryService productCategoryService;
    @Inject @NotNull protected QuantityService quantityService;
    @Inject @NotNull protected StorageService storageService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findProductCategories(@NotNull ProductCategoryRequest findProductCategoryRequest) {
        populateFromContext(findProductCategoryRequest);

        Map<Integer, ProductCategory> products = productCategoryService.findProductCategories(findProductCategoryRequest.getProductCategoryInfo(), findProductCategoryRequest.pagination);

        return DataResponse.create(new ProductCategoryInfoList(products).setPagination(findProductCategoryRequest.pagination));
    }


    @POST
    @Path("/quantity")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse countProductCategories(@NotNull ProductCategoryQuantityRequest productCategoryQuantityRequest) {
        populateFromContext(productCategoryQuantityRequest);

        required(productCategoryQuantityRequest.productCategoryInfo, "productCategoryQuantityRequest.productCategoryInfo");

        ProductCategoryInfo productCategoryInfo = productCategoryQuantityRequest.productCategoryInfo;
        Map<Integer, ProductCategoryQuantityInfo> quantities = new HashMap<>();

        Storage storage = storageService.getStorage(getClientContext().getStorageId());

        if (productCategoryInfo.id > 0) {
            ProductCategory productCategory = productCategoryService.getProductCategory(productCategoryInfo.id);
            quantities.put(productCategoryInfo.id, quantityService.calculateProductCategoryQuantityInfo(storage, productCategory));
        } else {
            for (ProductCategoryQuantityInfo productCategoryQuantityInfo : quantityService.calculateProductCategoryQuantityInfo(storage)) {
                quantities.put(productCategoryQuantityInfo.productQuantityInfo.categoryId, productCategoryQuantityInfo);
            }
        }

        return DataResponse.create(new ProductCategoryQuantityInfoList(quantities));
    }

}