package ru.rfidcontrol.core.data.product;

public class PriceInfo {

    public double price;
    public double vat;

    public PriceInfo() {}

    public PriceInfo(double price, double vat) {
        this.price = price;
        this.vat = vat;
    }
}
