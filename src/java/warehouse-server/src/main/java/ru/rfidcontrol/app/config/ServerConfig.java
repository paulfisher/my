package ru.rfidcontrol.app.config;

import com.google.common.base.MoreObjects;
import com.owlike.genson.Genson;
import com.owlike.genson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Reader;

public class ServerConfig {

    @Nullable
    @JsonProperty(value = "env")
    private Environment environment;

    @Nullable
    @JsonProperty(value = "db")
    private DbConfig dbConfig;

    @Nullable
    @JsonProperty(value = "migrationPath")
    public String migrationPath;

    @Nullable
    public Environment getEnvironment() {
        return environment;
    }

    @Nullable
    public DbConfig getDbConfig() {
        return dbConfig;
    }

    @Nullable
    public String getMigrationPath() {
        return migrationPath;
    }

    public static <T> T load(@Nonnull Reader reader, Class<T> type) {
        return new Genson().deserialize(reader, type);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("environment", environment)
                .add("dbConfig", dbConfig)
                .add("migrationPath", migrationPath)
                .toString();
    }

}
