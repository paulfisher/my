package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import ru.rfidcontrol.core.data.storage.StorageInfo;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "storage")
public class Storage {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "create_time", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "reference")
    private String reference;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @OneToMany(mappedBy = "storage")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Device> devices;

    public Storage() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Storage setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    @NotNull
    public Storage setName(String name) {
        this.name = name;
        return this;
    }

    @NotNull
    public Date getCreateTime() {
        return createTime;
    }

    @NotNull
    public Storage setCreateTime(@NotNull Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    @NotNull
    public Storage setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    @Nullable
    public String getReference() {
        return reference;
    }

    @NotNull
    public Storage setReference(@NotNull String reference) {
        this.reference = reference;
        return this;
    }

    @NotNull
    public Company getCompany() {
        return company;
    }

    public Storage setCompany(@NotNull Company company) {
        this.company = company;
        return this;
    }

    @NotNull
    public List<Device> getDevices() {
        if (devices == null) {
            devices = new ArrayList<>();
        }
        return devices;
    }

    @NotNull
    public Storage setDevices(@NotNull List<Device> devices) {
        if (this.devices == null) {
            this.devices = new ArrayList<>(devices);
        }
        this.devices.clear();
        this.devices.addAll(devices);
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("companyId", getCompany() != null ? getCompany().getId() : "-")
                .toString();
    }

    public StorageInfo toInfo() {
        return new StorageInfo(this);
    }

    public static Storage fromInfo(StorageInfo storageInfo) {
        return new Storage()
                .setName(storageInfo.name)
                .setDeleted(storageInfo.isDeleted);
    }
}
