package ru.rfidcontrol.app.controllers.admin;

import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.company.CompanyRequest;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.company.CompanyInfoList;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.services.CompanyService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/admin/company")
@AccessControl
@AllowFor({AccessTypes.ADMIN})
public class CompanyController extends BaseController {

    @Inject @NotNull protected CompanyService companyService;

    @POST
    @Path("/find")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findCompanies(@NotNull CompanyRequest companyRequest) {
        Map<Integer, Company> findResult = companyService.findCompanies(companyRequest.companyInfo());

        return DataResponse.create(new CompanyInfoList(findResult));
    }

    @POST
    @Path("/get")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse findCompany(@NotNull CompanyRequest companyRequest) {
        required(companyRequest.companyInfo().id, "info.id");

        Company company = companyService.getCompany(companyRequest.companyInfo().id);

        return DataResponse.create(company.toInfo());
    }

    @POST
    @Path("/create")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse createCompany(@NotNull CompanyRequest companyRequest) {
        Company created = companyService.createCompany(companyRequest.companyInfo());

        return DataResponse.create(created.toInfo());
    }

    @POST
    @Path("/edit")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse editCompany(@NotNull CompanyRequest companyRequest) {
        required(companyRequest.companyInfo().id, "info.id");

        Company company = companyService.getCompany(companyRequest.companyInfo().id);
        companyService.editCompany(company, companyRequest.companyInfo());

        return DataResponse.create(company.toInfo());
    }

    @POST
    @Path("/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse deleteCompany(@NotNull CompanyRequest companyRequest) {
        required(companyRequest.companyInfo().id, "info.id");

        Company company = companyService.getCompany(companyRequest.companyInfo().id);
        companyService.deleteCompany(company);

        return DataResponse.create(company.toInfo());
    }
}