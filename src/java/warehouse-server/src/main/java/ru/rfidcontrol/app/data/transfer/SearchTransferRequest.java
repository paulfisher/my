package ru.rfidcontrol.app.data.transfer;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.transfer.TransferInfo;

import javax.validation.constraints.NotNull;

public class SearchTransferRequest extends BaseRequest {

    @NotNull public TransferInfo transferInfo;

    public SearchTransferRequest() {
        transferInfo = new TransferInfo();
    }

    @NotNull
    public TransferInfo getTransferInfo() {
        return transferInfo;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        transferInfo.companyId = clientContext.getCompanyId();
    }
}
