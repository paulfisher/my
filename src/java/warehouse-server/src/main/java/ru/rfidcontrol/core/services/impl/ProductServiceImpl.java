package ru.rfidcontrol.core.services.impl;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.product.ProductInfo;
import ru.rfidcontrol.core.data.tag.TagProductInfo;
import ru.rfidcontrol.core.entity.*;
import ru.rfidcontrol.core.exceptions.WarehouseException;
import ru.rfidcontrol.core.services.*;

import javax.annotation.Nullable;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductServiceImpl extends ServiceBase implements ProductService {

    @Inject ProductCategoryService productCategoryService;
    @Inject TagService tagService;
    @Inject UserService userService;
    @Inject SizerService sizerService;

    @Override
    public Map<Integer, Product> findProducts(ProductInfo productInfo) {
        Map<Integer, Product> result = new HashMap<>();
        List<Product> products = em().createQuery("from Product", Product.class).getResultList();
        for (Product product : products) {
            result.put(product.getId(), product);
        }

        return result;
    }

    @Override
    public Map<Integer, Product> findProducts(Storage storage, ProductCategory productCategory, Pagination pagination) {
        Map<Integer, Product> result = new HashMap<>();
        TypedQuery<Product> products = em()
                .createQuery("from Product where productCategory = :productCategory and tag.storage = :storage", Product.class)
                .setParameter("storage", storage)
                .setParameter("productCategory", productCategory);

        if (!pagination.all) {
            products.setFirstResult(pagination.offset)
                    .setMaxResults(pagination.count);
        }
        for (Product product : products.getResultList()) {
            result.put(product.getId(), product);
        }

        return result;
    }

    @NotNull
    @Override
    public Map<Integer, Product> findProducts(Storage storage, ProductCategory productCategory) {
        return findProducts(storage, productCategory, Pagination.all());
    }

    @Override
    public int countProducts(Storage storage, ProductCategory productCategory) {
        return count(
            em()
                .createQuery("select count(id) from Product where productCategory = :productCategory and tag.storage = :storage", Number.class)
                .setParameter("productCategory", productCategory)
                .setParameter("storage", storage)
        );
    }

    @Nullable
    @Override
    public Product findProduct(int id) {
        return find(Product.class, id);
    }

    @Override
    public Product getProduct(int id) {
        Product product = findProduct(id);
        if (product == null) {
            throw WarehouseException.notFound(Product.class, id);
        }
        return product;
    }

    @Override
    @Transactional
    public Product createProduct(@NotNull Storage storage,
                                 @NotNull Company company,
                                 @NotNull ProductInfo productInfo) {
        if (productInfo.category == null) {
            throw WarehouseException.create("product category required");
        }

        //get or create category
        ProductCategory category;
        if (productInfo.category.id > 0) {
            category = productCategoryService.getProductCategory(productInfo.category.id);
        } else {
            category = productCategoryService.createProductCategory(productInfo.category);
        }

        //get or create tag
        Tag tag;
        if (productInfo.tagId > 0) {
            tag = tagService.getTag(productInfo.tagId);
        } else if (!Strings.isNullOrEmpty(productInfo.rfId)) {
            tag = tagService.findTag(company, productInfo.rfId);
            if (tag == null) {
                TagProductInfo tagToCreate = new TagProductInfo();
                tagToCreate.rfId = productInfo.rfId;
                tagToCreate.storageId = storage.getId();
                tagToCreate.isDeleted = false;
                tag = tagService.createTag(tagToCreate);
                em().refresh(tag);
            }
        } else {
            throw WarehouseException.create("tag rfId or id is required");
        }

        //checks
        if (category.getCompany() != company) {
            throw WarehouseException.create("category company conflict");
        }
        if (category.getCompany() != tag.getCompany()) {
            throw WarehouseException.create("tag and category have different company binding");
        }

        //product creating
        Product product = Product.fromInfo(productInfo)
                .setProductCategory(category)
                .setTag(tag);

        if (productInfo.responsibleUserId > 0) {
            product.setResponsibleUser(userService.getUser(productInfo.responsibleUserId));
        }

        if (productInfo.quantityInfo != null && productInfo.quantityInfo.sizer != null && productInfo.quantityInfo.sizer.okeiCode > 0) {
            product.setSizer(sizerService.getSizerByOkeiCode(productInfo.quantityInfo.sizer.okeiCode));
        }

        save(product);

        return product;
    }

    @Override
    @Transactional
    public Product editProduct(@NotNull Product product, @NotNull ProductInfo productInfo) {
        if (productInfo.category == null) {
            throw WarehouseException.create("product category required");
        }
        ProductCategory category = productCategoryService.getProductCategory(productInfo.category.id);
        Tag tag = tagService.getTag(productInfo.tagId);

        product.setProductCategory(category)
                .setTag(tag);

        return product;
    }

    @Override
    @Transactional
    public Product deleteProduct(@NotNull Product product) {
        return product.setDeleted(true);
    }
}
