package ru.rfidcontrol.app.data.transfer;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.product.ProductInfo;
import ru.rfidcontrol.core.data.transfer.TransferInfo;

import javax.validation.constraints.NotNull;

public class ProductTransferRequest extends BaseRequest {

    @NotNull public TransferInfo transferInfo;
    @NotNull public ProductInfo productInfo;

    public ProductTransferRequest() {
        transferInfo = new TransferInfo();
        productInfo = new ProductInfo();
    }

    @NotNull
    public TransferInfo getTransferInfo() {
        return transferInfo;
    }

    @NotNull
    public ProductInfo getProductInfo() {
        return productInfo;
    }

    @Override
    public void populateFromContext(@NotNull ClientContext clientContext) {
        if (clientContext.getStorageId() > 0) {
            transferInfo.storageId = clientContext.getStorageId();
        }
        transferInfo.userId = clientContext.getUserId();
        transferInfo.companyId = clientContext.getCompanyId();
        transferInfo.deviceId = clientContext.getDeviceId();

        productInfo.category.companyId = clientContext.getCompanyId();
    }
}
