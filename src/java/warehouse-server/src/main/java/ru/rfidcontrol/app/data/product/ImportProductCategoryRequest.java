package ru.rfidcontrol.app.data.product;

import ru.rfidcontrol.app.data.BaseRequest;
import ru.rfidcontrol.core.data.product.ProductCategoryInfo;

import javax.annotation.Nullable;

public class ImportProductCategoryRequest extends BaseRequest {

    @Nullable public String companyRef;
    @Nullable public ProductCategoryInfo productCategoryInfo;
}