package ru.rfidcontrol.core.data.product;

import ru.rfidcontrol.core.entity.Product;
import ru.rfidcontrol.core.entity.Sizer;
import ru.rfidcontrol.core.entity.User;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class ProductInfo {

    public int id;
    public boolean isDeleted;
    public ProductCategoryInfo category;

    public int tagId;
    public String rfId;

    public int responsibleUserId;
    public String responsibleUserName;

    @Nullable public QuantityInfo quantityInfo;
    @Nullable public PriceInfo priceInfo;

    public ProductInfo() {}

    public ProductInfo(@NotNull Product product) {
        this.id = product.getId();
        this.isDeleted = product.isDeleted();
        this.category = new ProductCategoryInfo(product.getProductCategory());

        this.tagId = product.getTag().getId();
        this.rfId = product.getTag().getRfId();

        User responsibleUser = product.getResponsibleUser();
        if (responsibleUser != null) {
            this.responsibleUserId = responsibleUser.getId();
            this.responsibleUserName = responsibleUser.getName();
        }

        Sizer sizer = product.getSizer();
        if (sizer != null) {
            this.quantityInfo = new QuantityInfo(new SizerInfo(sizer), product.getQuantity());
        }
        this.priceInfo = new PriceInfo();
        this.priceInfo.price = product.getProductCategory().getPrice() == null ? 0 : product.getProductCategory().getPrice();
        this.priceInfo.vat = product.getProductCategory().getVat() == null ? 0 : product.getProductCategory().getVat();
    }
}
