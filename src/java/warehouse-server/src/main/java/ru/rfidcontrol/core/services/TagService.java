package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.data.Pagination;
import ru.rfidcontrol.core.data.tag.TagProductInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Tag;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public interface TagService {

    @NotNull Map<String, Tag> findTags(@NotNull TagProductInfo tagInfo, @NotNull List<String> rfIds);

    @NotNull Map<Integer, Tag> findTags(@NotNull TagProductInfo tagInfo, @NotNull Pagination pagination);

    @NotNull Map<Integer, Tag> findTags(@NotNull TagProductInfo tagInfo);

    @Nullable Tag findTag(int id);

    @NotNull Tag getTag(int id);

    @Nullable Tag findTag(Company company, @NotNull String rfId);

    @NotNull Tag getTag(Company company, @NotNull String rfId);

    @NotNull Tag createTag(@NotNull TagProductInfo tagInfo);

    @NotNull Tag editTag(@NotNull Tag tag, @NotNull TagProductInfo tagInfo);

    @NotNull Tag deleteTag(@NotNull Tag tag);
}