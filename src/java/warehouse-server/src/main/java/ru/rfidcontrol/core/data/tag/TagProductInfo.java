package ru.rfidcontrol.core.data.tag;

import ru.rfidcontrol.core.data.BaseInfo;
import ru.rfidcontrol.core.data.product.ProductInfo;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.entity.Tag;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class TagProductInfo extends BaseInfo {

    public int id;
    public String rfId;
    public Date createTime;
    public boolean isDeleted;

    public int storageId = 0;
    public int companyId = 0;
    public String storageName = "";

    public ProductInfo productInfo;

    public TagProductInfo() {}

    public TagProductInfo(@NotNull Tag tag) {
        this.id = tag.getId();
        this.rfId = tag.getRfId();
        this.createTime = tag.getCreateTime();
        this.isDeleted = tag.isDeleted();

        Storage storage = tag.getStorage();
        if (storage != null) {
            this.storageId = storage.getId();
            this.storageName = storage.getName();
            this.companyId = storage.getCompany().getId();
        }

        if (tag.getProduct() != null) {
            this.productInfo = new ProductInfo(tag.getProduct());
        }
    }
}