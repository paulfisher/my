package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import org.hibernate.annotations.*;
import ru.rfidcontrol.core.data.device.DeviceInfo;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "device")
public class Device {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "imei_code")
    private String imeiCode;

    @Column(name = "create_time", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "last_login_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginTime;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "storage_id")
    private Storage storage;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_device",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "device_id") })
    private List<User> users;


    @OneToMany(mappedBy = "device")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private List<Transfer> transfers;

    public Device() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public Device setId(int id) {
        this.id = id;
        return this;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public Device setName(String name) {
        this.name = name;
        return this;
    }

    @NotNull
    public String getImeiCode()
    {
        return imeiCode;
    }

    @NotNull
    public Device setImeiCode(String imeiCode) {
        this.imeiCode = imeiCode;
        return this;
    }

    @NotNull
    public Date getCreateTime() {
        return createTime;
    }

    @NotNull
    public Device setCreateTime(@NotNull Date createTime) {
        this.createTime = createTime;
        return this;
    }

    @Nullable
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    @NotNull
    public Device setLastLoginTime(@NotNull Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        return this;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    @NotNull
    public Device setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    @Nullable
    public Storage getStorage() {
        return storage;
    }

    @NotNull
    public Device setStorage(@Nullable Storage storage) {
        this.storage = storage;
        return this;
    }

    @NotNull
    public Company getCompany() {
        return company;
    }

    @NotNull
    public Device setCompany(@NotNull Company company) {
        this.company = company;
        return this;
    }

    @NotNull
    public List<User> getUsers() {
        if (users == null) {
            users = new ArrayList<>();
        }
        return users;
    }

    @NotNull
    public Device setUsers(@NotNull List<User> users) {
        if (this.users == null) {
            this.users = new ArrayList<>();
        }
        this.users.clear();
        this.users.addAll(users);
        return this;
    }

    @NotNull
    public List<Transfer> getTransfers() {
        if (transfers == null) {
            transfers = new ArrayList<>();
        }
        return transfers;
    }

    @NotNull
    public Device setTransfers(@NotNull List<Transfer> transfers) {
        if (this.transfers == null) {
            this.transfers = new ArrayList<>();
        }
        this.transfers.clear();
        this.transfers.addAll(transfers);
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("imei_code", imeiCode)
                .toString();
    }

    public static Device fromInfo(DeviceInfo deviceInfo) {
        return new Device()
                .setName(deviceInfo.name)
                .setImeiCode(deviceInfo.imeiCode);
    }

    public DeviceInfo toInfo() {
        return new DeviceInfo(this);
    }
}
