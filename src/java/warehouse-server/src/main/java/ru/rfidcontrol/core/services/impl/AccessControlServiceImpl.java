package ru.rfidcontrol.core.services.impl;

import ru.rfidcontrol.core.entity.type.ExternalApp;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.services.AccessControlService;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AccessControlServiceImpl implements AccessControlService {

    @Override
    public boolean hasAccess(@NotNull ClientContext context, @NotNull Class<?> controller, @NotNull Method action) {
        List<String> accessTypes = new ArrayList<>();
        if (controller.isAnnotationPresent(AllowFor.class)) {
            accessTypes.addAll(Arrays.asList(controller.getAnnotation(AllowFor.class).value()));
        }
        if (action.isAnnotationPresent(AllowFor.class)) {
            accessTypes.addAll(Arrays.asList(action.getAnnotation(AllowFor.class).value()));
        }
        if (accessTypes.isEmpty()) {
            return true;
        }
        for (String accessType : accessTypes) {
            if (hasAccess(context, accessType)) {
                return true;
            }
        }

        return false;
    }

    private boolean hasAccess(@NotNull ClientContext context, @NotNull String accessType) {
        switch (accessType) {
            case AccessTypes.ADMIN: return context.isAdmin();
            case AccessTypes.DEVICE: return context.getDeviceId() > 0;
            case AccessTypes.MANAGER: return context.getUserId() > 0 && !context.isAdmin();
            case AccessTypes.AUTHORIZED: return context.getUserId() > 0;
            case AccessTypes.C1: return context.getExternalApp() == ExternalApp.C1;
        }
        return false;
    }

}
