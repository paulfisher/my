package ru.rfidcontrol.core.data;

public class Pagination {

    public int count = 10;
    public int offset = 0;
    public int total = 0;
    public boolean all = false;

    public static Pagination all() {
        Pagination pagination = new Pagination();
        pagination.all = true;

        return pagination;
    }

}