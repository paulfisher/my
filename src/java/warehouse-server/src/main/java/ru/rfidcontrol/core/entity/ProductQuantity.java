package ru.rfidcontrol.core.entity;

import com.google.common.base.MoreObjects;
import ru.rfidcontrol.core.data.product.ProductQuantityInfo;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product_quantity")
public class ProductQuantity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "quantity")
    private double quantity;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "product_category_id")
    private ProductCategory productCategory;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "storage_id")
    private Storage storage;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    public ProductQuantity() {
    }

    public int getId() {
        return id;
    }

    @NotNull
    public ProductQuantity setId(int id) {
        this.id = id;
        return this;
    }

    public double getQuantity() {
        return quantity;
    }

    @NotNull
    public ProductQuantity setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    @NotNull
    public ProductCategory getProductCategory() {
        return productCategory;
    }

    @NotNull
    public ProductQuantity setProductCategory(@NotNull ProductCategory productCategory) {
        this.productCategory = productCategory;
        return this;
    }

    @Nullable
    public Storage getStorage() {
        return storage;
    }

    @NotNull
    public ProductQuantity setStorage(@Nullable Storage storage) {
        this.storage = storage;
        return this;
    }

    @NotNull
    public Company getCompany() {
        return company;
    }

    @NotNull
    public ProductQuantity setCompany(@NotNull Company company) {
        this.company = company;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .toString();
    }

    @NotNull
    public static ProductQuantity fromInfo(@NotNull ProductQuantityInfo productQuantityInfo) {
        return new ProductQuantity()
                .setQuantity(productQuantityInfo.quantity);
    }
}
