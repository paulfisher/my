package ru.rfidcontrol.core.data.inventory;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.Inventory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryInfoList extends BaseInfoList {

    @Nullable public Map<Integer, InventoryInfo> inventories;

    public int size;

    public InventoryInfoList() {
        this(new ArrayList<Inventory>());
    }

    public InventoryInfoList(List<Inventory> inventories) {
        this.inventories = new HashMap<>();
        for (Inventory inventory : inventories) {
            this.inventories.put(inventory.getId(), inventory.toInfo());
        }
        this.size = this.inventories.size();
    }

    public InventoryInfoList(Map<Integer, Inventory> inventories) {
        this.inventories = new HashMap<>();
        for (Map.Entry<Integer, Inventory> integerInventoryEntry : inventories.entrySet()) {
            this.inventories.put(integerInventoryEntry.getKey(), integerInventoryEntry.getValue().toInfo());
        }
        this.size = this.inventories.size();
    }
}
