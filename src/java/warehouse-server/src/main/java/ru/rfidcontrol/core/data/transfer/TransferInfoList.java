package ru.rfidcontrol.core.data.transfer;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.Transfer;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class TransferInfoList extends BaseInfoList {

    @Nullable public Map<Integer, TransferInfo> transfers;

    public int size;

    public TransferInfoList() {
    }

    public TransferInfoList(Map<Integer, Transfer> transfers) {
        this.transfers = new HashMap<>();
        for (Map.Entry<Integer, Transfer> integerTransferEntry : transfers.entrySet()) {
            this.transfers.put(integerTransferEntry.getKey(), integerTransferEntry.getValue().toInfo());
        }
        this.size = this.transfers.size();
    }
}
