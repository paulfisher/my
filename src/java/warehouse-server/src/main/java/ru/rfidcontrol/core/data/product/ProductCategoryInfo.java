package ru.rfidcontrol.core.data.product;

import ru.rfidcontrol.core.entity.ProductCategory;

public class ProductCategoryInfo {

    public int id;
    public String name;
    public int parentCategoryId;
    public String parentCategoryName;
    public ProductCategoryInfo parent;
    public SizerInfo sizer;

    public String reference;

    public boolean isDeleted;

    public int childrenSize;
    public boolean isTopLevel;

    public int companyId;

    public Double price;
    public Double vat;
    public String article;

    public ProductCategoryInfo(ProductCategory category) {
        this.id = category.getId();
        this.name = category.getName();
        this.companyId = category.getCompany().getId();
        if (category.getParentProductCategory() != null) {
            this.parentCategoryId = category.getParentProductCategory().getId();
            this.parentCategoryName = category.getParentProductCategory().getName();
            this.parent = new ProductCategoryInfo(category.getParentProductCategory());
        }
        this.sizer = category.getSizer() != null ? category.getSizer().toInfo() : null;
        this.childrenSize = category.getProductCategories().size();
        this.isDeleted = category.isDeleted();
        this.reference = category.getReference();
        this.isTopLevel = category.getParentProductCategory() == null;
        this.article = category.getArticle();
    }

    public ProductCategoryInfo() {
        this.name = "";
        this.article = "";
        this.reference = "";

    }

    public static ProductCategoryInfo flat(ProductCategory category) {
        ProductCategoryInfo full = new ProductCategoryInfo(category);
        full.parent = null;
        return full;
    }
}
