package ru.rfidcontrol.app.config;

import javax.annotation.Nonnull;

public enum Environment {
    LOCAL("local"),
    TEST("test"),
    DEV("dev"),
    PROD("prod");

    public final String value;

    Environment(@Nonnull String value) {
        this.value = value;
    }
}
