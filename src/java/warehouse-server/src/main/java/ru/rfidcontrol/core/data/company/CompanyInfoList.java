package ru.rfidcontrol.core.data.company;

import ru.rfidcontrol.core.data.BaseInfoList;
import ru.rfidcontrol.core.entity.Company;

import java.util.HashMap;
import java.util.Map;

public class CompanyInfoList extends BaseInfoList {

    public final Map<Integer, CompanyInfo> companies;
    public final int size;

    public CompanyInfoList(Map<Integer, Company> companies) {
        this.companies = new HashMap<>();
        for (Map.Entry<Integer, Company> companyEntry : companies.entrySet()) {
            this.companies.put(companyEntry.getKey(), new CompanyInfo(companyEntry.getValue()));
        }
        size = this.companies.size();
    }


}