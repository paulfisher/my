package ru.rfidcontrol.app.controllers;

import ru.rfidcontrol.core.exceptions.WarehouseException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/err")
public class ErrorController extends BaseController {

    @GET
    @Path("/test")
    @Produces({MediaType.APPLICATION_JSON})
    public Response testSimpleMessageError() {
        throw WarehouseException.create("test error message");
    }
}