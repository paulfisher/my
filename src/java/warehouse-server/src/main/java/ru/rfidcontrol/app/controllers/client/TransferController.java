package ru.rfidcontrol.app.controllers.client;

import com.google.common.base.Strings;
import ru.rfidcontrol.app.controllers.BaseController;
import ru.rfidcontrol.app.data.response.DataResponse;
import ru.rfidcontrol.app.data.tag.TagRequest;
import ru.rfidcontrol.app.data.transfer.ProductTransferRequest;
import ru.rfidcontrol.app.filters.annotations.AccessControl;
import ru.rfidcontrol.core.annotations.AccessTypes;
import ru.rfidcontrol.core.annotations.AllowFor;
import ru.rfidcontrol.core.data.transfer.TagTransferInfoList;
import ru.rfidcontrol.core.data.transfer.TransferInfo;
import ru.rfidcontrol.core.entity.Product;
import ru.rfidcontrol.core.entity.Tag;
import ru.rfidcontrol.core.entity.TagTransfer;
import ru.rfidcontrol.core.entity.Transfer;
import ru.rfidcontrol.core.services.*;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/transfer")
@AccessControl
@AllowFor({AccessTypes.DEVICE})
public class TransferController extends BaseController {

    @Inject @NotNull protected TransferService transferService;
    @Inject @NotNull protected TagService tagService;
    @Inject @NotNull protected ProductService productService;
    @Inject @NotNull protected CompanyService companyService;
    @Inject @NotNull protected StorageService storageService;

//    @POST
//    @Path("/register")
//    @Consumes({MediaType.APPLICATION_JSON})
//    @Produces({MediaType.APPLICATION_JSON})
//    public DataResponse register(@NotNull TransferRequest transferRequest) {
//
//        populateFromContext(transferRequest);
//
//        required(transferRequest.getTransferInfo().storageId, "transferInfo.storageId");
//
//        Transfer transfer = transferService.createTransfer(transferRequest.getTransferInfo());
//
//        return DataResponse.create(transfer.toInfo());
//    }

    @POST
    @Path("/product/register")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse productRegister(@NotNull ProductTransferRequest productTransferRequest) {

        populateFromContext(productTransferRequest);

        Product product = productService.findProduct(productTransferRequest.getProductInfo().id);
        if (product == null) {
            product = productService.createProduct(
                    storageService.getStorage(productTransferRequest.getTransferInfo().storageId),
                    companyService.getCompany(getClientContext().getCompanyId()),
                    productTransferRequest.getProductInfo());
        } else {
            product = productService.editProduct(product, productTransferRequest.getProductInfo());
        }

        Transfer transfer = transferService.createTransfer(productTransferRequest.getTransferInfo());
        transferService.createTagTransfer(
                transfer,
                product.getTag(),
                productTransferRequest.getProductInfo().priceInfo,
                productTransferRequest.getProductInfo().quantityInfo
        );

        //todo: ��� ��� ��������������. �����, ����� ���� ����������� ��������� ���. billNumber, ����������, ����
        //
        // ���� ��������� billNumber: �� ��������� ��������� ������ �������. ���� � ��� �� �������� ������ tagTransfer, �� ������?
        // ������ ����� transfer � ����� billNumber, � ����� tagTransfer.
        // ���� ���������� ��������� ������: ������ ����� ��������� ������, ������� �� ������
        // ���� ���������� ����: ����� ������ ����
        // ���� ��������� vat: ����� vat
        // ����� ����� responsibleUser � Product
        // ����� ����� supplier � Transfer

        return DataResponse.create(transfer.toInfo());
    }

    @POST
    @Path("/product/update")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse productUpdate(@NotNull ProductTransferRequest productTransferRequest) {
        /*
        Transfer
        TagTransfer[]
        Tag
        Product
        * */

        populateFromContext(productTransferRequest);
        required(productTransferRequest.getProductInfo().id, "productTransferRequest.getProductInfo().id");

        TransferInfo transferInfo = productTransferRequest.getTransferInfo();
        required(transferInfo.id, "transferInfo.id");

        Product product = productService.editProduct(
                productService.getProduct(productTransferRequest.getProductInfo().id),
                productTransferRequest.getProductInfo());



        Transfer transfer = transferService.getTransfer(transferInfo.id);
        TagTransfer lastTagTransfer =  null;
        for (TagTransfer tagTransfer : transfer.getTagTransfers()) {
            if (tagTransfer.getTag().getId() == productTransferRequest.getProductInfo().tagId) {
                lastTagTransfer = tagTransfer;
                break;
            }
        }

        if (!Strings.isNullOrEmpty(transfer.getBillNumber()) && !transfer.getBillNumber().equals(transferInfo.billNumber)) {

            //����� bill number - ����� transfer
            //1. ������� �������� ����� �� billNumber, �������� ����� transfer ��� ���������
            transfer = transferService.getTransfer(transferInfo.billNumber, storageService.getStorage(transferInfo.storageId));
            if (transfer == null) {
                //2. �� ����� �� billNumber -> ������� ����� transfer
                transfer = transferService.createTransfer(transferInfo);
            }
            if (lastTagTransfer != null) {
                Transfer oldBillNumberTransfer = lastTagTransfer.getTransfer();
                transferService.deleteTagTransfer(lastTagTransfer);
                if (oldBillNumberTransfer.getTagTransfers().isEmpty()) {
                    transferService.deleteTransfer(oldBillNumberTransfer);
                }
            }

            transferService.createTagTransfer(
                    transfer,
                    product.getTag(),
                    productTransferRequest.getProductInfo().priceInfo,
                    productTransferRequest.getProductInfo().quantityInfo
            );
        } else {
            transferService.editTransfer(transfer, transferInfo);

            if (lastTagTransfer != null) {
                transferService.editTagTransfer(lastTagTransfer, productTransferRequest.getProductInfo());
            }
        }

        //��� ��� ��������������. �����, ����� ���� ����������� ��������� ���. billNumber, ����������, ����
        //
        // ���� ��������� billNumber: �� ��������� ��������� ������ �������. ���� � ��� �� �������� ������ tagTransfer, �� ������?
        // ������ ����� transfer � ����� billNumber, � ����� tagTransfer.
        // ���� ���������� ��������� ������: ������ ����� ��������� ������, ������� �� ������
        // ���� ���������� ����: ����� ������ ����
        // ���� ��������� vat: ����� vat
        // ����� ����� responsibleUser � Product
        // ����� ����� supplier � Transfer

        return DataResponse.create(transfer.toInfo());
    }

    @POST
    @Path("/history")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataResponse history(@NotNull TagRequest tagRequest) {

        populateFromContext(tagRequest);

        required(tagRequest.getTagInfo().id, "tagInfo.id");

        Tag tag = tagService.getTag(tagRequest.getTagInfo().id);

        List<TagTransfer> tagTransferList = transferService.getTagTransfers(tag, tagRequest.pagination);

        return DataResponse.create(new TagTransferInfoList(tagTransferList).setPagination(tagRequest.pagination));
    }
}