package ru.rfidcontrol.core.services;

import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Supplier;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface SupplierService {

    @NotNull Map<Integer, Supplier> getSuppliers(Company company);
    @NotNull Supplier getSupplier(int id);
    @Nullable Supplier findSupplier(int id);
}
