package ru.rfidcontrol.guice;

import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.services.AccessControlService;

import java.lang.reflect.Method;

public class MockAccessControlServiceImpl implements AccessControlService {
    @Override
    public boolean hasAccess(ClientContext context, Class<?> controller, Method action) {
        return true;
    }
}
