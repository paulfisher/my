package ru.rfidcontrol;

import org.junit.Ignore;
import org.junit.Test;
import ru.rfidcontrol.core.data.company.CompanyInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.UserService;

import javax.inject.Inject;

import static org.junit.Assert.*;

@Ignore
public class CompanyTest extends BaseTest {

    @Inject
    CompanyService companyService;
    @Inject
    UserService userService;

    @Test
    public void testCrudCompany() {
        Company company;
        CompanyInfo info = new CompanyInfo();
        info.name = "testCrudCompany";
        info.email = "testCrudCompany@test.com";
        company = companyService.createCompany(info);

        assertNotNull(company);

        int companyId = company.getId();
        assertEquals(company.getName(), "testCrudCompany");
        assertTrue(companyId > 0);

        Company companyToDelete = companyService.findCompany(companyId);
        companyService.deleteCompany(companyToDelete);

        Company deleted = companyService.findCompany(companyId);
        assertNotNull(deleted);
        assertEquals(deleted.isDeleted(), true);
    }
}