package ru.rfidcontrol;

import com.google.inject.Provider;
import org.junit.Ignore;
import org.junit.Test;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.company.CompanyInfo;
import ru.rfidcontrol.core.data.inventory.InventoryInfo;
import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.data.user.UserInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Inventory;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.InventoryService;
import ru.rfidcontrol.core.services.StorageService;
import ru.rfidcontrol.core.services.UserService;

import javax.inject.Inject;

import static org.junit.Assert.*;

@Ignore
public class InventoryTest extends BaseTest {

    @Inject CompanyService companyService;
    @Inject StorageService storageService;
    @Inject UserService userService;
    @Inject InventoryService inventoryService;
    @Inject Provider<ClientContext> clientContextProvider;

    @Test
    public void testInventoryCreate() {
        String testName = this.getClass().getCanonicalName() + "-testInventoryCreate";
        Company company;
        CompanyInfo info = new CompanyInfo();
        info.name = testName;
        info.email = testName + "@mail";
        company = companyService.createCompany(info);
        assertNotNull(company);
        assertTrue(company.getId() > 0);

        UserInfo userInfo = new UserInfo();
        userInfo.companyId = company.getId();
        userInfo.name = testName;
        User user = userService.createUser(userInfo);
        assertNotNull(user);
        assertTrue(user.getId() > 0);


        StorageInfo storageInfo = new StorageInfo();
        storageInfo.companyId = company.getId();
        storageInfo.name = testName;
        Storage storage = storageService.createStorage(storageInfo);
        assertNotNull(storage);
        assertTrue(storage.getId() > 0);
        storageInfo.id = storage.getId();

        clientContextProvider.get().setStorageId(storage.getId());
        assertEquals(storage.getId(), clientContextProvider.get().getStorageId());

        InventoryInfo inventoryInfo = new InventoryInfo();
        inventoryInfo.storageId = storageInfo.id;
        Inventory inventory = inventoryService.startInventory(inventoryInfo);
        assertNotNull(inventory);
        assertEquals(inventory.getStorage().getId(), storage.getId());
    }
}