package ru.rfidcontrol;

import org.junit.Ignore;
import ru.rfidcontrol.core.data.company.CompanyInfo;
import ru.rfidcontrol.core.data.user.UserInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.UserService;
import org.junit.Test;

import javax.inject.Inject;

import static org.junit.Assert.*;

@Ignore
public class UserTest extends BaseTest {

    @Inject
    UserService userService;
    @Inject
    CompanyService companyService;

    @Test
    public void testCreateUser() {
        CompanyInfo info = new CompanyInfo();
        info.name = "testCreateUser";
        info.email = "testCreateUser@test.com";

        Company company = companyService.createCompany(info);
        assertNotNull(company);
        assertTrue(company.getId() > 0);


        UserInfo userInfo = new UserInfo();
        userInfo.name = "testCreateUser";
        userInfo.isAdmin = true;
        userInfo.companyId = company.getId();

        User user = userService.createUser(userInfo);
        assertNotNull(user);
        assertNotNull(user.getCompany());
        assertEquals(user.getCompany().getId(), company.getId());

        int userId = user.getId();
        assertEquals(user.getName(), "testCreateUser");
        assertTrue(userId > 0);

        assertEquals(1, company.getUsers().size());

        User userToDelete = userService.findUser(userId);
        assertNotNull(userToDelete);
        userService.deleteUser(userToDelete);

        User deletedUser = userService.findUser(userId);
        assertNotNull(deletedUser);
        assertEquals(deletedUser.isDeleted(), true);
    }



}