package ru.rfidcontrol;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;
import ru.rfidcontrol.app.config.ConfigurationException;
import ru.rfidcontrol.app.config.ServerConfig;
import ru.rfidcontrol.app.config.ServerConfigLoader;
import ru.rfidcontrol.app.guice.ApplicationModule;
import ru.rfidcontrol.guice.TestApplicationModule;
import liquibase.Liquibase;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.HsqlConnection;
import liquibase.resource.FileSystemResourceAccessor;
import liquibase.resource.ResourceAccessor;
import org.junit.Before;

import java.sql.Connection;
import java.sql.DriverManager;

public abstract class BaseTest {

    protected static Injector injector;

    static {
        ServerConfig config = ServerConfigLoader.load("config-test");
        try {
            initDb(config);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        injector = Guice.createInjector(new ApplicationModule(config), new TestApplicationModule());
        injector.getInstance(PersistService.class).start();
    }

    public static void initDb(ServerConfig config) throws Exception{
        if (null == config.getMigrationPath()) {
            throw new ConfigurationException("config: migration path is null");
        }
        Connection connection = DriverManager.getConnection("jdbc:h2:mem:warehouse", "root", "1234");
        DatabaseConnection c = new HsqlConnection(connection);

        Liquibase liquibase = null;
        try {
            ResourceAccessor resourceAccessor = new FileSystemResourceAccessor(config.getMigrationPath());
            liquibase = new Liquibase("changelog.xml", resourceAccessor, c);
            liquibase.update("jdbc/dsName");
        } finally {
            if (liquibase != null) {
                liquibase.forceReleaseLocks();
            }
            c.rollback();
            c.close();
        }
    }

    @Before
    public void setup() {
        injector.injectMembers(this);
    }

}