package ru.rfidcontrol.guice;

import ru.rfidcontrol.app.services.LoginService;
import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.User;

public class MockLoginServiceImpl implements LoginService {
    @Override
    public User loginUser(String username, String password) {
        return null;
    }

    @Override
    public Device loginDevice(String imei) {
        return null;
    }

    @Override
    public void loginStorage(StorageInfo storageInfo) {}

    @Override
    public void logout() {
    }
}
