package ru.rfidcontrol.guice;

import com.google.inject.AbstractModule;
import ru.rfidcontrol.app.services.LoginService;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.services.AccessControlService;

public class TestApplicationModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ClientContext.class).toProvider(MockUserContextProvider.class);
        bind(AccessControlService.class).to(MockAccessControlServiceImpl.class).asEagerSingleton();
        bind(LoginService.class).to(MockLoginServiceImpl.class).asEagerSingleton();
    }

}
