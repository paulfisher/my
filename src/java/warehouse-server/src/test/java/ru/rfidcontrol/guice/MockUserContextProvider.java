package ru.rfidcontrol.guice;

import com.google.inject.Provider;
import ru.rfidcontrol.core.ClientContext;

public class MockUserContextProvider implements Provider<ClientContext> {

    private static ClientContext context;

    @Override
    public ClientContext get() {
        if (context == null) {
            context = new ClientContext();
        }
        return context;
    }
}
