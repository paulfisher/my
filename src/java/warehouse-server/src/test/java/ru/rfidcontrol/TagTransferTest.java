package ru.rfidcontrol;

import com.google.inject.Provider;
import org.junit.Ignore;
import org.junit.Test;
import ru.rfidcontrol.core.ClientContext;
import ru.rfidcontrol.core.data.company.CompanyInfo;
import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.data.transfer.TransferInfo;
import ru.rfidcontrol.core.data.user.UserInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.entity.Transfer;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.entity.type.TransferDirection;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.StorageService;
import ru.rfidcontrol.core.services.TransferService;
import ru.rfidcontrol.core.services.UserService;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class TagTransferTest extends BaseTest {

    @Inject CompanyService companyService;
    @Inject StorageService storageService;
    @Inject UserService userService;
    @Inject TransferService transferService;
    @Inject Provider<ClientContext> clientContextProvider;

    @Test
    public void testTransferCreate() {
        String testName = this.getClass().getCanonicalName() + "-testTransferCreate";
        Company company;
        CompanyInfo info = new CompanyInfo();
        info.name = testName;
        info.email = testName + "@mail";
        company = companyService.createCompany(info);
        assertNotNull(company);
        assertTrue(company.getId() > 0);

        UserInfo userInfo = new UserInfo();
        userInfo.companyId = company.getId();
        userInfo.name = testName;
        User user = userService.createUser(userInfo);
        assertNotNull(user);
        assertTrue(user.getId() > 0);


        StorageInfo storageInfo = new StorageInfo();
        storageInfo.companyId = company.getId();
        storageInfo.name = testName;
        Storage storage = storageService.createStorage(storageInfo);
        assertNotNull(storage);
        assertTrue(storage.getId() > 0);

        clientContextProvider.get().setStorageId(storage.getId());
        assertEquals(storage.getId(), clientContextProvider.get().getStorageId());

        TransferInfo transferInfo = new TransferInfo(TransferDirection.IN);
        transferInfo.storageId = storage.getId();
        transferInfo.userId = user.getId();
        Transfer transfer = transferService.registerTransfer(transferInfo);
        assertNotNull(transfer);
        assertTrue(transfer.getId() > 0);
    }
}