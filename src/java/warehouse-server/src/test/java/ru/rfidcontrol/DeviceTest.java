package ru.rfidcontrol;

import com.google.inject.Inject;
import org.junit.Ignore;
import org.junit.Test;
import ru.rfidcontrol.core.data.company.CompanyInfo;
import ru.rfidcontrol.core.data.device.DeviceInfo;
import ru.rfidcontrol.core.data.storage.StorageInfo;
import ru.rfidcontrol.core.data.user.UserInfo;
import ru.rfidcontrol.core.entity.Company;
import ru.rfidcontrol.core.entity.Device;
import ru.rfidcontrol.core.entity.Storage;
import ru.rfidcontrol.core.entity.User;
import ru.rfidcontrol.core.services.CompanyService;
import ru.rfidcontrol.core.services.DeviceService;
import ru.rfidcontrol.core.services.StorageService;
import ru.rfidcontrol.core.services.UserService;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class DeviceTest extends BaseTest {

    @Inject UserService userService;
    @Inject CompanyService companyService;
    @Inject StorageService storageService;
    @Inject DeviceService deviceService;

    @Test
    public void testUserDevices() {
        String testName = this.getClass().getCanonicalName() + "-testUserDevices";
        CompanyInfo info = new CompanyInfo();
        info.name = testName;
        info.email = testName + "@test.com";

        Company company = companyService.createCompany(info);
        assertNotNull(company);
        assertTrue(company.getId() > 0);

        StorageInfo storageInfo = new StorageInfo();
        storageInfo.companyId = company.getId();
        storageInfo.name = testName;
        Storage storage = storageService.createStorage(storageInfo);
        assertNotNull(storage);
        assertTrue(storage.getId() > 0);

        String imeiCode = "11111";
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.companyId = company.getId();
        deviceInfo.imeiCode = imeiCode;
        deviceInfo.name = testName;
        deviceInfo.storageId = storage.getId();
        Device device = deviceService.createDevice(deviceInfo);

        assertNotNull(device);

        UserInfo userInfo = new UserInfo();
        userInfo.name = testName;
        userInfo.companyId = company.getId();

        User user = userService.createUser(userInfo);
        assertNotNull(user);

        userService.setDevice(user, device);

        Device sameDevice = deviceService.getDevice(device.getId());
        assertNotNull(sameDevice);
        assertTrue(sameDevice.getUsers().size() == 1);
        assertTrue(sameDevice.getUsers().get(0).getId() == user.getId());
    }



}