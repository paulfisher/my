;(function() {

    getApp().
        controller('gmailController', GmailController);

    GmailController.$inject = ['$scope', '$rootScope', 'googleService', 'windowHelper', 'googleAuth'];
    function GmailController(scope, rootScope, googleService, windowHelper, googleAuth) {

        var CONTACTS_PER_PAGE = 100;

        var vm = scope;
        vm.token = null;
        vm.authWindow = null;
        vm.shareText = null;
        vm.shareSubject = null;
        vm.author = {};
        vm.contacts = [];
        vm.checked = [];
        vm.loading = false;
        vm.isSendToAll = false;

        vm.send = send;
        vm.toggleSendToAll = toggleSendToAll;
        vm.toggleContact = toggleContact;
        vm.getImage = getImage;
        vm.isReadyToSend = isReadyToSend;
        vm.loadMore = loadMore;
        vm.toggleAll = toggleAll;
        vm.allChecked = false;
        vm.hasMoreItems = true;
        vm.searchContact = "";

        vm.messageSending = false;
        vm.messageSent = false;

        function loadMore() {
            loadContacts();
        }

        function toggleAll() {
            if (vm.allChecked) {
                uncheckAll();
                vm.allChecked = false;
            } else {
                checkAll();
                vm.allChecked = true;
            }
        }

        function checkAll() {
            for (var key in vm.contacts) {
                if (vm.contacts.hasOwnProperty(key)) {
                    checkContact(vm.contacts[key]);
                }
            }
        }

        function uncheckAll() {
            for (var key in vm.contacts) {
                if (vm.contacts.hasOwnProperty(key)) {
                    uncheckContact(vm.contacts[key]);
                }
            }
        }

        function checkContact(contact) {
            contact.checked = true;
            vm.checked.push(contact);
        }

        function uncheckContact(contact) {
            contact.checked = false;
            var index = vm.checked.indexOf(contact);
            if (index > -1) {
                vm.checked.splice(index, 1);
            }
        }

        function toggleContact(contact) {
            if (contact.checked) {
                uncheckContact(contact);
                vm.allChecked = false;
            } else {
                checkContact(contact);
            }
        }

        function isReadyToSend() {
            return (vm.checked.length > 0 || vm.isSendToAll) && $.trim(vm.shareText)  != '';
        }

        function toggleSendToAll() {
            vm.isSendToAll = !vm.isSendToAll;
        }

        function sendToAll() {
            vm.messageSending = true;
            vm.messageSent = false;

            googleService.gmailShareAll(vm.shareText, vm.shareSubject)
                .success(function() {
                    vm.messageSending = false;
                    vm.messageSent = true;
                    vm.shareText = '';
                    setTimeout(function() {
                        $('#gmailModal').modal('hide');
                    }, 1500);
                })
                .error(function() {
                    vm.messageSending = false;
                    console.log(arguments);
                });

            return false;
        }

        function sendPersonally() {
            vm.messageSending = true;
            vm.messageSent = false;
            var emails = [];
            angular.forEach(vm.checked, function(contact) {
                if (contact.checked) {
                    angular.forEach(contact.emails, function(email) {
                        emails.push(email);
                    });
                }
            });

            googleService.gmailShare(vm.shareText, vm.shareSubject, emails)
                .success(function() {
                    vm.messageSending = false;
                    vm.messageSent = true;
                    vm.shareText = '';
                    angular.forEach(vm.checked, function(contact) {
                        uncheckContact(contact);
                    });
                    setTimeout(function() {
                        $('#gmailModal').modal('hide');
                    }, 1500);
                })
                .error(function() {
                    vm.messageSending = false;
                    console.log(arguments);
                });

            return false;
        }

        function send() {
            if (vm.isSendToAll) {
                sendToAll();
            } else {
                sendPersonally();
            }

        }

        function getImage(item) {
            if (item.image) {
                return item.image + "&access_token=" + vm.token.access_token;
            } else {
                return '/images/profile.png';
            }
        }

        function auth() {
            vm.loading = true;
            var scopes = 'email profile https://www.googleapis.com/auth/contacts.readonly https://www.googleapis.com/auth/gmail.compose';
            googleAuth.auth('gmailShare', scopes,
                function(auth2, isSignedIn) {
                    vm.loading = false;

                    if (isSignedIn) {
                        vm.token = auth2.currentUser.get().getAuthResponse();
                        googleService.setAccessToken(vm.token).success(function() {
                            loadContacts();
                        });
                    }
                },
                function(auth2) {
                    auth2.signIn().then(null, function() {
                        $('#gmailModal').modal('hide');
                    });
                });
        }

        function share(data) {
            if (vm.contacts.length > 0) {
                return;
            }

            vm.shareText = data.body;
            vm.shareSubject = data.subject;

            auth();
        }

        function loadContacts(search) {
            if (vm.loading) {
                return;
            }
            vm.loading = true;
            googleService.getContacts(vm.contacts.length, search, CONTACTS_PER_PAGE)
                .success(function(response) {
                    if (response.items.length == 0) {
                        vm.hasMoreItems = false;
                    }
                    addContacts(response.items);
                    vm.loading = false;
                })
                .error(function() {
                    vm.loading = false;
                    console.error(arguments);
                    //auth();
                });
        }

        function addContacts(contacts) {
            for (var key in contacts) {
                if (contacts.hasOwnProperty(key)) {
                    vm.contacts.push(contacts[key]);
                }
            }

        }

        vm.contactsFiltered = false;
        vm.$watch('searchContact', function() {
            setTimeout(function() {
                var searchContact = vm.searchContact;
                if (searchContact.length >= 3) {
                    vm.contacts = [];
                    vm.contactsFiltered = true;
                    vm.hasMoreItems = false;
                    loadContacts(searchContact);
                } else if (vm.contactsFiltered && searchContact.length == 0) {
                    vm.contacts = [];
                    vm.contactsFiltered = false;
                    vm.hasMoreItems = true;
                    loadContacts();
                }
            }, 1500);
        });

        rootScope.$on('share', function(event, data) {
            $('#gmailModal').modal('show');
            share(data);
        });
    }



})();