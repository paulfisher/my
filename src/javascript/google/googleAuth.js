;(function() {

    getApp().
        factory('googleAuth', GoogleAuth);

    GoogleAuth.$inject = ['googleService'];
    function GoogleAuth(googleService) {

        var auth2Cache = {};
        function initGapiScript(onLoad) {
            var gapiScript = document.createElement('script');
            gapiScript.setAttribute('src','//apis.google.com/js/api.js');
            gapiScript.onload = onLoad;
            document.head.appendChild(gapiScript);
        }

        var gapiInitialized = false;
        function auth(key, scopes, onSignInStatusUpdated, onAuthInit) {
            if (!gapiInitialized) {
                gapiInitialized = true;
                initGapiScript(function() {
                    gapi.load('client:auth2', function() {
                        auth(key, scopes, onSignInStatusUpdated, onAuthInit);
                    });
                });
                return;
            }

            if (!auth2Cache[key]) {
                if (scopes == '') {
                    scopes = 'profile email';
                }
                googleService.getCredentials(function(_, clientId, apiKey) {
                    gapi.client.setApiKey(apiKey);
                    gapi.auth2.init({
                        client_id: clientId,
                        scope: scopes
                    }).then(function() {
                        auth2Cache[key] = gapi.auth2.getAuthInstance();

                        if (auth2Cache[key].isSignedIn.get()) {
                            auth2Cache[key].signOut();
                        }

                        auth2Cache[key].isSignedIn.listen(function(isSignIn) {
                            onSignInStatusUpdated(auth2Cache[key], isSignIn);
                        });

                        onAuthInit(auth2Cache[key]);


                    });
                });
            } else {
                onAuthInit(auth2Cache[key]);
            }
        }

        return {
            auth: auth
        };
    }


})();