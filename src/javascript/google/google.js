;(function (w) {
    w.__google = w.__google || {};
    var _g = w.__google;

    function cleanGoogleGlobalData() {
        w.__google = {};
        _g = w.__google;
    }

    function initGapiScript(onLoad) {
        var gapiScript = document.createElement('script');
        gapiScript.setAttribute('src','//apis.google.com/js/api.js');
        gapiScript.onload = onLoad;
        document.head.appendChild(gapiScript);
    }

    getApp().
        directive('googleLogin', GoogleLogin).
        directive('googleShare', GoogleShare).
        directive('googleLink', GoogleLink).
        directive('googleUnlink', GoogleUnlink).
        directive('gmailShare', GmailShare);

    GoogleLogin.$inject = ['googleService', 'googleAuth'];
    function GoogleLogin(googleService, googleAuth) {
        return function(scope, element) {
            var nextInput = $('#RegistrationForm_next');
            if (nextInput.length == 0) {
                nextInput = $('#UserLogin_next');
            }
            var inviteInput = $('#RegistrationForm_inviteHash');

            element.bind('click', function() {
                if (inviteInput.length > 0 && $.trim(inviteInput.val()) == '') {
                    inviteInput.focus();
                    return false;
                }
                googleAuth.auth('login', '', onIsSignedInStatusUpdated, signIn);
            });

            function signIn(auth2) {
                auth2.signIn().then(null, function() {
                    $('#Profile_full_name').focus();
                    $('#UserLogin_username').focus();
                });
            }

            var onIsSignedInStatusUpdated = function(auth2, isSignedIn) {
                if (isSignedIn) {
                    var next = nextInput.val();
                    var invite = inviteInput.val();

                    var basicProfile = auth2.currentUser.get().getBasicProfile();
                    googleService.login({
                        id : basicProfile.getId(),
                        name : basicProfile.getName(),
                        email : basicProfile.getEmail(),
                        imageUrl : basicProfile.getImageUrl(),
                        invite : invite,
                        next: next
                    }).success(function(response) {
                        location.href = response.redirectTo ? response.redirectTo : '/';
                    });
                }
            };
        }
    }

    GoogleLink.$inject = ['googleService', '$http', 'googleAuth'];
    function GoogleLink(googleService, http, googleAuth) {
        return function(scope, element) {
            element.bind('click', function() {

                googleAuth.auth('link', '',
                    function(auth2, isSignedIn) {
                        if (isSignedIn) {
                            var basicProfile = auth2.currentUser.get().getBasicProfile();
                            var linkGoogleProfile = {
                                googleId : basicProfile.getId(),
                                name : basicProfile.getName(),
                                pic : basicProfile.getImageUrl()
                            };
                            http.post('/user/social/linkGoogle', $.param(linkGoogleProfile))
                                .success(function() {
                                    location.reload();
                                });
                        }
                    },
                    function(auth2) {
                        auth2.signIn().then(null, function() {});
                    });
                return false;
            });
        };

        function authSuccess() {
            googleService.me()
                .success(function(result) {
                    http.post('/user/social/linkGoogle', $.param({googleId: result.id, name: result.name, link: result.link, pic: result.picture})).
                        success(function() {
                            location.reload();
                        });
                });
        }
    }

    GoogleUnlink.$inject = ['$http'];
    function GoogleUnlink(http) {
        return function(scope, element) {

            function unlinkGoogle() {
                http.get('/user/social/unlinkGoogle')
                    .success(function() {
                        location.reload();
                    });
            }

            element.bind('click', function() {

                window.bifConfirm('Disconnect google', 'Disconnect your google account?', {
                    cb: unlinkGoogle
                });

                return false;
            });
        }
    }

    GoogleShare.$inject = ['windowHelper'];
    function GoogleShare(windowHelper) {
        return function(scope, element, $attrs) {
            var url = encodeURIComponent($attrs.googleShare);
            element.bind('click', function() {
                var shareUrl = "https://plus.google.com/share?url=" + url;
                windowHelper.popupWindow(shareUrl, 'Google+ share', 600, 600);
            });
        }
    }

    GmailShare.$inject = ['$rootScope'];
    function GmailShare(rootScope) {
        return function(scope, element, $attrs) {
            element.bind('click', function() {
                rootScope.$emit('share', {subject: $attrs.subject, body: $attrs.body});
            });
        }
    }

})(window);