;(function() {

    var clientId = '123.apps.googleusercontent.com';
    var apiKey = '12345';

    getApp().
        value('GOOGLE_API',
        {
            getCredentials: '/google/creds',
            auth: '/google/auth',
            setAccessToken: '/google/setAccessToken',
            getContacts: '/google/getContacts',
            gmailShare: '/google/gmailShare',
            me: '/google/me',
            gmailShareAll: '/google/gmailShareAll',
            login: '/user/login/google'
        }).
        factory('googleService', GoogleService);

    GoogleService.$inject = ['GOOGLE_API', '$http'];
    function GoogleService(GOOGLE_API, http) {
        http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

        return {
            getCredentials: getCredentials,
            setAccessToken: setAccessToken,
            auth: auth,
            getContacts: getContacts,
            gmailShare: gmailShare,
            me: me,
            gmailShareAll: gmailShareAll,
            login: login
        };

        function getCredentials(callback) {
            http.get(GOOGLE_API.getCredentials).success(function(data) {
                var clientId = data.clientId;
                var apiKey = data.apiKey;
                callback(data, clientId, apiKey)
            });
        }

        function auth() {
            return http.post(GOOGLE_API.auth);
        }

        function setAccessToken(authResponse) {
            return http.post(GOOGLE_API.setAccessToken, $.param({
                access_token : authResponse.access_token,
                token_type : authResponse.token_type,
                expires_in : authResponse.expires_in,
                id_token : authResponse.id_token,
                created : authResponse.first_issued_at
            }));
        }

        function getContacts(index, searchWord, count) {
            return http.post(GOOGLE_API.getContacts, $.param({index: index, count: count, search: searchWord}));
        }

        function me() {
            return http.get(GOOGLE_API.me);
        }

        function gmailShare(text, subject, emails) {
            return http.post(GOOGLE_API.gmailShare, $.param({text: text, emails: emails, subject: subject}));
        }

        function gmailShareAll(token, text, subject) {
            return http.post(GOOGLE_API.gmailShareAll, $.param({text: text, subject: subject}));
        }

        function login(data) {
            return http.post(GOOGLE_API.login, $.param(data));
        }
    }


})();