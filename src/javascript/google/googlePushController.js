;(function () {

    getApp()
        .controller('googlePushController', GooglePushController);

    GooglePushController.$inject = ['$scope', 'googlePushService'];
    function GooglePushController(scope, googlePushService) {
        var vm = scope;

        vm.install = install;

        function install() {
            googlePushService.install();
        }
    }

})();