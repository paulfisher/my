'use strict';

(function() {

    getApp().factory('googlePushService', GooglePushService);

    GooglePushService.$inject = ['pushService'];
    function GooglePushService(pushService) {
        var reg;
        return {
            install: install,
            subscribe: subscribe,
            unsubscribe: unsubscribe
        };

        function install() {
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('/googlePushSw.js').then(function() {
                    return navigator.serviceWorker.ready;
                }).then(function(serviceWorkerRegistration) {
                    reg = serviceWorkerRegistration;
                    subscribe();
                }).catch(function(error) {
                    console.log('sw error', error);
                });
            }
        }

        function subscribe() {
            reg.pushManager.subscribe({userVisibleOnly: true}).
                then(function(pushSubscription) {
                    pushService.saveDestination('chrome', pushSubscription.endpoint);
                });
        }

        function unsubscribe() {
            reg.pushManager.getSubscription().then(function(subscription) {
                subscription && subscription.unsubscribe();
            });
        }
    }
})();