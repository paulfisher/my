;(function() {

    angular
        .module('warehouseOnline')
        .constant('AUTH_API', {
            auth: '/auth',
            login: '/auth',
            logout: '/auth/logout'
        })
        .factory('authService', AuthService);

    AuthService.$inject = ['$q', 'userSession', 'AUTH_API', 'warehouseApi'];
    function AuthService($q, userSession, AUTH_API, warehouseApi) {
        return {
            login: login,
            authorize: authorize,
            logout: logout
        };

        function authorize() {
            var def = $q.defer();

            warehouseApi.get(AUTH_API.auth)
                .success(function(data) {
                    if (allowContext(data)) {
                        def.resolve(data);
                    } else {
                        def.reject();
                    }
                })
                .error(function() {
                    def.reject();
                });

            return def.promise;
        }

        function login(data) {
            var def = $q.defer();

            warehouseApi.post(AUTH_API.login, data)
                .success(function(result) {
                    if (allowContext(result.data)) {
                        userSession.loginSuccess(result.data);
                        def.resolve(result.data);
                    } else {
                        userSession.loginFailed();
                        def.reject(result);
                    }
                })
                .error(function(data) {
                    def.reject(data);
                });

            return def.promise;
        }

        function logout() {
            var def = $q.defer();

            warehouseApi.post(AUTH_API.logout)
                .success(function(result) {
                    if (result.data.userId == 0) {
                        userSession.logoutSuccess(result.data);
                        def.resolve(result.data);
                    } else {
                        def.reject();
                    }
                })
                .error(function() {
                    def.reject();
                });

            return def.promise;
        }

        function allowContext(context) {
            //пока что закрываем админам вход в online
            return context.userId > 0 && !context.admin
        }
    }

})();