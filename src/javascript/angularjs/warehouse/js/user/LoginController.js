;(function() {
    angular.module('warehouseOnline')
        .controller('loginController', LoginController);

    LoginController.$inject = ['$scope', '$location', 'userSession', 'authService'];
    function LoginController(scope, $location, userSession, authService) {
        var vm = scope;
        vm.onSubmit = onSubmit;
        vm.username = null;
        vm.password = null;
        vm.error = null;

        init();
        function init() {
            if (userSession.isAuthorized()) {
                $location.path('/admin');
            }
        }

        function onSubmit() {
            vm.error = null;
            authService.login({username: vm.username, password: vm.password})
                .then(function(data) {
                    $location.path('/admin');
                }, function(error) {
                    vm.error = error;
                });
        }
    }
})();