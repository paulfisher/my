;(function() {
    angular.module('warehouseOnline')
        .controller('logoutController', LogoutController);

    LogoutController.$inject = ['$scope', '$location', 'userSession', 'authService'];
    function LogoutController(scope, $location, userSession, authService) {
        var vm = scope;

        vm.init = init;

        function init() {
            setTimeout(function() {
                authService.logout();
            }, 500);
        }
    }
})();