angular.module('warehouseOnline', ['ngRoute', 'ngStorage']);

function objectToArray(object) {
    var result = [];
    for (var key in object) {
        if (object.hasOwnProperty(key)) {
            result.push(object[key]);
        }
    }
    return result;
}