;(function() {
    angular
        .module('warehouseOnline')
        .service('warehouseApi', WarehouseApi);

    WarehouseApi.$inject = ['$http', 'warehouseConfig'];
    function WarehouseApi($http, warehouseConfig) {
        return {
            api : prepareApiUrl,
            post : post,
            get: get
        };

        function post(method, data) {
            return $http.post(prepareApiUrl(method), data);
        }

        function get(method, params) {
            return $http.get(prepareApiUrl(method, params));
        }

        //example: converts '/company/find/{id}', {id: 123} to 'http://localhost:8080/company/find/123'
        function prepareApiUrl(method, params) {
            var apiUrl = warehouseConfig.apiBaseUrl + method;
            if (params) {
                for (var key in params) {
                    if (params.hasOwnProperty(key)) {
                        apiUrl = apiUrl.replace("{" + key + "}", params[key]);
                    }
                }
            }
            return apiUrl;
        }
    }

})();