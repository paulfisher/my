;(function() {
    angular
        .module('warehouseOnline')
        .constant('AUTH_EVENTS', {
            loginSuccess: 'auth-login-success',
            loginFailed: 'auth-login-failed',
            logoutSuccess: 'auth-logout-success',
            sessionTimeout: 'auth-session-timeout',
            notAuthenticated: 'auth-not-authenticated',
            notAuthorized: 'auth-not-authorized'
        })
        .service('userSession', UserSession);

    UserSession.$inject = ['AUTH_EVENTS', '$rootScope', '$sessionStorage'];
    function UserSession(AUTH_EVENTS, $rootScope, $sessionStorage) {
        return {
            loginSuccess : loginSuccess,
            loginFailed : loginFailed,
            logoutSuccess : logoutSuccess,
            authSuccess : authSuccess,
            authFailed : authFailed,
            isAuthorized : isAuthorized,
            getUserContext : getUserContext
        };

        function loginSuccess(userContext) {
            saveUserContext(userContext, true);
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, userContext);
        }

        function loginFailed() {
            saveUserContext({}, false);
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed, {});
        }

        function authSuccess(userContext) {
            saveUserContext(userContext, true);
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, userContext);
        }

        function logoutSuccess(userContext) {
            saveUserContext(userContext, false);
            $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess, userContext);
        }


        function authFailed() {
            saveUserContext({}, false);
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated, {});
        }

        function isAuthorized() {
            return $sessionStorage.authorized;
        }

        function saveUserContext(userContext, authorized) {
            $sessionStorage.userContext = userContext;
            $sessionStorage.authorized = authorized;
        }

        function getUserContext() {
            return $sessionStorage.userContext;
        }
    }
})();