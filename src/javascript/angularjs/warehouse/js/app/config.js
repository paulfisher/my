angular
    .module('warehouseOnline')
    .factory('onlineRequestInterceptor', OnlineRequestInterceptor)
    .constant('warehouseConfig', {
        apiBaseUrl: 'http://api.rfidcontrol.ru'
    })
    .config(WarehouseConfig);

var isAuthorized = function($q, userSession) {
    if (!userSession.isAuthorized()) {
        userSession.authFailed();
    }
    return userSession.isAuthorized();
};

WarehouseConfig.$inject = ['$routeProvider', '$httpProvider'];
function WarehouseConfig($routeProvider, $httpProvider) {
    $httpProvider.interceptors.push('onlineRequestInterceptor');
    $httpProvider.defaults.withCredentials = true;

    $routeProvider
        .when('/', {
            templateUrl : 'pages/login.html',
            controller  : 'loginController'
        })
        .when('/login', {
            templateUrl : 'pages/login.html',
            controller  : 'loginController'
        })
        .when('/logout', {
            templateUrl : 'pages/logout.html',
            controller  : 'logoutController',
            resolve: { authorized: isAuthorized }
        })
        .when('/admin', {
            templateUrl : 'pages/admin.html',
            controller : 'adminController',
            resolve: { authorized: isAuthorized }
        })

        .when('/tags', {
            templateUrl : 'pages/online/tag/search.html',
            controller : 'tagSearchController',
            resolve: { authorized: isAuthorized }
        })
        .when('/tag/:tagId', {
            templateUrl : 'pages/online/tag/view.html',
            controller : 'tagViewController',
            resolve: { authorized: isAuthorized }
        })

        .when('/transfers', {
            templateUrl : 'pages/online/transfer/search.html',
            controller : 'transferSearchController',
            reloadOnSearch: false,
            resolve: { authorized: isAuthorized }
        })
        .when('/transfer/:transferId', {
            templateUrl : 'pages/online/transfer/view.html',
            controller : 'transferViewController',
            resolve: { authorized: isAuthorized }
        })

        .when('/storages', {
            templateUrl : 'pages/online/storage/search.html',
            controller : 'storageSearchController',
            resolve: { authorized: isAuthorized }
        })
        .when('/storage/:storageId', {
            templateUrl : 'pages/online/storage/view.html',
            controller : 'storageViewController',
            resolve: { authorized: isAuthorized }
        })

        .when('/devices', {
            templateUrl : 'pages/online/device/search.html',
            controller : 'deviceSearchController',
            resolve: { authorized: isAuthorized }
        })
        .when('/device/:deviceId', {
            templateUrl : 'pages/online/device/view.html',
            controller : 'deviceViewController',
            resolve: { authorized: isAuthorized }
        })

        .when('/users', {
            templateUrl : 'pages/online/user/search.html',
            controller : 'userSearchController',
            resolve: { authorized: isAuthorized }
        })
        .when('/user/:userId', {
            templateUrl : 'pages/online/user/view.html',
            controller : 'userViewController',
            resolve: { authorized: isAuthorized }
        })

        .when('/inventories', {
            templateUrl : 'pages/online/inventory/search.html',
            controller : 'inventorySearchController',
            resolve: { authorized: isAuthorized }
        })
        .when('/inventory/:inventoryId', {
            templateUrl : 'pages/online/inventory/view.html',
            controller : 'inventoryViewController',
            resolve: { authorized: isAuthorized }
        })

        .otherwise({
            redirectTo: '/'
        });
}

OnlineRequestInterceptor.$inject = ['$q', 'userSession'];
function OnlineRequestInterceptor($q, userSession) {
    return {
        responseError: function(error) {
            userSession.authFailed(error);
            return $q.reject(error);
        }
    };
}