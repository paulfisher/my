;(function() {
    angular.module('warehouseOnline')
        .controller('deviceSearchController', DeviceSearchController);

    DeviceSearchController.$inject = ['$scope', '$location', 'deviceService', '$routeParams'];
    function DeviceSearchController(scope, $location, deviceService, $routeParams) {
        var vm = scope;
        vm.search = search;
        vm.show = show;

        vm.searchParams = {};
        vm.devices = [];

        function search() {
            if ($routeParams.storageId > 0) {
                vm.searchParams.storageId = $routeParams.storageId;
            }
            deviceService.findDevices(vm.searchParams)
                .success(function(findDevicesResult) {
                    vm.devices = findDevicesResult.data.devices;
                });
        }

        function show(device) {
            $location.path('/device/' + device.id);
        }
    }
})();