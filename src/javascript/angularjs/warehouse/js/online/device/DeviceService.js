;(function () {

    angular.module('warehouseOnline')
        .constant('DEVICE_API',
        {
            findDevices: '/online/device/find',
            getDevice: '/online/device/get'
        })
        .factory('deviceService', DeviceService);

    DeviceService.$inject = ['DEVICE_API', 'warehouseApi'];
    function DeviceService(deviceApi, warehouseApi) {
        return {
            findDevices: findDevices,
            getDevice: getDevice
        };

        function findDevices(deviceInfo) {
            return warehouseApi.post(deviceApi.findDevices, {info: deviceInfo});
        }

        function getDevice(deviceId) {
            return warehouseApi.post(deviceApi.getDevice, {info: {id: deviceId}});
        }
    }

})();