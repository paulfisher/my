;(function() {
    angular.module('warehouseOnline')
        .controller('deviceViewController', DeviceViewController);

    DeviceViewController.$inject = ['$scope', '$location', 'deviceService', '$routeParams'];
    function DeviceViewController(scope, $location, deviceService, $routeParams) {
        var vm = scope;
        vm.init = init;

        vm.device = null;

        function init() {
            deviceService.getDevice($routeParams.deviceId)
                .success(function(response) {
                    vm.device = response.data;
                });
        }
    }
})();