;(function () {

    angular.module('warehouseOnline')
        .constant('USER_API',
            {
                getMe: '/online/user/me',
                findUsers: '/online/user/find',
                getUser: '/online/user/get'
            })
        .factory('userService', UserService);

    UserService.$inject = ['USER_API', 'warehouseApi'];
    function UserService(userApi, warehouseApi) {
        return {
            getMe: getMe,
            findUsers: findUsers,
            getUser: getUser
        };

        function getMe() {
            return warehouseApi.get(userApi.getMe);
        }

        function findUsers(userInfo) {
            return warehouseApi.post(userApi.findUsers, {info: userInfo})
        }

        function getUser(userId) {
            return warehouseApi.post(userApi.getUser, {info: {id: userId}});
        }
    }

})();