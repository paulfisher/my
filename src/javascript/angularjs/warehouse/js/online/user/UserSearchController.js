;(function() {
    angular.module('warehouseOnline')
        .controller('userSearchController', UserSearchController);

    UserSearchController.$inject = ['$scope', '$location', 'userService', '$routeParams'];
    function UserSearchController(scope, $location, userService, $routeParams) {
        var vm = scope;
        vm.search = search;
        vm.show = show;

        vm.users = [];

        function search() {
            userService.findUsers({})
                .success(function(findUsersResult) {
                    vm.users = findUsersResult.data.users;
                });
        }

        function show(user) {
            $location.path('/user/' + user.id);
        }
    }
})();