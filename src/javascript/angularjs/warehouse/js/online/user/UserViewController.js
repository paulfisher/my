;(function() {
    angular.module('warehouseOnline')
        .controller('userViewController', UserViewController);

    UserViewController.$inject = ['$scope', '$location', 'userService', '$routeParams'];
    function UserViewController(scope, $location, userService, $routeParams) {
        var vm = scope;
        vm.init = init;

        vm.user = null;

        function init() {
            userService.getUser($routeParams.userId)
                .success(function(response) {
                    vm.user = response.data;
                });
        }
    }
})();