;(function() {

    angular
        .module('warehouseOnline')
        .controller('rootController', RootController);

    RootController.$inject = ['$scope', 'userSession', '$location', 'AUTH_EVENTS', '$rootScope', 'authService', 'userService', 'companyService'];
    function RootController(scope, userSession, $location, AUTH_EVENTS, $rootScope, authService, userService, companyService) {
        var vm = scope;

        $rootScope.title = '';

        vm.showSidebar = false;
        vm.authorized = userSession.isAuthorized();
        vm.userContext = userSession.getUserContext();
        vm.user = null;
        vm.company = null;
        vm.$watch('showSidebar', function(showSidebar) {
            //HACK. Not angular way.
            if (showSidebar) {
                $('#page-wrapper').css('margin-left', "");
            } else {
                $('#page-wrapper').css('margin-left', 0);
            }
        });
        vm.$watch('authorized', function() {
            vm.showSidebar = vm.authorized;//on authorized, show sidebar
        });
        loadUser();
        loadCompany();


        $rootScope.$on(AUTH_EVENTS.loginSuccess, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.loginFailed, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.sessionTimeout, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.notAuthenticated, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.notAuthorized, updateAuthorized);


        function updateAuthorized() {
            vm.authorized = userSession.isAuthorized();
            vm.userContext = userSession.getUserContext();

            if (!vm.authorized) {
                //check one more time to be sure
                authService.authorize()
                    .then(function(data) {
                        console.log('authorized');
                        userSession.authSuccess(data);
                    }, function() {
                        console.log('not authorized');
                        if ($location.path() != '/login') {
                            $location.path('/login');
                        }
                    });
            }
            loadUser();
            loadCompany();
        }

        function loadUser() {
            if (vm.authorized) {
                userService.getMe()
                    .success(function(myUser) {
                        vm.user = myUser.data;
                    });
            }
        }

        function loadCompany() {
            if (vm.authorized) {
                companyService.getMyCompany()
                    .success(function(myCompany) {
                        vm.company = myCompany.data;
                        $rootScope.title = vm.company.name;
                    });
            }
        }
    }

})();