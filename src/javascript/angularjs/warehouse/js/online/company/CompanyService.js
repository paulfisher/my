;(function () {

    angular
        .module('warehouseOnline')
        .constant('COMPANY_API',
            {
                getMyCompany: '/online/company/my',
                getMyCompanyStats: '/online/company/my/stats'
            })
        .factory('companyService', CompanyService);

    CompanyService.$inject = ['COMPANY_API', 'warehouseApi'];
    function CompanyService(companyApi, warehouseApi) {
        return {
            getMyCompany: getMyCompany,
            getMyCompanyStats : getMyCompanyStats
        };

        function getMyCompany() {
            return warehouseApi.get(companyApi.getMyCompany);
        }

        function getMyCompanyStats() {
            return warehouseApi.get(companyApi.getMyCompanyStats);
        }

    }

})();