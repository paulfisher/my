;(function () {

    angular.module('warehouseOnline')
        .constant('TRANSFER_DIRECTION',
        {
            in: 1,
            out: 2
        })
        .constant('TRANSFER_API',
        {
            findTransfers: '/online/transfer/find',
            getTransfer: '/online/transfer/get',
            findTag: '/online/transfer/findTag',
            registerTransfer: '/online/transfer/register'
        })
        .factory('transferService', TransferService);

    TransferService.$inject = ['TRANSFER_API', 'warehouseApi'];
    function TransferService(transferApi, warehouseApi) {
        return {
            findTransfers: findTransfers,
            getTransfer: getTransfer,
            findTag: findTag,
            registerTransfer: registerTransfer
        };

        function registerTransfer(tag, direction, storageId) {
            var tags = {};
            tags[tag.id] = {
                id: tag.id
            };
            return warehouseApi.post(transferApi.registerTransfer,
                {
                    transferInfo: {
                        directionType: direction,
                        tagInfoList: {
                            tags: tags
                        },
                        storageId: storageId
                    }
                });
        }

        function findTag(tagId) {
            return warehouseApi.post(transferApi.findTag, {tagInfo: {id: tagId}});
        }

        function findTransfers(transferInfo, pagination, sorting) {
            return warehouseApi.post(transferApi.findTransfers, {transferInfo: transferInfo, pagination: pagination, sorting: sorting});
        }

        function getTransfer(transferId) {
            return warehouseApi.post(transferApi.getTransfer, {transferInfo: {id: transferId}});
        }
    }

})();