;(function() {
    angular.module('warehouseOnline')
        .controller('tagTransferSearchController', TagTransferSearchController);

    TagTransferSearchController.$inject = ['$scope', '$location', 'transferService', '$routeParams'];
    function TagTransferSearchController(scope, $location, transferService, $routeParams) {
        var vm = scope;

        vm.search = search;
        vm.show = show;

        vm.tagId = $routeParams.tagId;
        vm.tagTransfers = [];

        function search() {
            transferService.findTag(vm.tagId)
                .success(function(findTagResult) {
                    vm.tagTransfers = findTagResult.data.tagTransfers;
                });
        }

        function show(tagTransfer) {
            $location.path('/transfer/' + tagTransfer.transferId);
        }
    }
})();