;(function() {
    angular.module('warehouseOnline')
        .controller('transferSearchController', TransferSearchController);

    TransferSearchController.$inject = ['$scope', '$location', 'transferService', '$routeParams', '$route'];
    function TransferSearchController(scope, $location, transferService, $routeParams, $route) {

        var TRANSFERS_PER_PAGE = 50;
        var SORT_ASC = 0;
        var SORT_DESC = 1;
        var START_PAGE = 1;

        var vm = scope;

        vm.search = search;
        vm.toggleOrder = toggleOrder;
        vm.show = show;
        vm.applyFilter = applyFilter;
        vm.sortingPropertyToObjectKeyMap = {
            'when' : 'when',
            'user' : 'userId',
            'device' : 'deviceId'
        };

        vm.hasNext = hasNext;
        vm.hasPrev = hasPrev;

        vm.next = next;
        vm.prev = prev;

        vm.searchRequest = {
            storageId: $routeParams.storageId,
            deviceId: $routeParams.deviceId,
            userId: $routeParams.userId,
            directionType: $routeParams.directionType,
            page: +$routeParams.page || START_PAGE,
            asc: $routeParams.asc,
            prop: $routeParams.prop || 'when'
        };

        vm.showStorageFilter = vm.searchRequest.storageId > 0;
        vm.showDeviceFilter = vm.searchRequest.deviceId > 0;
        vm.showUserFilter = vm.searchRequest.userId > 0;
        vm.showDirectionFilter = vm.searchRequest.directionType > 0;

        vm.transfers = [];

        vm.sorting = {
            enabled: true,
            property: vm.searchRequest.prop,
            asc: vm.searchRequest.asc == SORT_ASC
        };
        vm.pagination = {
            count: TRANSFERS_PER_PAGE
        };

        function search() {
            vm.loading = true;
            vm.pagination.offset = Math.max((vm.searchRequest.page-1), 0) * vm.pagination.count;
            transferService.findTransfers(vm.searchRequest, vm.pagination, vm.sorting)
                .success(function(findTransfersResult) {
                    vm.transfers = objectToArray(findTransfersResult.data.transfers);
                    vm.pagination = findTransfersResult.data.pagination;
                    vm.loading = false;
                });
        }

        function hasNext() {
            return vm.pagination.offset + vm.pagination.count < vm.pagination.total;
        }

        function hasPrev() {
            return vm.pagination.offset > 0;
        }

        function next() {
            if (hasNext()) {
                vm.searchRequest.page++;
                update();
            }
        }

        function prev() {
            if (hasPrev()) {
                vm.searchRequest.page--;
                update();
            }
        }

        function show(transfer) {
            $location.url('/transfer/' + transfer.id);
        }

        function applyFilter(param, value) {
            vm.searchRequest.page = 1;//reset page
            if (vm.searchRequest[param] == value) {
                vm.searchRequest[param] = 0;
            } else {
                vm.searchRequest[param] = value;
            }
            update();
        }

        function update() {
            $route.updateParams(vm.searchRequest);
            search();
        }

        function toggleOrder(prop) {
            if (vm.sorting.property == prop) {
                vm.sorting.asc = !vm.sorting.asc;
            }
            vm.sorting.property = prop;
            vm.searchRequest['asc'] = vm.sorting.asc ? SORT_ASC : SORT_DESC;
            vm.searchRequest['prop'] = prop;
            update();
        }
    }
})();