;(function() {
    angular.module('warehouseOnline')
        .controller('transferViewController', TransferViewController);

    TransferViewController.$inject = ['$scope', '$location', 'transferService', '$routeParams', '$route'];
    function TransferViewController(scope, $location, transferService, $routeParams, $route) {
        var vm = scope;
        vm.init = init;

        vm.transfer = null;

        function init() {
            transferService.getTransfer($routeParams.transferId)
                .success(function(response) {
                    vm.transfer = response.data;
                });
        }
    }
})();