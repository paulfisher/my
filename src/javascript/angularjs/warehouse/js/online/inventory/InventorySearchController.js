;(function() {
    angular.module('warehouseOnline')
        .controller('inventorySearchController', InventorySearchController);

    InventorySearchController.$inject = ['$scope', '$location', 'inventoryService', '$routeParams'];
    function InventorySearchController(scope, $location, inventoryService, $routeParams) {
        var vm = scope;
        vm.search = search;
        vm.show = show;
        vm.cancel = cancel;
        vm.finish = finish;

        vm.inventories = [];

        function search() {
            inventoryService.findInventories({})
                .success(function(findInventoryResult) {
                    vm.inventories = findInventoryResult.data.inventories;
                });
        }

        function show(inventory) {
            $location.path('/inventory/' + inventory.id);
        }

        function cancel(inventory) {
            if (confirm("Вы уверены, что хотите ОТМЕНИТЬ инвентаризацию " + inventory.id + "?")) {
                inventoryService.cancelInventory(inventory)
                    .success(function(cancelInventoryResult) {
                        angular.merge(inventory, cancelInventoryResult.data);
                    });
            }
        }

        function finish(inventory) {
            if (confirm("Вы уверены, что хотите ЗАВЕРШИТЬ инвентаризацию " + inventory.id + "?")) {
                inventoryService.finishInventory(inventory)
                    .success(function(finishInventoryResult) {
                        angular.merge(inventory, finishInventoryResult.data);
                    });
            }
        }
    }
})();