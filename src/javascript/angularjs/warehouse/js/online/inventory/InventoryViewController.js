;(function() {
    angular
        .module('warehouseOnline')
        .controller('inventoryViewController', InventoryViewController);

    InventoryViewController.$inject = ['$scope', '$location', 'inventoryService', '$routeParams'];
    function InventoryViewController(scope, $location, inventoryService, $routeParams) {
        var vm = scope;
        vm.init = init;

        vm.inventory = null;

        function init() {
            inventoryService.getInventory($routeParams.inventoryId)
                .success(function(response) {
                    vm.inventory = response.data;
                });
        }
    }
})();