;(function () {

    angular.module('warehouseOnline')
        .constant('INVENTORY_API',
        {
            findInventories: '/online/inventory/find',
            getInventory: '/online/inventory/get',
            cancelInventory: '/online/inventory/cancel',
            finishInventory: '/online/inventory/finish'
        })
        .factory('inventoryService', InventoryService);

    InventoryService.$inject = ['INVENTORY_API', 'warehouseApi'];
    function InventoryService(inventoryApi, warehouseApi) {
        return {
            findInventories: findInventories,
            getInventory: getInventory,
            cancelInventory: cancelInventory,
            finishInventory: finishInventory
        };

        function findInventories(inventoryInfo) {
            return warehouseApi.post(inventoryApi.findInventories, {inventoryInfo: inventoryInfo});
        }

        function getInventory(inventoryId) {
            return warehouseApi.post(inventoryApi.getInventory, {inventoryInfo: {id: inventoryId}});
        }

        function cancelInventory(inventory) {
            return warehouseApi.post(inventoryApi.cancelInventory, {inventoryInfo: inventory});
        }

        function finishInventory(inventory) {
            return warehouseApi.post(inventoryApi.finishInventory, {inventoryInfo: inventory});
        }
    }

})();