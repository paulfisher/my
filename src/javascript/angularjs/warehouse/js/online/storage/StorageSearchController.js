;(function() {
    angular.module('warehouseOnline')
        .controller('storageSearchController', StorageSearchController);

    StorageSearchController.$inject = ['$scope', '$location', 'storageService', '$routeParams'];
    function StorageSearchController(scope, $location, storageService, $routeParams) {
        var vm = scope;
        vm.search = search;
        vm.show = show;

        vm.storages = [];

        function search() {
            storageService.findStorages({})
                .success(function(findStoragesResult) {
                    vm.storages = findStoragesResult.data.storages;
                });
        }

        function show(storage) {
            $location.path('/storage/' + storage.id);
        }
    }
})();