;(function () {

    angular.module('warehouseOnline')
        .constant('STORAGE_API',
        {
            findStorages: '/online/storage/find',
            getStorage: '/online/storage/get'
        })
        .factory('storageService', StorageService);

    StorageService.$inject = ['STORAGE_API', 'warehouseApi'];
    function StorageService(storageApi, warehouseApi) {
        return {
            findStorages: findStorages,
            getStorage: getStorage
        };

        function findStorages(storageInfo) {
            return warehouseApi.post(storageApi.findStorages, {info: storageInfo});
        }

        function getStorage(storageId) {
            return warehouseApi.post(storageApi.getStorage, {info: {id: storageId}});
        }
    }

})();