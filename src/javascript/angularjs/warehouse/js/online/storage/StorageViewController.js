;(function() {
    angular.module('warehouseOnline')
        .controller('storageViewController', StorageViewController);

    StorageViewController.$inject = ['$scope', '$location', 'storageService', '$routeParams'];
    function StorageViewController(scope, $location, storageService, $routeParams) {
        var vm = scope;
        vm.init = init;

        vm.storage = null;

        function init() {
            storageService.getStorage($routeParams.storageId)
                .success(function(response) {
                    vm.storage = response.data;
                });
        }
    }
})();