;(function() {
    angular.module('warehouseOnline')
        .controller('tagViewController', TagViewController);

    TagViewController.$inject = ['$scope', 'tagService', 'transferService', '$routeParams', '$route', 'TRANSFER_DIRECTION'];
    function TagViewController(scope, tagService, transferService, $routeParams, route, TRANSFER_DIRECTION) {
        var vm = scope;

        vm.init = init;
        vm.storageOut = storageOut;
        vm.storageIn = storageIn;

        vm.tag = null;
        vm.inStorageId = null;

        function init() {
            tagService.getTag($routeParams.tagId)
                .success(function(response) {
                    vm.tag = response.data;
                });
        }

        function storageOut() {
            if (confirm("Вы уверены, что хотите вывести товар со склада?")) {
                transferService.registerTransfer(vm.tag, TRANSFER_DIRECTION.out, vm.tag.storageId)
                    .success(function() {
                        route.reload();
                    });
            }
        }

        function storageIn() {
            if (!vm.inStorageId) {
                alert("Выберите склад");
                return;
            }
            if (confirm("Вы уверены, что хотите поместить товар на склад?")) {
                transferService.registerTransfer(vm.tag, TRANSFER_DIRECTION.in, vm.inStorageId)
                    .success(function() {
                        route.reload();
                    });
            }
        }
    }
})();