;(function () {

    angular.module('warehouseOnline')
        .constant('TAG_API',
        {
            findTags: '/online/tag/find',
            getTag: '/online/tag/get'
        })
        .factory('tagService', TagService);

    TagService.$inject = ['TAG_API', 'warehouseApi'];
    function TagService(tagApi, warehouseApi) {
        return {
            findTags: findTags,
            getTag: getTag
        };

        function findTags(tagInfo) {
            return warehouseApi.post(tagApi.findTags, {tagInfo: tagInfo});
        }

        function getTag(tagId) {
            return warehouseApi.post(tagApi.getTag, {tagInfo: {id: tagId}});
        }
    }

})();