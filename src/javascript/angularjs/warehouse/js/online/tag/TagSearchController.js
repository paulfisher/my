;(function() {
    angular.module('warehouseOnline')
        .controller('tagSearchController', TagSearchController);

    TagSearchController.$inject = ['$scope', '$location', 'tagService', '$routeParams'];
    function TagSearchController(scope, $location, tagService, $routeParams) {
        var vm = scope;

        vm.search = search;
        vm.show = show;

        vm.tags = [];

        function search() {
            tagService.findTags({})
                .success(function(findTagsResult) {
                    vm.tags = findTagsResult.data.tags;
                });
        }

        function show(tag) {
            $location.path('/tag/' + tag.id);
        }
    }
})();