;(function() {

    angular.module('warehouseOnline')
        .controller('adminController', AdminController);

    AdminController.$inject = ['$scope', '$location', 'companyService', '$rootScope'];
    function AdminController(scope, $location, companyService, $rootScope) {

        var vm = scope;
        vm.init = init;

        function init() {
            companyService.getMyCompanyStats()
                .success(function(getCompanyStatsResult) {
                    vm.companyStats = getCompanyStatsResult.data;
                });
        }

    }

})();