<?php

require_once 'api/RfIdControlApi.php';
require_once 'handlers/ProductPriceImport.php';
require_once 'handlers/ProductQuantityImport.php';
require_once 'handlers/ProductTreeImport.php';
require_once 'handlers/StockListImport.php';

$handler = new ImportFileHandler(new RfIdControlApi());
$handler->run();

class ImportFileHandler {

    /**
     * @var RfIdControlApi
     */
    private $_api;

    private $productPricePattern = '/.*_product_price_.*\.xml/';
    private $productQuantityPattern = '/.*_product_quantity_.*\.xml/';
    private $productTreePattern = '/.*_product_tree_.*\.xml/';
    private $stockListPattern = '/.*_stock_list_.*\.xml/';

    private $dirFresh;
    private $dirOld;

    public function __construct($api) {
        $this->_api = $api;
        $this->dirFresh = dirname(__FILE__) . DIRECTORY_SEPARATOR . '_data';
        $this->dirOld = dirname(__FILE__) . DIRECTORY_SEPARATOR . '_old';
    }

    public function run() {

        $files = scandir($this->dirFresh, SCANDIR_SORT_NONE);
        $sortedByCreatingTime = [];

        foreach ($files as $file) {
            if ($file == "." || $file == "..") continue;
            $sortedByCreatingTime[] = $file;
        }
        usort($sortedByCreatingTime, function($left, $right) {
            return filectime($this->getAbsolutePath($left)) < filectime($this->getAbsolutePath($right));
        });
        foreach ($sortedByCreatingTime as $file) {
            $this->handleFile($file);
        }

        /*
        1. захожу в нужную папку
        2. получаю list всех файлов
        список файлов должен быть отсортирован по дате создания. начиная со старых, и заканчивая новыми
        3. для каждого файла:
        3.1 определяю тип, id компании
        3.2 запускаю определенный SomeApiImport..
        3.3 переношу файл в папку /old
        */
    }

    private function handleFile($file) {
        if ($this->isProductPriceFile($file)) {
            $this->handleProductPriceFile($file);
        } else if ($this->isProductQuantityFile($file)) {
            $this->handleProductQuantityFile($file);
        } else if ($this->isProductTreeFile($file)) {
            $this->handleProductTreeFile($file);
        } else if ($this->isStockListFile($file)) {
            $this->handleStockListFile($file);
        } else {
            return;
        }
        $this->onFileProcessed($file);
    }

    private function isProductPriceFile($file) {
        return preg_match($this->productPricePattern, $file) == 1;
    }

    private function isProductQuantityFile($file) {
        return preg_match($this->productQuantityPattern, $file) == 1;
    }

    private function isProductTreeFile($file) {
        return preg_match($this->productTreePattern, $file) == 1;
    }

    private function isStockListFile($file) {
        return preg_match($this->stockListPattern, $file) == 1;
    }

    private function handleProductPriceFile($file) {
        $import = new ProductPriceImport();
        $import->parse($this->getAbsolutePath($file), function($companyRef, $price, $productCategoryRef) {
            $this->_api->updatePrice($companyRef, $price, $productCategoryRef);
        });
    }

    private function handleProductQuantityFile($file) {
        $import = new ProductQuantityImport();
        $import->parse($this->getAbsolutePath($file), function($companyRef, $stockRef, $productCategoryRef, $quantity) {
            $this->_api->updateQuantity($companyRef, $stockRef, $productCategoryRef, $quantity);
        });
    }

    private function handleProductTreeFile($file) {
        $import = new ProductTreeImport();
        $import->parse($this->getAbsolutePath($file), function($companyRef, $productCategory) {
            $this->_api->updateProductCategory($companyRef, $productCategory);
        });
    }

    private function handleStockListFile($file) {
        $import = new StockListImport();
        $import->parse($this->getAbsolutePath($file), function($companyRef, $stockRef, $stockName) {
            $this->_api->updateStock($companyRef, $stockRef, $stockName);
        });
    }

    private function onFileProcessed($file) {
        $path = $this->getAbsolutePath($file);
        $oldDirPath = $this->getOldDirPath($file);
        //echo "moving to " . $oldDirPath . "\n";
        rename($path, $oldDirPath);
    }

    private function getAbsolutePath($file) {
        return $this->dirFresh . DIRECTORY_SEPARATOR . $file;
    }

    private function getOldDirPath($file) {
        return $this->dirOld . DIRECTORY_SEPARATOR . $file;
    }
}