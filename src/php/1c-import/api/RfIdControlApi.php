<?php

class RfIdControlApi {

    private $apiBase = 'http://api.rfidcontrol.loc';
    private $updatePriceMethod = '/connect/c1/price/save';
    private $updateQuantityMethod = '/connect/c1/quantity/save';
    private $updateProductCategoryMethod = '/connect/c1/category/save';
    private $updateStockMethod = '/connect/c1/storage/save';

    public function updateStock($companyRef, $stockRef, $stockName) {
        $url = $this->apiBase . $this->updateStockMethod;
        $this->call($url, [
            'companyRef' => $companyRef,
            'stockRef' => $stockRef,
            'stockName' => $stockName
        ]);
    }

    public function updatePrice($companyRef, $price, $productCategoryRef) {
        $url = $this->apiBase . $this->updatePriceMethod;
        $this->call($url, [
            'companyRef' => $companyRef,
            'productCategoryRef' => $productCategoryRef,
            'priceInfo' => [
                'priceInfo' => $price
            ]
        ]);
    }

    public function updateQuantity($companyRef, $stockRef, $productCategoryRef, $quantity) {
        $url = $this->apiBase . $this->updateQuantityMethod;
        $this->call($url, [
            'companyRef' => $companyRef,
            'stockRef' => $stockRef,
            'productCategoryRef' => $productCategoryRef,
            'quantity' => $quantity
        ]);
    }

    public function updateProductCategory($companyRef, $productCategory) {
        $url = $this->apiBase . $this->updateProductCategoryMethod;
        $this->call($url, [
            'companyRef' => $companyRef,
            'productCategoryInfo' => [
                'name' => $productCategory['name'],
                'reference' => $productCategory['ref'],
                'article' => $productCategory['article'],
                'parent' => [
                    'reference' => $productCategory['parentRef']
                ],
                'sizer' => [
                    'okeiCode' => $productCategory['okei']
                ]
            ],
        ]);
    }

    private function call($url, $data) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-type: application/json", "rfidcontrol-external-app: rXfB0UY7cxW5lxALh4vS"]);

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 200) {
            echo ("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }
        curl_close($curl);
        //$response = @json_decode($json_response, true);
    }
}