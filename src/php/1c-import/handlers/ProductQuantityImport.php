<?php

class ProductQuantityImport {

    public function parse($path, $callback) {
        /** @var SimpleXMLElement $xml */
        $xml = $this->loadXml($path);
        if (!$xml) {
            echo "fail";
            die();
        }

        $companyRef = (string)$xml->attributes()->company;
        $stocks = $xml->children();
        foreach ($stocks as $stock) {
            $stockRef = (string)$stock->attributes()->id;
            $products = $stock->children();
            foreach ($products as $product) {
                $productCategoryRef = (string)$product->attributes()->id;
                $quantity = (integer)$product->attributes()->quantity;
                $callback($companyRef, $stockRef, $productCategoryRef, $quantity);
            }
        }
    }

    private function loadXml($path) {
        libxml_use_internal_errors(true);
        return simplexml_load_file($path);
    }
}