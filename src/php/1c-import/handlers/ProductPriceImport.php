<?php

class ProductPriceImport {

    public function parse($path, $callback) {
        /** @var SimpleXMLElement $xml */
        $xml = $this->loadXml($path);
        if (!$xml) {
            echo "fail";
            die();
        }

        $companyRef = (string)$xml->attributes()->company;
        $items = $xml->prices->children();

        foreach ($items as $item) {
            /** @var SimpleXMLElement $item */
            $price = (float)$item->attributes()->price;
            $productCategoryRef = (string)$item->attributes()->id;
            //$this->_api->updatePrice($companyRef, $price, $productCategoryRef);
            $callback($companyRef, $price, $productCategoryRef);
        }
    }

    private function loadXml($path) {
        libxml_use_internal_errors(true);
        return simplexml_load_file($path);
    }
}