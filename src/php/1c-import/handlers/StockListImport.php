<?php

class StockListImport {

    public function parse($path, $callback) {
        /** @var SimpleXMLElement $xml */
        $xml = $this->loadXml($path);
        if (!$xml) {
            echo "fail";
            die();
        }

        $companyRef = (string)$xml->attributes()->company;
        $items = $xml->stocks->children();

        foreach ($items as $item) {
            /** @var SimpleXMLElement $item */
            $name = (string)$item->attributes()->name;
            $stockRef = (string)$item->attributes()->id;
            $callback($companyRef, $stockRef, $name);
        }
    }

    private function loadXml($path) {
        libxml_use_internal_errors(true);
        return simplexml_load_file($path);
    }
}