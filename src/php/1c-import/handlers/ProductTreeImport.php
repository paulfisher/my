<?php

class ProductTreeImport {

    public function parse($path, $callback) {
        /** @var SimpleXMLElement $xml */
        $xml = $this->loadXml($path);
        if (!$xml) {
            echo "fail";
            die();
        }

        $companyRef = (string)$xml->attributes()->company;
        $products = $xml->products->children();

        foreach ($products as $product) {
            /** @var SimpleXMLElement $product */
            $productCategoryRef = (string)$product->attributes()->id;
            $name = (string)$product->attributes()->name;
            $article = (string)$product->attributes()->article;
            $okei = (integer)$product->attributes()->okei;
            $nds = (string)$product->attributes()->nds;
            $measure = (string)$product->attributes()->measure;
            $parentProductCategoryRef = (string)$product->attributes()->parent_id;

            $callback($companyRef, [
                'ref' => $productCategoryRef,
                'name' => $name,
                'article' => $article,
                'okei' => $okei,
                'nds' => $nds,
                'measure' => $measure,
                'parentRef' => $parentProductCategoryRef
            ]);
        }
    }

    private function loadXml($path) {
        libxml_use_internal_errors(true);
        return simplexml_load_file($path);
    }
}