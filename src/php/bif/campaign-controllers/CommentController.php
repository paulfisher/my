<?php

class CommentController extends BaseCampaignController {

    private $_model;

    public function actionIndex() {
        $this->loadModel();

        $this->scriptEnd('/protected/modules/campaign/assets/backCampaignController.js')
            ->scriptEnd('/protected/modules/campaign/assets/calculatePledgeDirective.js')
            ->scriptEnd('/protected/modules/campaign/assets/CommentController.js')
            ->scriptEnd('/protected/modules/campaign/assets/CommentService.js')
            ->render('comments', [
                'isCommentAvailable' => $this->account() && $this->account()->user->isCommentAvailable(),
                'model' => $this->_model,
            ]);
	}

    public function actionGet() {
        $req = Yii::app()->request;
        $offset = $req->getParam('offset', null);
        $count = $req->getParam('count', null);

        $comments = Yii::app()->comments()->getComments($this->loadModel(), $offset, $count);
        $result = array();
        foreach ($comments as $comment) {
            $result[] = $comment->info();
        }
        echo CJSON::encode($result);
        Yii::app()->end();
    }

    public function actionCreate() {
        $req = Yii::app()->request;
        $campaignId = $req->getParam('campaignId', null);
        if (is_null($campaignId)) {
            throw $this->notFound('Error: parameter campaignId missing');
        }
        $text = $req->getParam('text', null);
        if (is_null($text)) {
            throw $this->notFound('Error: parameter text missing');
        }

        /** @var Campaign $campaign */
        $campaign = Yii::app()->campaignRepo()->get($campaignId);
        $author = Yii::app()->getUser()->model();
        $comment = Yii::app()->comments()->create($campaign, $author, $text);
        $comment->refresh();
        Yii::app()->bifStatService()->campaign($campaign);

        echo CJSON::encode($comment->info());
        Yii::app()->end();
    }

    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = $this->getModel();
        }
        return $this->_model;
    }
}