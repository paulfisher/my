<?php

class CreateController extends BaseCampaignController {

    const STEP_BASICS = 'basics';
    const STEP_STORY = 'story';
    const STEP_PROFILE = 'profile';
    const STEP_PAYOUT = 'payout';

    public function filters() {
        return CMap::mergeArray(parent::filters(), ['accessControl']);
    }

    public function actionBasics() {
        $campaignBasics = new CampaignBasicsForm();
        if ($this->account()->tree) {
            $campaignBasics->maximumGoalSum = $this->account()->tree->max_goal;
        }

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'campaign-basics-form') {
            echo CActiveForm::validate($campaignBasics);
            Yii::app()->end();
        }

        $image = null;
        $requestParams = isset($_POST['CampaignBasicsForm']) ? $_POST['CampaignBasicsForm'] : null;
        if ($requestParams) {
            $campaignBasics->attributes = $requestParams;
            if ($campaignBasics->validate()) {
                Yii::app()->getUser()->model()->saveCampaignWizardData(self::STEP_BASICS, $campaignBasics->getAttributes());
                $this->redirect('story');
            }
        } else {
            $campaignBasics->setAttributes($this->load('basics'));
            $image = Yii::app()->imageRepo()->getById(intval($campaignBasics->imageId, 10));
        }

        $this->render('basics', [
            'categories' => Category::getAll(),
            'campaignBasicsForm' => $campaignBasics,
            'image' => $image,
        ]);
    }

    public function actionStoryBack() {
        $attributes = isset($_POST['CampaignStoryForm']) ? $_POST['CampaignStoryForm'] : [];
        Yii::app()->getUser()->model()->saveCampaignWizardData(self::STEP_STORY, $attributes);
        $this->redirect('basics');
    }

    public function actionStory() {
        $this->checkStepDone(self::STEP_BASICS);
        $campaignStoryForm = new CampaignStoryForm();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'campaign-story-form') {
            echo CActiveForm::validate($campaignStoryForm);
            Yii::app()->end();
        }

        $requestParams = isset($_POST['CampaignStoryForm']) ? $_POST['CampaignStoryForm'] : null;
        if ($requestParams) {
            $campaignStoryForm->attributes = $requestParams;
            if ($campaignStoryForm->validate()) {
                Yii::app()->getUser()->model()->saveCampaignWizardData(self::STEP_STORY, $campaignStoryForm->getAttributes());
                $this->redirect('profile');
            }
        } else {
            $campaignStoryForm->setAttributes($this->load('story'));
        }

        $this->render('story', [
            'campaignStoryForm' => $campaignStoryForm,
        ]);
    }

    public function actionProfileBack() {
        $attributes = isset($_POST['ProfileForm']) ? $_POST['ProfileForm'] : [];
        Yii::app()->getUser()->model()->saveCampaignWizardData(self::STEP_PROFILE, $attributes);
        $this->redirect('story');
    }

    public function actionProfile() {
        $this->checkStepDone(self::STEP_STORY, 'campaign/create/story');
        $profileForm = new ProfileForm();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'profile-form') {
            echo CActiveForm::validate($profileForm);
            Yii::app()->end();
        }

        /** @var User $user */
        $user = $this->account()->user;
        /** @var Profile $profile */
        $profile = $user->profile;

        $requestParams = isset($_POST['ProfileForm']) ? $_POST['ProfileForm'] : null;
        if ($requestParams) {
            $profileForm->attributes = $requestParams;
            if ($profileForm->validate()) {
                /*
                $oldProfileUri = $profile->uri;

                $profile->attributes = [
                    'uri' => $requestParams['username'],
                    'full_name' => $requestParams['fullName'],
                    'location' => $requestParams['location'],
                    'biography' => $requestParams['biography']
                ];
                $valid = $profile->validate();
                $uriIsValid = $oldProfileUri == $profile->uri || $profile->isOldUriChangeable($oldProfileUri);
                if ($valid && $uriIsValid) {
                    $profile->save();
                    $user->account->name = $profile->full_name;
                    $user->account->save();
                }
                */

                Yii::app()->getUser()->model()->saveCampaignWizardData(self::STEP_PROFILE, $profileForm->getAttributes());
                $this->redirect('payout');
            }
        } else {
            $profileData = $this->load(self::STEP_PROFILE);
            if (!empty($profileData)) {
                $profileForm->setAttributes($profileData);
            } else {
                $profileForm->fullName = $profile->full_name;
                $profileForm->username = $profile->uri;
                $profileForm->email = $user->email;
                $profileForm->location = $profile->location;
                $profileForm->biography = $profile->biography;
            }
        }
        if(!isset($user)) $user = $this->account()->user;

        $this->render('profile', [
            'user' => $user,
            'profileForm' => $profileForm,
        ]);
    }

    public function actionPayout() {
        $this->checkStepDone(self::STEP_PROFILE, 'campaign/create/profile');
        Yii::app()->getUser()->model()->saveCampaignWizardData(self::STEP_PAYOUT);
        $this->render('payout');
    }

    public function actionComplete() {
        $this->checkStepDone(self::STEP_PAYOUT, 'campaign/create/payout');

        $user = $this->account()->user;
        $profile = $user->profile;

        $profileData = $this->load(self::STEP_PROFILE);
        $basics = $this->load(self::STEP_BASICS);
        $story = $this->load(self::STEP_STORY);

        $oldProfileUri = $profile->uri;
        $profile->attributes = [
            'uri' => $profileData['username'],
            'full_name' => $profileData['fullName'],
            'location' => $profileData['location'],
            'biography' => $profileData['biography']
        ];
        $valid = $profile->validate();
        $uriIsValid = $oldProfileUri == $profile->uri || $profile->isOldUriChangeable($oldProfileUri);
        if ($valid && $uriIsValid) {
            $profile->save();
            $user->account->name = $profile->full_name;
            $user->account->save();
        }

        $model = (new Campaign('create_scenario'))
            ->populateAttributes($basics)
            ->populateAttributes($story);
        $model->account_id = $this->account()->id;
        $model->current_sum = 0;
        $model->status = CampaignStatus::DRAFT;
        $model->karma_bonus = 0;
        $model->create_time = ServerTime::nowDate();
        $model->end_time = ServerTime::addDaysDate(14);
        $model->slug = StringHelper::slugIt($model->name);

        if ($model->validate()) {
            if ($model->save()) {
                Yii::app()->onCampaignEvent(new CampaignEvent($model, CampaignEvent::CREATED));
                $video = isset($story['video']) ? $story['video'] : '';
                $this->saveCampaignVideo($model, $video);

                Yii::app()->onCampaignEvent(new CampaignEvent($model, CampaignEvent::EDITED));

                Yii::app()->getUser()->user()->clearCampaignWizardData();

                return $this->render('complete', ['campaign' => $model]);
            }
        }
        $this->render('complete');
    }

    private function saveCampaignVideo(Campaign $campaign, $videoUrl) {
        if (empty($videoUrl)) {
            $campaignVideo = $campaign->getVideo();
            if ($campaignVideo) {
                $campaignVideo->delete();
            }
        } else {
            $video = new CampaignVideo();
            $video->campaign_id = $campaign->id;
            $video->name = $campaign->name . ' video';
            $video->source_type = VideoHelper::getVideoSourceType($videoUrl);
            $video->source = VideoHelper::getVideoId($videoUrl, $video->source_type);
            $video->url = $videoUrl;
            $video->save();
        }
    }

    public function actionPreloadVideo() {
        $videoUrl = Yii::app()->getRequest()->getParam('url');
        $sourceType = VideoHelper::getVideoSourceType($videoUrl);

        $result = ['url' => $videoUrl];
        if ($sourceType === false) {
            $result['error'] = 'Invalid video source';
        } else {
            $source = VideoHelper::getVideoId($videoUrl, $sourceType);
            if ($source) {
                $result['sourceType'] = $sourceType;
                $result['source'] = $source;
                $result['output'] = $this->widget('application.components.widgets.VideoWidget', ['type' => $sourceType, 'src' => $source], true);
            } else {
                $result['error'] = 'Invalid video url';
            }
        }

        echo json_encode($result);
        Yii::app()->end();
    }

    private function load($key) {
        return Yii::app()->getUser()->user()->loadCampaignWizardData($key);
    }

    private function checkStepDone($key, $redirectOnFailTo = 'campaign/create/basics') {
        if (Yii::app()->getUser()->user()->loadCampaignWizardData($key)) {
            return true;
        } else {
            Yii::app()->getRequest()->redirect(Yii::app()->createUrl($redirectOnFailTo));
            return false;
        }
    }
}