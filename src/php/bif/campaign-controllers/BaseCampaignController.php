<?php

abstract class BaseCampaignController extends BaseController {


    /**
     * @return Campaign|mixed|null
     * @throws CHttpException
     */
    public function getModel() {
        $model = null;
        if (isset($_GET['slugOrId'])) {
            $slugOrId = $_GET['slugOrId'];
            if (isset($_GET['owner'])) {
                $owner = $_GET['owner'];

                /** @var Account $account */
                $account = Yii::app()->accountRepo()->find(intval($owner));//1. интерпретируем $owner как account_id
                if (!$account) {
                    $user = Yii::app()->userService()->getByUri($owner);//2. интерпретируем $owner как profile->uri
                    if (!$user) {
                        $user = User::model()->findByPk(intval($owner));//3. интерпретируем $owner как user_id
                    }
                    if ($user) {
                        $account = $user->account;
                    }
                }
                if ($account) {
                    $model = Yii::app()->campaignRepo()->findBySlugOrId($account, $slugOrId);
                }
            }
        }

        if (isset($_GET['id'])) {
            $model = Campaign::model()->findbyPk($_GET['id']);
        }

        if ($model === null) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }


}