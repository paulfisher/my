<?php


class DefaultController extends BaseCampaignController {

    private $_model;

    public function filters() {
        return CMap::mergeArray(parent::filters(),array(
            'accessControl', // perform access control for CRUD operations
        ));
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('create', 'backed'),
                'users'=>array('@'),
            ),
            array('deny',
                'actions'=>array('create', 'backed'),
                'users'=>array('*'),
            ),
        );
    }

    public function actionView() {
        /** @var Campaign $campaign */
        $campaign = $this->loadModel();

        /** @var User $self */
        $self = Yii::app()->user->model();

        if ($self && !Yii::app()->user->isGuest && $campaign->account_id === $self->account_id && $campaign->status == CampaignStatus::FINISHED) {
            $this->actionFinish();
            Yii::app()->end();
        }
        $this->pageTitle = $campaign->name;
        $this->pageDescription = $campaign->pitch;
        $this->registerMetaProperty('og:type', Yii::app()->eauth->services['facebook']['object_campaign']);
        if ($campaign->getImage() !== NULL) {
            /**
             * facebook:
             * Use images that are at least *1200 x 630 * pixels for the best display on high resolution devices.
             * At the minimum, you should use images that are *600 x 315 *pixels to display link page posts with larger images.
             * We've also redesigned link page posts so that the aspect ratio for images is the same across desktop and mobile News Feed.
             * Try to keep your images as close to 1*.91:1 aspect ratio* as possible to display the full image in News Feed without any cropping.
             * The minimum image size is 200 x 200 pixels.
             */
            $ogImage = null;
            if ($campaign->image) {
                if ($campaign->image->width >= 1200) {
                    $ogImage = $campaign->getImage(1200);
                } else if ($campaign->image->width >= 600) {
                    $ogImage = $campaign->getImage(600);
                } else {
                    $ogImage = $campaign->getImage(200);
                }
            } else {
                $ogImage = $campaign->getImage();
            }
            if ($ogImage) {
                $this->ogImage = $ogImage->src;
            }
        }
        $this->script('/protected/modules/campaign/assets/backCampaignController.js', CClientScript::POS_END);
        $this->script('/protected/modules/campaign/assets/calculatePledgeDirective.js', CClientScript::POS_END);
        $this->render('view', array(
            'campaign' => $campaign,
            'status' => Yii::app()->request->getParam('status'),
        ));
    }

    public function actionBackers() {
        $model = $this->loadModel();
        $backersDataProvider = Yii::app()->campaignRepo()->getBackersDataProvider($model->id);
        $this->pageTitle = $model->name;
        $this->pageDescription = $model->pitch;
        $this->registerMetaProperty('og:type', Yii::app()->eauth->services['facebook']['object_campaign']);
        if ($model->getImage() !== NULL) {
            $this->ogImage = $model->getImage();
        }
        $this->script('/protected/modules/campaign/assets/backCampaignController.js', CClientScript::POS_END);
        $this->script('/protected/modules/campaign/assets/calculatePledgeDirective.js', CClientScript::POS_END);
        $this->render('backers',
            array(
                'model' => $model,
                'backers' => $backersDataProvider,
                'pages' => $backersDataProvider->pagination,
            ));
    }


    public function actionFinish() {
        $campaign = $this->loadModel();
        if (!$campaign->mine()) {
            throw new CHttpException(404);
        }
        $this->pageTitle = $campaign->name;
        $this->pageDescription = $campaign->pitch;
        $this->registerMetaProperty('og:type', Yii::app()->eauth->services['facebook']['object_campaign']);
        $this->render('finish', array(
            'campaign' => $campaign,
        ));
    }

    public function actionPledge() {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->back();
        } else {
            $campaign = $this->loadModel();
            if (!$campaign->isBackAvailable()) {
                throw $this->notFound("Error: campaign back not available");
            }
            $pledgeValue = Yii::app()->getUser()->getPledgeValue(true);
            $requestedPledgeValue = Yii::app()->getRequest()->getParam('value');
            if (!$pledgeValue && $requestedPledgeValue) {
                $pledgeValue = $requestedPledgeValue;
            }
            $this->pageTitle = Yii::t('main', 'Pledge to') . ' ' . $campaign->name;

            $this->script('/protected/modules/campaign/assets/backCampaignController.js', CClientScript::POS_END);
            $this->script('/protected/modules/campaign/assets/calculatePledgeDirective.js', CClientScript::POS_END);

            $this->render('pledge', array(
                'model' => $campaign,
                'pledgeValue' => $pledgeValue,
                'mode' => 'brief'
            ));
        }
    }

    /**
     * @throws CDbException
     * @throws CHttpException
     */
    public function back() {
        Yii::app()->user->unloadPayment();

        $req = Yii::app()->request;

        $value = intval($req->getParam('pledgeval'));
        if ($value <= 0) {
            throw $this->notFound('Error: wrong value');
        }

        $campaignId = intval($req->getParam('campaign_id'));
        if ($campaignId <= 0) {
            throw $this->notFound('Error: empty campaign param');
        }

        /** @var Campaign $campaign */
        $campaign = Yii::app()->campaignRepo()->get($campaignId);

        if (!$campaign->isBackAvailable()) {
            throw $this->notFound("Error: campaign back not available");
        }
        if ($campaign->tree->min_pledge > $value) {
            throw $this->notAllowed("Error: minimum pledge value is " . $campaign->tree->min_pledge);
        }

        $account = $this->account();
        if (Yii::app()->getUser()->getIsGuest()) {
            $email = trim($req->getParam('userEmail'));
            if (!$email) {
                throw $this->notFound();
            }
            //1.  first, try to find such user
            //2.  if user exists, login is required
            //2.1 if no such user, create account
            $user = Yii::app()->userService()->findByEmail($email);
            if ($user) {
                Yii::app()->getUser()->setEmailToLogin($email)->setPledgeValue($value)->loginRequired();
            } else {
                $account = Yii::app()->userService()->createAccountForCampaignBack($email);
                $user = $account->user;
                $user->notsafe();
                $user->refresh();

                //в template-е userRegistered зашита ссылка на активацию пользователя
                Yii::app()->notify(NotificationEvent::userRegistered($user, $user->activkey));

                $identity = new UserIdentity($email, '');
                $identity->setState('id', $user->id);
                $identity->setState('__id', $user->id);
                $identity->setState('name', $user->username);

                Yii::app()->getUser()->login($identity, 2592000);// 2592000 = 3600 * 24 * 30 = 30 days
                $user->lastvisit = time();
                $user->save();
            }
        }

        $payment = Yii::app()->paymentService()->createPayment($account, $campaign, $value);
        Yii::app()->getUser()->setPayment($payment->id);

        $this->redirect($this->createUrl('/payment/confirm'));
    }

    public function actionCreate() {
        if (!$this->account()->createCampaignAvailable()) {
            $this->render('create-campaign-unavailable', [
                'account' => $this->account()
            ]);
            return;
        }
        if (!Yii::app()->getUser()->user()->loadCampaignWizardData(CreateController::STEP_BASICS)) {
            $this->redirect('/campaign/create/basics');
        }
        if (!Yii::app()->getUser()->user()->loadCampaignWizardData(CreateController::STEP_STORY)) {
            $this->redirect('/campaign/create/story');
        }
        if (!Yii::app()->getUser()->user()->loadCampaignWizardData(CreateController::STEP_PROFILE)) {
            $this->redirect('/campaign/create/profile');
        }
        if (!Yii::app()->getUser()->user()->loadCampaignWizardData(CreateController::STEP_PAYOUT)) {
            $this->redirect('/campaign/create/payout');
        }

        $this->redirect('/campaign/create/basics');
    }

    public function actionStop() {
        $model = $this->loadModel();
        if (!$model->isEditable()) {
            throw new CHttpException(404, "Error: campaign not found");
        }
        if (!$model->in_active_zone) {
            $model->status = CampaignStatus::STOPPED;
            $model->save();
        }
        $this->redirect($this->createUrl('edit', array('id' => $model->id)));
    }

    public function actionCancel() {
        $model = $this->loadModel();
        if (!$model->isEditable()) {
            throw new CHttpException(404, "Error: campaign not found");
        }
        $model->cancel();
        $this->redirect($model->publicUrl());
    }

    public function actionContinue() {
        $model = $this->loadModel();
        if (!$model->isEditable()) {
            throw new CHttpException(404, "Error: campaign not found");
        }
        if (!$model->in_active_zone && $model->status == CampaignStatus::STOPPED) {
            $model->status = CampaignStatus::APPROVED;
            $model->save();
        }
        $this->redirect($this->createUrl('edit', array('id' => $model->id)));
    }

    public function actionExitTree() {
        $model = $this->loadModel();
        if (!$model->isEditable()) {
            throw new CHttpException(404, "Error: campaign not found");
        }
        $model->finishSafe();
        $this->redirect($model->publicUrl());
    }

    public function actionEdit() {
        /** @var Campaign $model */
        $model = $this->loadModel();
        if (!$model->isEditable()) {
            throw new CHttpException(403, "Error: campaign not found");
        }

        if (isset($_POST['ajax']) && $_POST['ajax']==='campaign-create-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Campaign'])) {
            $postData = $_POST['Campaign'];
            $model->attributes = $postData;
            if ($model->validate()) {
                if ($model->save()) {
                    $videoUrl = isset($postData['videoUrl']) ? $postData['videoUrl'] : '';
                    $this->saveCampaignVideo($model, $videoUrl);
                    Yii::app()->onCampaignEvent(new CampaignEvent($model, CampaignEvent::EDITED));

                    $this->redirect($model->publicUrl());
                }
            }
        }

        $this->render('edit', array(
            'categories' => Category::getAll(),
            'model' => $model,
        ));
    }

    public function actionModeration() {
        $this->isForModerators();

        $campaignsCount = Yii::app()->campaignRepo()->getForModerationDataProvider()->getTotalItemCount();
        $this->render('moderation', compact('campaignsCount'));
    }

    public function actionModerate() {
        if (!$this->account()->user->isActiveStatus()) {
            throw $this->notAllowed("activate account");
        }
        $this->isForModerators();

        $req = Yii::app()->getRequest();
        $id = $req->getParam('id');

        /** @var Campaign $campaign */
        $campaign = Yii::app()->campaignRepo()->get($id);
        if (!$campaign->isOnModeration()) {
            throw $this->notFound();
        }

        $action = $req->getParam('action');
        $reason = $req->getParam('reason');
        switch($action) {
            case 'ban':
                $this->isForAdmins();
                $campaign->ban($reason);
                break;
            case 'approve':
                $campaign->approve();
                break;
            case 'reject':
                $campaign->reject($reason);
                break;
        }

        $this->redirect($campaign->publicUrl());
    }

    public function actionApplyModeration() {
        $req = Yii::app()->getRequest();
        $id = $req->getParam('id');
        /** @var Campaign $campaign */
        $campaign = Yii::app()->campaignRepo()->get($id);

        if (!$campaign->mine()) {
            throw $this->notFound();
        }

        $campaign->moderation();

        $this->redirect($campaign->publicUrl());
    }


    public function actionSearch() {
        $q = Yii::app()->getRequest()->getParam('q');
        $result = [];
        if (empty($q)) {
            echo json_encode($result);
            Yii::app()->end();
        }

        /** @var CActiveDataProvider $campaigns */
        $campaigns = Yii::app()->campaignRepo()->searchCampaigns($q);
        foreach ($campaigns->getData() as $campaign) {
            /** @var Campaign $campaign */
            $result[] = [
                'id' => $campaign->id,
                'name' => $campaign->name,
                'image' => $campaign->image ? $campaign->getImage()->src : ''
            ];
        }
        echo json_encode($result);
        Yii::app()->end();
    }

    /**
     * @return Campaign|mixed|null
     * @throws CHttpException
     */
    public function loadModel() {
        if($this->_model===null) {
            $this->_model = $this->getModel();
        }
        return $this->_model;
    }

    private function saveCampaignVideo(Campaign $campaign, $videoUrl) {
        if (empty($videoUrl)) {
            $campaignVideo = $campaign->getVideo();
            if ($campaignVideo) {
                $campaignVideo->delete();
            }
        } else {
            $video = new CampaignVideo();
            $video->campaign_id = $campaign->id;
            $video->name = $campaign->name . ' video';
            $video->source_type = VideoHelper::getVideoSourceType($videoUrl);
            $video->source = VideoHelper::getVideoId($videoUrl, $video->source_type);
            $video->url = $videoUrl;
            $video->save();
        }
    }
}