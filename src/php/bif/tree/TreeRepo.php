<?php

class TreeRepo extends BaseRepo {

    public function create($name) {
        $tree = new Tree();

        $tree->name = $name;
        $tree->create_time = ServerTime::nowDate();
        $tree->last_change_time = ServerTime::nowDate();
        $tree->active_zone_level = 3;
        $tree->money_flow = 20;
        $tree->money_flow_type = MoneyFlowType::Flat;
        $tree->karma_flow = 20;
        $tree->karma_flow_type = KarmaFlowType::Flat;
        $tree->karma_goal_multi = 1;
        $tree->minimum_backs = 0;
        $tree->minimum_karma = 10;
        $tree->karma_bonus_rate = 1;
        $tree->stillness_limit = 10;
        $tree->max_goal = 20000;
        $tree->max_inactive_time = 10;

        return $tree;
    }

    public function getCodesForLevel(Tree $tree, $level) {
        return $this->cmd(TreeNode::model()->tableName())
            ->select('code')
            ->where('tree_id = :tree_id and level = :level',
                array(
                    ':tree_id' => $tree->id,
                    ':level' => $level,
                ))
            ->queryColumn();
    }

    /**
     * @param Tree $tree
     * @param $level
     * @return TreeNode[]
     */
    public function getNodesForLevel(Tree $tree, $level, $order = 'desc') {
        $criteria = new CDbCriteria;
        $criteria->join = 'left join account as acc on t.account_id = acc.id';
        $criteria->condition = 't.tree_id = :tree_id and t.level = :level';
        $criteria->params = array(
            ':level' => $level,
            ':tree_id' => $tree->id
        );
        $criteria->order = 'acc.karma ' . $order;
        return TreeNode::model()->findAll($criteria);
    }

    public function countAccounts($treeId = null) {
        $command = $this->cmd(TreeNode::model()->tableName())->select('count(id)');
        return $treeId && $treeId > 0
            ? $command->where('tree_id = :tree_id and account_id is not null', array(':tree_id' => $treeId))->queryScalar()
            : $command->where('account_id is not null')->queryScalar();
    }

    public function findNodesByCodes($treeId, $codes) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'tree_id = :tree_id and code in (:codes)';
        $criteria->params = array(
            ':codes' => implode(',', $codes),
            ':tree_id' => $treeId
        );

        return TreeNode::model()->findAll($criteria);
    }

    public function findBestJoinWithMinimalLevel($treeId) {
        $criteria = new CDbCriteria;

        $criteria->condition = 'tree_id = :tree_id and is_best_join = :best_join';
        $criteria->params = array(
            ':best_join' => 1,
            ':tree_id' => $treeId
        );
        $criteria->order = 'level asc';
        $criteria->limit = 1;

        return TreeNode::model()->find($criteria);
    }

    public function findNonOccupiedWithMinimalLevel($treeId, $minimalLevel = 0) {
        $criteria = new CDbCriteria;

        $criteria->condition = 'tree_id = :tree_id and account_id is null';
        $params = [
            ':tree_id' => $treeId
        ];
        if ($minimalLevel > 0) {
            $criteria->condition .= ' and level >= :minimal_level';
            $params[':minimal_level'] = $minimalLevel;
        }
        $criteria->params = $params;
        $criteria->order = 'level asc';
        $criteria->limit = 1;

        return TreeNode::model()->find($criteria);
    }

    public function findOccupiedWithMinimalLevelEmptyChildren($treeId, $minimalLevel = 0) {
        $criteria = new CDbCriteria;

        $criteria->condition = 'tree_id = :tree_id and left_node_id is null and right_node_id is null';
        $params = [
            ':tree_id' => $treeId
        ];
        if ($minimalLevel > 0) {
            $criteria->condition .= ' and level >= :minimal_level';
            $params[':minimal_level'] = $minimalLevel;
        }
        $criteria->params = $params;
        $criteria->order = 'level asc';
        $criteria->limit = 1;

        return TreeNode::model()->find($criteria);
    }

    public function findIds() {
        return $this->cmd()->select('id')->queryColumn();
    }

    protected function model() {
        return Tree::model();
    }

    public function all() {
        return $this->model()->findAll();
    }
}