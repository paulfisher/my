<?php

/**
 * Service with main logic of structure/payments/karma
 * Class BifService
 */
class BifService extends CComponent {

    /** @var TreeLock[] */
    private $locks = [];

    public function init() {
        if (version_compare(phpversion(), "5.4.0") < 0) {
            throw new CException("Update your php version. Need > 5.4.0");
        }
    }

    private function trees() {
        return Yii::app()->getTreeService();
    }

    /**
     * @param Account $account
     * @param Tree $tree
     * @return TreeNode
     */
    public function autoJoinSafe(Account $account, Tree $tree) {
        $autoJoin = function () use ($account, $tree) {
            $node = $this->trees()->autoJoin($account, $tree);
            $this->trees()->recalculateNodesIndexes($tree, $node->level + 1);
            return $node;
        };
        return $this->transactional($tree->id, 'autoJoin', $autoJoin);
    }

    public function joinSafe(Account $account, TreeNode $treeNode) {
        $join = function () use ($account, $treeNode) {
            $result = $this->trees()->join($account, $treeNode);
            $this->trees()->recalculateNodesIndexes($treeNode->tree, $treeNode->level + 1);
            return $result;
        };
        return $this->transactional($treeNode->tree_id, 'joinSafe', $join);
    }

    public function createNodeSafe(Tree $tree, TreeNode $parent = null, $childDirection = TreeNode::CHILD_NO) {
        $createNode = function() use ($tree, $parent, $childDirection) {
            $node = $this->trees()->createNode($tree, $parent, $childDirection);
            $this->trees()->recalculateNodesIndexes($tree, $node->level);
            return $node;
        };
        return $this->transactional($tree->id, 'createNode', $createNode);
    }

    public function clearNodeSafe(TreeNode $node) {
        $tree = $node->tree;
        $clearNode = function() use ($node, $tree) {
            $this->trees()->clearNode($node);
            $this->trees()->recalculateNodesIndexesForTree($tree);
        };
        $this->transactional($tree->id, 'clearNode', $clearNode);
    }

    public function handlePaymentSafe(Campaign $campaign) {
        $tree = $campaign->account->tree;
        $handler = function() use ($campaign, $tree) {
            if (!$campaign->account->treeNode) {
                throw new CException("Campaign " . $campaign->id . ' has no tree node');
            }
            $campaign->checkGoal();
            if ($campaign->account->refresh() && $campaign->account->treeNode) {
                $this->trees()->onNodePayment($campaign->account->treeNode);
                $this->trees()->recalculateNodesIndexesForTree($tree);
            }
        };
        $this->transactional($tree->id, 'handlePaymentSafe', $handler);
    }

    public function handleNodeWeightChanged(TreeNode $treeNode) {
        $handler = function() use ($treeNode) {
            $activeZoneVacantNode = null;
            while (true) {
                if ($treeNode->isEmptyAccount() && $treeNode->inActiveZone()) {
                    $activeZoneVacantNode = $treeNode;//заполним самую верхнюю пустую ноду в активной зоне
                }
                if (!$treeNode->parentNode) {
                    break;
                }
                $treeNode = $treeNode->parentNode;
            }
            if ($activeZoneVacantNode) {
                $this->clearNodeSafe($activeZoneVacantNode);
            }
        };
        $this->transactional($treeNode->tree->id, 'handleNodeWeightChanged', $handler);
    }

    public function transactional($lockId, $name, $fn) {
        $result = null;

        Yii::info('trying lock ' . $name);
        $this->lock($lockId);

        $transaction = Yii::app()->db->getCurrentTransaction();
        $inner = !!$transaction;
        if (!$inner) {
            $transaction = Yii::app()->db->beginTransaction();
        }

        try {
            $then = microtime(true);
            $result = $fn();
            if (!$inner) {
                $transaction->commit();
            }
            Yii::log(sprintf("%s [%f]", $name, microtime(true)-$then), CLogger::LEVEL_INFO);
        } catch (Exception $e) {
            if (!$inner) {
                $transaction->rollback();
            }
            Yii::info('exception, unlocking ' . $name);
            $this->unlock($lockId);
            throw $e;
        }

        Yii::info('unlocking ' . $name);
        $this->unlock($lockId);

        return $result;
    }

    public function checkTransactional() {
        $transaction = Yii::app()->db->getCurrentTransaction();
        if (!$transaction) {
            throw BifException::transactionRequired();
        }
    }

    private function lock($id) {
        if (isset($this->locks[$id])) {
            $this->locks[$id]['ref'] += 1;
            return;
        }

        $lock = new TreeLock($id);
        while (!$lock->lock()) {}
        $this->locks[$id] = [
            'lock' => $lock,
            'ref' => 1
        ];
    }

    private function unlock($id) {
        if (!isset($this->locks[$id])) {
            throw new CException("Invalid lock state. No lock for key $id.");
        }
        $this->locks[$id]['ref'] -= 1;
        if ($this->locks[$id]['ref'] == 0) {
            $this->locks[$id]['lock']->unlock();
            unset($this->locks[$id]);
        }
    }
}