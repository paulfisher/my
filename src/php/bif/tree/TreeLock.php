<?php

class TreeLock {

    protected $key = null;
    protected $file = null;
    protected $own = false;

    function __construct($key) {
        $this->key = $key;
        //create a new resource or get exisitng with same key
        $this->file = fopen(PROTECTED_PATH."/runtime/tree.$key.lockfile", 'w+');
    }

    function __destruct() {
        if ($this->own)
            $this->unlock();
    }

    function lock() {
        if (!flock($this->file, LOCK_EX)) {
            Yii::error("TreeLock::acquire_lock FAILED to acquire lock [" . $this->key . "]");
            return false;
        }
        ftruncate($this->file, 0);
        fwrite($this->file, "Locked");
        fflush($this->file);

        $this->own = true;
        return $this->own;
    }


    function unlock( ) {
        $key = $this->key;
        if ($this->own) {
            if (!flock($this->file, LOCK_UN)) {
                Yii::error("TreeLock::unlock FAILED to release lock [$key]");
                return FALSE;
            }
            ftruncate($this->file, 0);
            fwrite($this->file, "Unlocked");
            fflush($this->file);
        } else {
            Yii::error("TreeLock::unlock called on [$key] but its not acquired by caller");
        }
        $this->own = false;
        return $this->own;
    }
};