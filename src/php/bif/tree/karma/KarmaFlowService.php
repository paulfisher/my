<?php

class KarmaFlowService extends CApplicationComponent {

    public function donate(Account $donor, Account $recipient, $value) {
        Yii::app()->bifService()->checkTransactional();

        if ($donor->karma < $value) {
            throw BifException::karmaNotEnough($donor, $value);
        }

        $minusKarma = new KarmaBonus();
        $minusKarma->account_id = $donor->id;
        $minusKarma->karma_value = -$value;
        $minusKarma->type = KarmaBonusType::Donate;
        $minusKarma->status = KarmaBonusStatus::Active;

        $plusKarma = new KarmaBonus();
        $plusKarma->account_id = $recipient->id;
        $plusKarma->karma_value = $value;
        $plusKarma->type = KarmaBonusType::Donate;
        $plusKarma->status = KarmaBonusStatus::Active;

        if ($minusKarma->save()) {
            $plusKarma->save();
        }
    }

    public function flowAfterTreeEntry(Account $account) {
        if (!$account->treeNode) {
            throw BifException::accountNotInTree($account);
        }

        $tree = $account->treeNode->tree;
        $bonuses = $account->karmaBonuses;

        /** @var KarmaBonus[] $bonusesToDelete **/
        $bonusesToDelete = array();
        $karmaToDistrib = 0;

        //подсчитываем, сколько кармы следует раздать
        foreach ($bonuses as $bonus) {
            if ($bonus->status == KarmaBonusStatus::ActiveAfterTreeEntry) {
                $karmaToDistrib += $bonus->karma_value;
                $bonusesToDelete[] = $bonus;
            }
        }

        //нет кармы для дарения
        if ($karmaToDistrib <= 0) {
            return;
        }

        //раздаем карму
        foreach ($this->getKarmaFlowRule($tree)->getFlowBonuses($tree, $account, $karmaToDistrib) as $bonus) {
            $this->saveKarmaBonus($bonus);
        }

        //удаляем подаренную карму
        foreach ($bonusesToDelete as $bonusToDelete) {
            $bonusToDelete->delete();
        }

        $this->handleAccountKarmaChanged($account);
    }

    public function flowPayment(Payment $payment) {
        $tree = $payment->campaign->account->tree;
        $account = $payment->account;
        $karmaBefore = $account->refreshKarma()->karma;

        $totalKarma = KarmaCalculator::getKarma($tree, $payment);

        //3. Распределяем карму
        foreach ($this->getKarmaFlowRule($tree)->getBonuses($tree, $account, $totalKarma) as $bonus) {
            $this->saveKarmaBonus($bonus, $payment);
        }

        //4. Распределяем карму по реферерам
        foreach ($this->getReferrersBonuses($payment) as $bonus) {
            $this->saveKarmaBonus($bonus, $payment);
        }

        $account->refresh();
        if (!Yii::app()->isConsole()) {
            Yii::app()->user->setKarmaAdded($account->karma - $karmaBefore);
        }

        $this->handleAccountKarmaChanged($account);
    }

    public function createKarmaBonus(Account $account, $value, $type) {
        $bonus = new KarmaBonus();
        $bonus->account_id = $account->id;
        $bonus->karma_value = $value;
        $bonus->status = KarmaBonusStatus::Active;
        $bonus->type = $type;
        $bonus->save();

        return $bonus;
    }

    /**
     * @param Tree $tree
     * @return KarmaFlowRule
     */
    private function getKarmaFlowRule(Tree $tree) {
        switch ($tree->karma_flow_type) {
            case KarmaFlowType::Fade: return new FadeKarmaFlowRuleImpl();
            case KarmaFlowType::Flat: return new FlatKarmaFlowRuleImpl();
        }
        return null;
    }

    private function saveKarmaBonus(KarmaBonusInfo $bonus, Payment $payment = null) {
        $karmaBonus = new KarmaBonus();
        $karmaBonus->status = $bonus->getStatus();
        $karmaBonus->account_id = $bonus->getAccountId();
        $karmaBonus->karma_value = $bonus->getKarma();
        $karmaBonus->type = $bonus->getType();
        $karmaBonus->payment_id = $payment ? $payment->id : null;
        $karmaBonus->save();
    }

    private function getReferrersBonuses(Payment $payment) {
        $bonuses = array();
        $referrer = $payment->account->referrer;
        if ($referrer) {
            $bonuses[] = new KarmaBonusInfo($referrer->account_id, ($payment->value * $referrer->karma_flow_percentage), KarmaBonusType::Referrer);
        }
        return $bonuses;
    }

    private function handleAccountKarmaChanged(Account $account) {
        if (!$account->treeNode) {
            return;
        }

        //ищу по пэрентам дыру в activeZone. если она есть, делаю ей clear
        Yii::app()->bifService()->handleNodeWeightChanged($account->treeNode);
    }

    public function addKarma($account, $value) {
        $this->createKarmaBonus($account, $value, KarmaBonusType::ManualChange);
        $this->handleAccountKarmaChanged($account);
    }
}