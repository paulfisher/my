<?php

interface KarmaFlowRule {

    /**
     * @param Tree $tree
     * @param Account $account
     * @param $totalKarma
     * @return KarmaBonusInfo[]
     */
    function getBonuses(Tree $tree, Account $account, $totalKarma);

    /**
     * @param Tree $tree
     * @param Account $account
     * @param $karma
     * @return KarmaBonusInfo[]
     */
    function getFlowBonuses(Tree $tree, Account $account, $karma);

    /**
     * @param Tree $tree
     * @param Account $account
     * @param $karma
     * @return KarmaBonusInfo[]
     */
    function getAccountBonus(Tree $tree, Account $account, $karma);
}

class KarmaBonusInfo {
    private $_accountId;
    private $_karma;
    private $_type;
    private $_status;

    public function __construct($accountId, $karma, $type, $status = KarmaBonusStatus::Active) {
        $this->_accountId = $accountId;
        $this->_karma = $karma;
        $this->_type = $type;
        $this->_status = $status;
    }

    public function getAccountId() {return $this->_accountId;}
    public function getKarma() {return $this->_karma;}
    public function getType() {return $this->_type;}
    public function getStatus() {return $this->_status;}
}
