<?php

class FadeKarmaFlowRuleImpl implements KarmaFlowRule {

    public function getBonuses(Tree $tree, Account $account, $totalKarma) {
        $self = $this->getAccountBonus($tree, $account, $totalKarma);
        $toDistribute = $this->getFlowBonuses($tree, $account, $totalKarma - $self->getKarma());

        return array_merge(array($self), $toDistribute);
    }

    public function getFlowBonuses(Tree $tree, Account $account, $karma) {
        if ($karma < 0) {
            return array();
        }

        //если не в дереве, сохраняем карму для последующего распределения после попадания в дерево
        if (is_null($account->treeNode)) {
            $result = array(
                new KarmaBonusInfo(
                    $account->id,
                    $karma,
                    KarmaBonusType::Flow,
                    KarmaBonusStatus::ActiveAfterTreeEntry
                )
            );//эта карма перетечет после входа аккаунта в дерево
            return $result;
        }

        $result = array();
        $node = $account->treeNode->parentNode;
        while ($node != null) {
            if ($node->parentNode != null) {
                $karma = $karma - $this->part($karma, $tree->karma_flow);
            }

            $result[] = new KarmaBonusInfo(
                $node->account_id,
                $karma,
                KarmaBonusType::Flow,
                $node->inActiveZone() ? KarmaBonusStatus::Inactive : KarmaBonusStatus::Active//в активную зону карма не перетекает
            );

            $node = $node->parentNode;
        }

        return $result;
    }

    public function getAccountBonus(Tree $tree, Account $account, $karma) {
        if (!is_null($account->treeNode)) {
            $parents = $account->treeNode->getParents();
            if (empty($parents)) {//для аккаунта в вершине дерева вся карма достается ему
                return new KarmaBonusInfo($account->id, $karma, KarmaBonusType::Own);
            }
        }
        $distribKarma = floor($tree->karma_flow * $karma / 100);
        $selfNodeKarma = $karma - $distribKarma;

        return new KarmaBonusInfo($account->id, $selfNodeKarma, KarmaBonusType::Own);
    }

    private function part($total, $percent) {
        return floor($percent * $total / 100);
    }
}