<?php

class KarmaCalculator {

    public static function getKarma(Tree $tree, Payment $payment) {
        /*
         * Правило 2: бонусная карма
         * В случае, если это правило включено, то чем выше кампания, тем больше кармы она будет давать тому, кто ее поддержал.
         * Таким образом, если пользователь поддерживает самую нижнюю кампанию в активной зоне, то он получает X% кармы.
         * Если самую высшую - то Y% кармы. X и Y задаются в админке, в разделе настроек активной зоны, подразделе Бонусная Карма.
         * Например, X = 50%, Y = 150%. В этом случае поддержка самой нижней кампании даст бекеру 50% кармы за его платеж.
         * Средней кампании = 100%, самой верхней = 150%.
         * */
        $bestLvl = 1;
        $lowestLvl = $tree->active_zone_level;

        $campaignLevel = $payment->campaign->account->treeNode->level;

        $coefficient = self::getMinCoef() +
            (self::getMaxCoef() - self::getMinCoef()) *
                (($lowestLvl - $campaignLevel)
                / ($lowestLvl - $bestLvl));
        Yii::info("campaign level: $campaignLevel lowestLevel: $lowestLvl coef: $coefficient ");

        //return $payment->value * $tree->karma_bonus_rate * $coeff / 100;

        #TODO нужно предусмотреть включение/выключение этого правила

        return $payment->value * $tree->karma_bonus_rate;
    }

    private static function getMaxCoef() {
        return 150;
    }

    private static function getMinCoef() {
        return 50;
    }
}