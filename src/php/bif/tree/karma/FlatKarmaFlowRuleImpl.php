<?php

class FlatKarmaFlowRuleImpl implements KarmaFlowRule {

    /**
     * @param Tree $tree
     * @param Account $account
     * @param $totalKarma
     * @internal param \Payment $payment
     * @return KarmaBonusInfo[]
     */
    public function getBonuses(Tree $tree, Account $account, $totalKarma) {
        $self = $this->getAccountBonus($tree, $account, $totalKarma);
        $toDistribute = $this->getFlowBonuses($tree, $account, $totalKarma - $self->getKarma());

        return array_merge(array($self), $toDistribute);
    }

    public function getFlowBonuses(Tree $tree, Account $account, $karma) {
        if ($karma < 0) {
            return [];
        }
        $parents = [];
        $parentsAmount = 0;

        if (!is_null($account->treeNode)) {
            $parents = $account->treeNode->getParents();
            $parentsAmount = count($parents);
        }

        //если не в дереве, сохраняем карму для последующего распределения после попадания в дерево
        if (is_null($account->treeNode)) {
            return [new KarmaBonusInfo(
                $account->id,
                $karma,
                KarmaBonusType::Flow,
                KarmaBonusStatus::ActiveAfterTreeEntry)];//эта карма перетечет после входа аккаунта в дерево
        }

        if ($parentsAmount == 0) {
            return [];
        }

        $result = [];
        $karmaPerParent = floor($karma / $parentsAmount);
        //Кому-то должна достаться ошибка округления
        $roundErr = $karma - $karmaPerParent * $parentsAmount;

        //Остатки равномерно распределяем по родителям
        foreach ($parents as $parent) {
            $result[] = new KarmaBonusInfo(
                $parent->account_id,
                $karmaPerParent + $roundErr,
                KarmaBonusType::Flow,
                $parent->inActiveZone() ? KarmaBonusStatus::Inactive : KarmaBonusStatus::Active //в активную зону карма не перетекает
            );

            //Обнуляем ошибку, она достается только прямому родителю
            $roundErr = 0;
        }
        return $result;
    }

    public function getAccountBonus(Tree $tree, Account $account, $karma) {
        if (!is_null($account->treeNode)) {
            $parents = $account->treeNode->getParents();
            if (empty($parents)) {//для аккаунта в вершине дерева вся карма достается ему
                return new KarmaBonusInfo($account->id, $karma, KarmaBonusType::Own);
            }
        }
        $distribKarma = floor($tree->karma_flow * $karma / 100);
        $selfNodeKarma = $karma - $distribKarma;

        return new KarmaBonusInfo($account->id, $selfNodeKarma, KarmaBonusType::Own);
    }
}