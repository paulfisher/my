<?php

namespace App\Services\Calculators;

use App\Models\Pay;
use App\Models\RoiLtv;
use App\Services\ConversionService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RoiLtvCalculator {

    private $_registerCalculator;
    private $_payCalculator;
    private $_createCampaignCalculator;
    private $_visitCalculator;

    public function __construct(VisitCalculator $visitCalculator,
                                RegisterCalculator $registerCalculator,
                                PayCalculator $payCalculator,
                                CreateCampaignCalculator $createCampaignCalculator) {
        $this->_visitCalculator = $visitCalculator;
        $this->_registerCalculator = $registerCalculator;
        $this->_payCalculator = $payCalculator;
        $this->_createCampaignCalculator = $createCampaignCalculator;
    }

    public function calculate(Carbon $startDate, Carbon $endDate) {
        $roiLtv = new RoiLtv();
        $uniqueUsers = $this->_visitCalculator->countUniqueClientVisits($startDate, $endDate);
        $registers = $this->_registerCalculator->countRegisters($startDate, $endDate);
        $backers = $this->_payCalculator->countPayedUsers($startDate, $endDate);
        $creators = $this->_createCampaignCalculator->countCreators($startDate, $endDate);

        $campaignNames = $this->_createCampaignCalculator->getCampaignsByRegisteredAt($startDate, $endDate);
        $creatorsIncome = $this->_payCalculator->paysValueByCampaigns($campaignNames);
        $paymentsValue = $this->_payCalculator->paysValueByRegistered($startDate, $endDate);
        $roiLtv
            ->setUniqueUsers($uniqueUsers)
            ->setRegisters($registers)
            ->setBackers($backers)
            ->setCreators($creators)
            ->setIncome($paymentsValue)
            ->setCreatorsIncome($creatorsIncome)
            ->calc();

        return $roiLtv;
    }

    public function calculateFullYear($year) {
        $startDate = new Carbon();
        $startDate->year($year);
        $startDate->month(1);
        $startDate->day(1);

        $result = [];

        for ($month = 0; $month < 12; $month++) {
            $result[$month] = $this->calculate(
                $startDate->copy()->addMonths($month),
                $startDate->copy()->addMonths($month+1));
        }

        return $result;
    }
}
