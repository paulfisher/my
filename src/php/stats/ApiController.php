<?php

namespace App\Http\Controllers;

use App\Http\Requests\CampaignCreateRequest;
use App\Http\Requests\CampaignHitRequest;
use App\Http\Requests\CampaignRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\LogRequest;
use App\Http\Requests\PayRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\RollingRetentionRequest;
use App\Http\Requests\ViralHitRequest;
use App\Http\Requests\VisitRequest;
use App\Models\CampaignCreate;
use App\Models\Login;
use App\Models\Pay;
use App\Models\Register;
use App\Models\Visit;
use App\Services\StatService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ApiController extends Controller {

    public function postVisit(VisitRequest $visitRequest, StatService $statService) {
        $statService->findOrCreateVisit($visitRequest);
        return 'ok';
    }

    public function postRegister(RegisterRequest $registerRequest, StatService $statService) {
        $statService->createRegister($registerRequest);
        return 'ok';
    }

    public function postLogin(LoginRequest $loginRequest, StatService $statService) {
        $statService->createLogin($loginRequest);
        return 'ok';
    }

    public function postPay(PayRequest $payRequest, StatService $statService) {
        $statService->createPay($payRequest);
        if (!empty($payRequest->last_create_campaign_at)) {
            $statService->createPayCreateCampaignFromPay($payRequest);
        }
        return 'ok';
    }

    public function postCampaignCreate(CampaignCreateRequest $campaignCreateRequest, StatService $statService) {
        $statService->createCampaignCreate($campaignCreateRequest);
        if (!empty($campaignCreateRequest->last_payment_at)) {
            $statService->createPayCreateCampaignFromCampaignCreate($campaignCreateRequest);
        }
        return 'ok';
    }

    public function postRollingRetention(RollingRetentionRequest $request, StatService $statService) {
        $statService->createRollingRetention($request);
        return 'ok';
    }

    public function postLog(LogRequest $request, StatService $statService) {
        $statService->createLog($request);
        return 'ok';
    }

    public function postCampaign(CampaignRequest $campaignRequest, StatService $statService) {
        $statService->updateCampaign($campaignRequest);
        return 'ok';
    }

    public function postCampaignHit(CampaignHitRequest $campaignHitRequest, StatService $statService) {
        $statService->createCampaignHit($campaignHitRequest);
        return 'ok';
    }

    public function postViralHit(ViralHitRequest $viralHitRequest, StatService $statService) {
        $statService->createViralHit($viralHitRequest);
        return 'ok';
    }
}
