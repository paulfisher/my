<?php

namespace App\Models;

class RoiLtv {

    public $uniqueUsers;

    public $registrations;

    //среди тех, что зарегистрировались в заданный период
    public $backers;

    //среди тех, что зарегистрировались в заданный период
    public $creators;

    public $creatorsIncome;

    public $backersPart;
    public $arpu;
    public $arppu;
    public $income;
    public $profit;

    public $registrationLtv;
    public $backerLtv;
    public $creatorLtv;

    public $avgBackerProfit;
    public $avgCreatorProfit;

    public $bifCommission = 5;

    public function setUniqueUsers($uniqueUsers) {
        $this->uniqueUsers = $uniqueUsers;
        return $this;
    }

    public function setRegisters($registers) {
        $this->registrations = $registers;
        return $this;
    }

    public function setBackers($backers) {
        $this->backers = $backers;
        return $this;
    }

    public function setCreators($creators) {
        $this->creators = $creators;
        return $this;
    }

    public function setCreatorsIncome($creatorsIncome) {
        $this->creatorsIncome = $creatorsIncome;
        return $this;
    }

    public function calc() {
        $this->backersPart = 0;
        $this->arpu = 0;
        $this->registrationLtv = 0;
        $this->backerLtv = 0;
        $this->creatorLtv = 0;

        if ($this->registrations > 0) {
            $this->backersPart = $this->backers / $this->registrations;
            $this->arpu = $this->income / $this->registrations;

            $this->registrationLtv = $this->creatorsIncome / $this->registrations;
        }
        if ($this->backers > 0) {
            $this->backerLtv = $this->creatorsIncome / $this->backers;
        }
        if ($this->creators > 0) {
            $this->creatorLtv = $this->creatorsIncome / $this->creators;
        }

        $this->arppu = 0;
        if ($this->backers > 0) {
            $this->arppu = $this->income / $this->backers;
        }

        $this->profit = ($this->income * $this->bifCommission) / 100;

        $this->avgBackerProfit = ($this->backerLtv * $this->bifCommission) / 100;
        $this->avgCreatorProfit = ($this->creatorLtv * $this->bifCommission) / 100;
    }

    public function setIncome($income) {
        $this->income = $income;
        return $this;
    }
}
