<?php

namespace App\Http\Controllers;

use App\Services\Calculators\CreateCampaignCalculator;
use App\Services\Calculators\PayAndCreateCampaignCalculator;
use App\Services\Calculators\PayCalculator;
use App\Services\Calculators\RegisterCalculator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ConversionController extends Controller {

    public function index(CreateCampaignCalculator $createCampaignCalculator, RegisterCalculator $registerCalculator, PayCalculator $payCalculator) {
        $registerConversion = $registerCalculator->visitRegisterConversion();
        $payConversion = $payCalculator->registerPayConversion();
        $createCampaignConversion = $createCampaignCalculator->createCampaignRegisterConversion();

        return view('report.conversion.index', compact(
            'registerConversion',
            'payConversion',
            'createCampaignConversion'
        ));
    }

    public function register(Requests\ConversionSearchRequest $conversionRequest, RegisterCalculator $registerCalculator) {
        $conversionRequest->init();
        $startDate = Carbon::parse($conversionRequest->startDate);

        $registers = $registerCalculator->cortegeVisitRegisterConversion(
            $startDate,
            $conversionRequest->daysCount,
            $conversionRequest->filterByRef,
            $conversionRequest->isRef);

        return view('report.conversion.register',
            compact(
                'registers',
                'startDate',
                'conversionRequest'
            ));
    }

    public function pay(Requests\ConversionSearchRequest $conversionRequest, PayCalculator $conversionCalculator) {
        $conversionRequest->init();
        $startDate = Carbon::parse($conversionRequest->startDate);

        $pays = $conversionCalculator->cortegeRegisterPayConversion(
            $startDate,
            $conversionRequest->daysCount,
            $conversionRequest->filterByRef,
            $conversionRequest->isRef);

        return view('report.conversion.pay',
            compact(
                'pays',
                'startDate',
                'conversionRequest'
            ));
    }

    public function createCampaign(Requests\ConversionSearchRequest $conversionRequest, CreateCampaignCalculator $conversionCalculator) {
        $conversionRequest->init();
        $startDate = Carbon::parse($conversionRequest->startDate);

        $createCampaigns = $conversionCalculator->cortegeCreateCampaignsToRegisterConversions(
            $startDate,
            $conversionRequest->daysCount,
            $conversionRequest->filterByRef,
            $conversionRequest->isRef);

        return view('report.conversion.create_campaign',
            compact(
                'createCampaigns',
                'startDate',
                'conversionRequest'
            ));
    }

    public function payAndCreateCampaign(Requests\ConversionSearchRequest $conversionRequest, PayAndCreateCampaignCalculator $conversionCalculator) {
        $conversionRequest->init();
        $startDate = Carbon::parse($conversionRequest->startDate);

        $payAndCreateCampaigns = $conversionCalculator->cortegePayAndCreateCampaignsToRegisterConversion(
            $startDate,
            $conversionRequest->daysCount,
            $conversionRequest->filterByRef,
            $conversionRequest->isRef);

        return view('report.conversion.pay_and_create_campaign',
            compact(
                'payAndCreateCampaigns',
                'startDate',
                'conversionRequest'
            ));
    }
}
