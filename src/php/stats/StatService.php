<?php


namespace App\Services;

use App\Http\Requests\CampaignCreateRequest;
use App\Http\Requests\CampaignHitRequest;
use App\Http\Requests\CampaignRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\LogRequest;
use App\Http\Requests\PayRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\RollingRetentionRequest;
use App\Http\Requests\ViralHitRequest;
use App\Http\Requests\VisitRequest;
use App\Models\Campaign;
use App\Models\CampaignCreate;
use App\Models\CampaignHit;
use App\Models\Log;
use App\Models\Login;
use App\Models\Pay;
use App\Models\PayCampaignCreate;
use App\Models\Register;
use App\Models\RollingRetention;
use App\Models\Viral;
use App\Models\ViralHit;
use App\Models\ViralHitType;
use App\Models\Visit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StatService {

    public function createVisit($data) {
        $visit = $this->findVisit($data['uid']);
        if (!$visit) {
            $visit = Visit::create($data);
        }
        return $visit;
    }

    public function findOrCreateVisit(VisitRequest $visitRequest) {
        $visit = $this->findVisit($visitRequest->uid);
        if (!$visit) {
            $visit = Visit::create($visitRequest->all());
        }
        return $visit;
    }

    public function createRegister(RegisterRequest $registerRequest) {
        /** @var Visit $visit */
        $visit = $this->findVisit($registerRequest->uid);
        $register = null;
        if ($visit) {
            $register = Register::create(
                collect($registerRequest->all())
                    ->merge([
                        'visit_at' => $visit->create_at,
                        'since_visit_seconds' => Carbon::parse($registerRequest->create_at)->diffInSeconds($visit->create_at)
                    ])->all()
            );
        }
        return $register;
    }

    public function createLogin(LoginRequest $loginRequest) {
        return Login::create(
            collect($loginRequest->all())->merge([
                'create_at' => Carbon::now()
            ])->all()
        );
    }

    public function createPay(PayRequest $payRequest) {
        return Pay::create(
            collect($payRequest->all())
                ->merge([
                    'since_register_minutes' => Carbon::parse($payRequest->create_at)->diffInMinutes(Carbon::parse($payRequest->register_at))
                ])
                ->all()
        );
    }

    public function createCampaignCreate(CampaignCreateRequest $campaignCreateRequest) {
        return CampaignCreate::create(
            collect($campaignCreateRequest->all())
                ->merge([
                    'since_last_payment_minutes' => !empty($campaignCreateRequest->last_payment_at) ? Carbon::parse($campaignCreateRequest->create_at)->diffInMinutes(Carbon::parse($campaignCreateRequest->last_payment_at)) : -1,
                    'since_register_minutes' => Carbon::parse($campaignCreateRequest->create_at)->diffInMinutes(Carbon::parse($campaignCreateRequest->register_at)),
                ])
                ->all()
        );
    }

    public function findVisit($uid) {
        return Visit::where(['uid' => $uid])->first();
    }

    public function createPayCreateCampaignFromPay(PayRequest $payRequest) {
        PayCampaignCreate::create([
            'uid' => $payRequest->uid,
            'is_referral' => $payRequest->is_referral,
            'campaign_name' => $payRequest->last_create_campaign_name,
            'username' => $payRequest->username,
            'create_at' => Carbon::parse($payRequest->create_at),
            'register_at' => Carbon::parse($payRequest->register_at),
            'payment_at' => Carbon::parse($payRequest->create_at),
            'create_campaign_at' => Carbon::parse($payRequest->last_create_campaign_at),
            'goal' => $payRequest->last_create_campaign_goal,
            'category' => $payRequest->last_create_campaign_category,
            'since_register_minutes' => Carbon::parse($payRequest->create_at)->diffInMinutes(Carbon::parse($payRequest->register_at)),
        ]);
    }

    public function createPayCreateCampaignFromCampaignCreate(CampaignCreateRequest $campaignCreateRequest) {
        PayCampaignCreate::create([
            'uid' => $campaignCreateRequest->uid,
            'is_referral' => $campaignCreateRequest->is_referral,
            'campaign_name' => $campaignCreateRequest->campaign_name,
            'username' => $campaignCreateRequest->username,
            'create_at' => Carbon::parse($campaignCreateRequest->create_at),
            'register_at' => Carbon::parse($campaignCreateRequest->register_at),
            'payment_at' => Carbon::parse($campaignCreateRequest->last_payment_at),
            'create_campaign_at' => Carbon::parse($campaignCreateRequest->create_at),
            'goal' => $campaignCreateRequest->goal,
            'category' => $campaignCreateRequest->category,
            'since_register_minutes' => Carbon::parse($campaignCreateRequest->create_at)->diffInMinutes(Carbon::parse($campaignCreateRequest->register_at)),
        ]);
    }

    public function createRollingRetention(RollingRetentionRequest $rollingRetentionRequest) {
        return RollingRetention::create($rollingRetentionRequest->all());
    }

    public function createLog(LogRequest $logRequest) {
        return Log::create($logRequest->all());
    }

    public function updateCampaign(CampaignRequest $campaignRequest) {
        $campaignId = $campaignRequest->campaign_id;
        $campaignData = collect($campaignRequest->all())
            ->merge([
                'create_time_since_register_minutes' => Carbon::parse($campaignRequest->campaign_create_at)
                    ->diffInMinutes(Carbon::parse($campaignRequest->register_at), true)
            ])->all();

        /** @var Campaign $latest */
        $latest = Campaign::where('campaign_id', $campaignId)->latest()->first();
        if (!$latest || $latest->created_at->diffInDays(Carbon::now()) != 0) {
            Campaign::create($campaignData);
        } else {
            $latest->update($campaignData);
        }
    }

    public function createCampaignHit(CampaignHitRequest $campaignHitRequest) {
        CampaignHit::create($campaignHitRequest->all());
        /** @var Campaign $campaign */
        $campaign = Campaign::where('campaign_id', $campaignHitRequest->campaign_id)->first();
        if ($campaign) {
            $campaign->update(['views_count' => intval($campaign->views_count+1)]);
        }
    }

    public function createViralHit(ViralHitRequest $viralHitRequest) {
        $viralHitType = ViralHitType::getByName($viralHitRequest->type);
        $viralHit = ViralHit::where([
            'type' => $viralHitType,
            'key' => $viralHitRequest->key
        ])->first();
        if (!$viralHit) {

            /** @var Viral $viral */
            $viral = Viral::latest()->first();

            if (!$viral || $viral->created_at->diffInMonths(Carbon::now()) >= 1) {

                //накапливаем viral статистику по-месячно
                DB::table('viral_hit')->truncate();
                $viral = Viral::create([
                    'users_count' => Register::count(),
                    'active_users_count' => 0,
                    'mass_invite_users_count' => 0,
                    'invite_visit_count' => 0
                ]);
            }

            ViralHit::create([
                'type' => $viralHitType,
                'key' => $viralHitRequest->key
            ]);
            switch ($viralHitType) {
                case ViralHitType::ACTIVE_USER:
                    $viral->update(['active_users_count' => $viral->active_users_count+1]);
                    break;
                case ViralHitType::MASS_INVITE:
                    $viral->update(['mass_invite_users_count' => $viral->mass_invite_users_count+1]);
                    break;
                case ViralHitType::REFERRAL_VISIT:
                    $viral->update(['referral_visit_count' => $viral->referral_visit_count+1]);
                    break;
            }
        }
    }
}